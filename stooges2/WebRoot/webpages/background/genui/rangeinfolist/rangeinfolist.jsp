<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>日涨幅信息列表</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
   <div class="panel-Title">
	<h5 id="rangeinfo_datatablepaneltitle">日涨幅列表</h5>
</div>
<div class="titlePanel">
	<div class="title-search form-horizontal" id="rangeinfo_search" formcontrolid="402882a1605416ff016054629c58000b">
		<table>
			<tr>
				<td>查询条件</td>
				<td style="padding-left: 5px;">
					<div class="table-filter">
						<input type="text" style="width: 200px; " onclick="PlatUtil.showOrHideSearchTable(this);" class="table-form-control" name="search" readonly="readonly">
						<div class="table-filter-list" style="width: 420px; display: none;max-height: 280px;">
								<div class="form-group">
								    <jsp:include page="/webpages/common/plattagtpl/rangetime_tag.jsp">
									    <jsp:param name="label_col_num" value="3"></jsp:param>
									    <jsp:param name="format" value="YYYY-MM-DD"></jsp:param>
									    <jsp:param name="label_value" value="日期"></jsp:param>
									    <jsp:param name="comp_col_num" value="9"></jsp:param>
									    <jsp:param value="auth_type" name="write"></jsp:param>
									    <jsp:param name="posttimefmt" value="1"></jsp:param>
									    <jsp:param name="istime" value="false"></jsp:param>
									    <jsp:param name="allowblank" value="false"></jsp:param>
									    <jsp:param name="start_name" value="Q_T.RANGEINFO_DATE_GE"></jsp:param>
									    <jsp:param name="end_name" value="Q_T.RANGEINFO_DATE_LE"></jsp:param>
									</jsp:include>
								</div>
								<div class="form-group">
								    <plattag:select placeholder="" istree="false" label_col_num="3" label_value="开始币种" style="width:100%;" allowblank="true" comp_col_num="9" auth_type="write" static_values="" dyna_interface="dictionaryService.findList" dyna_param="{TYPE_CODE:'bittype',ORDER_TYPE:'ASC'}" name="Q_T.RANGEINFO_BEGIN_EQ">
								    </plattag:select>
								</div>
								<div class="form-group">
								    <plattag:select placeholder="" istree="false" label_col_num="3" label_value="结束币种" style="width:100%;" allowblank="true" comp_col_num="9" auth_type="write" static_values="btc:btc,usdt:usdt" dyna_interface="dictionaryService.findList" dyna_param="{TYPE_CODE:'bittype',ORDER_TYPE:'ASC'}" name="Q_T.RANGEINFO_END_EQ">
								    </plattag:select>
								</div>
							<div class="table-filter-list-bottom">
								<a onclick="PlatUtil.tableSearchReset('rangeinfo_search');" class="btn btn-default" href="javascript:void(0);"> 重 置</a> 
								<a onclick="PlatUtil.tableDoSearch('rangeinfo_datatable','rangeinfo_search',true);" class="btn btn-primary" href="javascript:void(0);"> 查 询</a>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="toolbar">
	      <button type="button" onclick="delJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-danger btn-sm">
			<i class="fa fa-trash"></i>&nbsp;删除
		  </button>
		
	</div>
</div>

<div class="gridPanel">
	<table id="rangeinfo_datatable"></table>
</div>
<div id="rangeinfo_datatable_pager"></div>
<script type="text/javascript">
function delJqGridRecordInfo(){
	PlatUtil.operMulRecordForTable({
		tableId:"rangeinfo_datatable",
		selectColName:"RANGEINFO_ID",
		url:"bittrade/RangeInfoController.do?multiDel",
		callback:function(resultJson){
			$("#rangeinfo_datatable").trigger("reloadGrid"); 
		}
	});
}

$(function(){
	PlatUtil.initJqGrid({
		  tableId:"rangeinfo_datatable",
		  searchPanelId:"rangeinfo_search",
		  url: "appmodel/CommonUIController.do?tabledata&FORMCONTROL_ID=402882a1605416ff016054629c58000b",
        rowNum : 100,
		  ondblClickRow:function(rowid,iRow,iCol,e){
				  
		  },
		  colModel: [
		         {name: "RANGEINFO_ID",label:"涨幅ID",
		         width: 100,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         },
		         {name: "RANGEINFO_DATE",label:"日期",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "RANGEINFO_BEGIN",label:"开始币种",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "RANGEINFO_END",label:"结束币种",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "RANGEINFO_UP",label:"单日涨幅",
		         width: 100,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
  if(cellvalue>=0){
    return "<span class=\"label label-primary\">"+cellvalue+"%</span>";
  }else{
    return "<span class=\"label label-danger\">"+cellvalue+"%</span>";
  }

},
		         sortable:true,index: "O_T.RANGEINFO_UP"
		         
		         },
		         {name: "RANGEINFO_RANGE",label:"当日振幅",
		         width: 100,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
  if(cellvalue>=0){
    return "<span class=\"label label-primary\">"+cellvalue+"%</span>";
  }else{
    return "<span class=\"label label-danger\">"+cellvalue+"%</span>";
  }

},
		         sortable:true,index: "O_T.RANGEINFO_RANGE"
		         
		         },
		         {name: "RANGEINFO_OPEN",label:"开盘",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "RANGEINFO_HIGH",label:"最高",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "RANGEINFO_LOW",label:"最低",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "RANGEINFO_CLOSE",label:"收盘",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         }
           ]
	 });
});
</script>
</div>
</div>
  </body>
</html>

<script type="text/javascript">

</script>

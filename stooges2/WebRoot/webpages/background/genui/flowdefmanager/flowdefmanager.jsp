<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>流程定义列表</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,slimscroll,webuploader"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,slimscroll,webuploader"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout" layoutsize="&quot;west__size&quot;:280">
   <div class="ui-layout-west" platundragable="true" compcode="direct_layout">
     <plattag:operlistgroup onclickfn="operlistclick" grouptitle="流程类别列表" dyna_interface="flowTypeService.findGroupList" dyna_param="无" addclickfn="operladdclick" editclickfn="operleditclick" delclickfn="operldelclick" id="flowtypelist"></plattag:operlistgroup>
<script type="text/javascript">
  function operlistclick(listvalue){
	//listvalue点击行的值，点击函数名称需要修改
  if(listvalue=="0"){
    $("input[name='Q_P.FLOWTYPE_ID_EQ']").val("");
  }else{
    $("input[name='Q_P.FLOWTYPE_ID_EQ']").val(listvalue);
  }
  PlatUtil.tableDoSearch("flowdef_datatable","flowdef_search");
}
  function operladdclick(FLOWTYPE_ID){
	var url = "workflow/FlowTypeController.do?goForm&UI_DESIGNCODE=flowtypeform";
	var title = "新增流程类别信息";
	if(FLOWTYPE_ID){
		url+=("&FLOWTYPE_ID="+FLOWTYPE_ID);
		title = "编辑流程类别信息";
	}
	PlatUtil.openWindow({
	  title:title,
	  area: ["500px","200px"],
	  content: url,
	  end:function(){
		  if(PlatUtil.isSubmitSuccess()){
              //弹出框提交成功后,需要回调的代码
            PlatUtil.reloadOperGroupList("flowtypelist",{
    		});
		  }
	  }
	});
}
  function operleditclick(listvalue){
    operladdclick(listvalue);
}
  function operldelclick(listvalue){
	//listvalue点击行的值，点击函数名称需要修改
  PlatUtil.ajaxProgress({
    url:"workflow/FlowTypeController.do?multiDel",
    params:{
      typeId:listvalue
    },
    callback : function(resultJson) {
      if (resultJson.success) {
        parent.layer.alert(PlatUtil.SUCCESS_MSG,{icon: 1,resize:false});
        PlatUtil.reloadOperGroupList("flowtypelist",{});
      } else {
        parent.layer.alert(PlatUtil.FAIL_MSG,{icon: 2,resize:false});
      }
    }
  });
}
</script>
</div>
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
   <div class="panel-Title">
	<h5 id="flowdef_datatablepaneltitle">流程定义列表</h5>
</div>
<div class="titlePanel">
	<div class="title-search form-horizontal" id="flowdef_search" formcontrolid="402848a55bcda417015bcda5e11f0019">
		<table>
			<tr>
				<td>查询条件</td>
				<td style="padding-left: 5px;">
					<div class="table-filter">
                			  <input type="hidden" name="Q_T.FLOWTYPE_ID_EQ" value="">
                			  <input type="hidden" name="Q_P.FLOWTYPE_ID_EQ" value="">
						<input type="text" style="width: 200px; " onclick="PlatUtil.showOrHideSearchTable(this);" class="table-form-control" name="search" readonly="readonly">
						<div class="table-filter-list" style="width: 420px; display: none;max-height: 280px;">
								<div class="form-group">
									<plattag:input name="Q_T.FLOWDEF_CODE_LIKE" auth_type="write" label_value="定义编码" maxlength="100" allowblank="true" placeholder="" label_col_num="3" comp_col_num="9"></plattag:input>
								</div>
								<div class="form-group">
									<plattag:input name="Q_T.FLOWDEF_NAME_LIKE" auth_type="write" label_value="定义名称" maxlength="100" allowblank="true" placeholder="" label_col_num="3" comp_col_num="9"></plattag:input>
								</div>
							<div class="table-filter-list-bottom">
								<a onclick="PlatUtil.tableSearchReset('flowdef_search');" class="btn btn-default" href="javascript:void(0);"> 重 置</a> 
								<a onclick="PlatUtil.tableDoSearch('flowdef_datatable','flowdef_search',true);" class="btn btn-primary" href="javascript:void(0);"> 查 询</a>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="toolbar">
	      <button type="button" onclick="addJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-plus"></i>&nbsp;新增
		  </button>
		
	      <button type="button" onclick="editJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-primary btn-sm">
			<i class="fa fa-pencil"></i>&nbsp;编辑
		  </button>
		
	      <button type="button" onclick="delJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-danger btn-sm">
			<i class="fa fa-trash"></i>&nbsp;删除
		  </button>
		
	      <button type="button" onclick="configFlowDef();" platreskey="" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-file-pdf-o"></i>&nbsp;流程配置
		  </button>
		
	      <button type="button" onclick="cloneFlowDef();" platreskey="" class="btn btn-outline btn-primary btn-sm">
			<i class="fa fa-clone"></i>&nbsp;克隆流程
		  </button>
		
	      <button type="button" onclick="versionFlowDef();" platreskey="" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-bookmark-o"></i>&nbsp;历史版本管理
		  </button>
		
	      <button type="button" onclick="exportDefConfig();" platreskey="" class="btn btn-outline btn-primary btn-sm">
			<i class="fa fa-download"></i>&nbsp;导出配置
		  </button>
		
          <span id="importdefjson" impfiletypes="json" platreskey="" impfilesize="10,485,760" imptableid="flowdef_datatable" impurl="workflow/FlowDefController.do?impConfig" class="btn btn-outline btn-primary btn-sm platImpUploadFile"><i class="fa fa-upload"></i>&nbsp;导入配置</span>
		
	</div>
</div>

<div class="gridPanel">
	<table id="flowdef_datatable"></table>
</div>
<div id="flowdef_datatable_pager"></div>
<script type="text/javascript">
function addJqGridRecordInfo(FLOWDEF_ID){
    var url = "workflow/FlowDefController.do?goOnlineDesign";
    if(FLOWDEF_ID){
       url+=("&FLOWDEF_ID="+FLOWDEF_ID);
    }
	window.open(url,"_blank");
}
function editJqGridRecordInfo(){
	var rowData = PlatUtil.getTableOperSingleRecord("flowdef_datatable");
	if(rowData){
		addJqGridRecordInfo(rowData.FLOWDEF_ID);
	}
}
function delJqGridRecordInfo(){
	PlatUtil.operMulRecordForTable({
		tableId:"flowdef_datatable",
		selectColName:"FLOWDEF_ID",
		url:"workflow/FlowDefController.do?multiDel",
		callback:function(resultJson){
			$("#flowdef_datatable").trigger("reloadGrid"); 
		}
	});
}
function configFlowDef(){
   var rowData = PlatUtil.getTableOperSingleRecord("flowdef_datatable");
	if(rowData){
        var FLOWDEF_ID = rowData.FLOWDEF_ID;
        var FLOWDEF_VERSION = rowData.FLOWDEF_VERSION;
        var url = "workflow/FlowDefController.do?goDefConfig&FLOWDEF_ID="+FLOWDEF_ID+"&FLOWDEF_VERSION="+FLOWDEF_VERSION;
        window.open(url,"_blank");
	}
  
   
}
function cloneFlowDef(){
  var rowData = PlatUtil.getTableOperSingleRecord("flowdef_datatable");
  if(rowData){
    var FLOWDEF_ID = rowData.FLOWDEF_ID;
    var FLOWDEF_NAME = rowData.FLOWDEF_NAME;
    var url = "workflow/FlowDefController.do?goClone&FLOWDEF_ID="+FLOWDEF_ID+"&FLOWDEF_NAME="+FLOWDEF_NAME;
    PlatUtil.openWindow({
      title:"克隆流程定义",
      area: ["900px","270px"],
      content: url,
      end:function(){
        if(PlatUtil.isSubmitSuccess()){
          //弹出框提交成功后,需要回调的代码
          $("#flowdef_datatable").trigger("reloadGrid"); 
        }
      }
    });
  }
}
function versionFlowDef(){
  var rowData = PlatUtil.getTableOperSingleRecord("flowdef_datatable");
  if(rowData){
    var FLOWDEF_ID = rowData.FLOWDEF_ID;
    var url = "workflow/FlowDefController.do?goVersion&FLOWDEF_ID="+FLOWDEF_ID;
    PlatUtil.openWindow({
      title:"流程版本管理",
      area: ["900px","400px"],
      content: url,
      end:function(){
        if(PlatUtil.isSubmitSuccess()){
          //弹出框提交成功后,需要回调的代码
          $("#flowdef_datatable").trigger("reloadGrid"); 
        }
      }
    });
  }
}
function exportDefConfig(){
    var selectDatas = PlatUtil.getTableOperMulRecord("flowdef_datatable");
	if(selectDatas){
       var selectDefIds = "";
       $.each(selectDatas,function(index,obj){
        if(index>0){
          selectDefIds+=",";
        }
        selectDefIds+= obj.FLOWDEF_ID;
       });
       var url = __ctxPath+"/workflow/FlowDefController/exportConfig.do?defIds="+selectDefIds;
		window.location.href=url;
    }
}



$(function(){
	PlatUtil.initJqGrid({
		  tableId:"flowdef_datatable",
		  searchPanelId:"flowdef_search",
		  url: "appmodel/CommonUIController.do?tabledata&FORMCONTROL_ID=402848a55bcda417015bcda5e11f0019",
		  ondblClickRow:function(rowid,iRow,iCol,e){
				  
		  },
		  colModel: [
		         {name: "FLOWDEF_ID",label:"流程定义ID",
		         width: 100,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         },
		         {name: "FLOWDEF_CODE",label:"流程定义编码",
		         width: 200,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "FLOWDEF_NAME",label:"流程定义名称",
		         width: 300,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "FLOWDEF_VERSION",label:"版本号",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "FLOWTYPE_NAME",label:"流程类别名称",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         }
           ]
	 });
});
</script>
</div>
</div>
  </body>
</html>

<script type="text/javascript">
function reloadFlowDefGrid(){
  $("#flowdef_datatable").trigger("reloadGrid"); 
}
</script>

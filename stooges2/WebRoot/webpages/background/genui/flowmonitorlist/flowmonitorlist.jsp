<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>流程监控列表</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
   <div class="panel-Title">
	<h5 id="flowmonitor_datatablepaneltitle">流程实例列表</h5>
</div>
<div class="titlePanel">
	<div class="title-search form-horizontal" id="flowmonitor_search" formcontrolid="402848a55c2ef72c015c2f02cd150057">
		<table>
			<tr>
				<td>查询条件</td>
				<td style="padding-left: 5px;">
					<div class="table-filter">
						<input type="text" style="width: 200px; " onclick="PlatUtil.showOrHideSearchTable(this);" class="table-form-control" name="search" readonly="readonly">
						<div class="table-filter-list" style="width: 420px; display: none;max-height: 280px;">
								<div class="form-group">
									<plattag:input name="Q_E.EXECUTION_ID_LIKE" auth_type="write" label_value="实例号" maxlength="100" allowblank="true" placeholder="" label_col_num="3" comp_col_num="9"></plattag:input>
								</div>
								<div class="form-group">
									<plattag:input name="Q_E.EXECUTION_SUBJECT_LIKE" auth_type="write" label_value="实例标题" maxlength="100" allowblank="true" placeholder="" label_col_num="3" comp_col_num="9"></plattag:input>
								</div>
								<div class="form-group">
								    <jsp:include page="/webpages/common/plattagtpl/rangetime_tag.jsp">
									    <jsp:param name="label_col_num" value="3"></jsp:param>
									    <jsp:param name="format" value="YYYY-MM-DD"></jsp:param>
									    <jsp:param name="label_value" value="发起日期"></jsp:param>
									    <jsp:param name="comp_col_num" value="9"></jsp:param>
									    <jsp:param value="auth_type" name="write"></jsp:param>
									    <jsp:param name="istime" value="false"></jsp:param>
									    <jsp:param name="allowblank" value="false"></jsp:param>
									    <jsp:param name="start_name" value="Q_E.EXECUTION_CREATETIME_GE"></jsp:param>
									    <jsp:param name="end_name" value="Q_E.EXECUTION_CREATETIME_LE"></jsp:param>
									</jsp:include>
								</div>
								<div class="form-group">
								    <plattag:select placeholder="" istree="false" label_col_num="3" label_value="状态" style="width:100%;" allowblank="true" comp_col_num="9" auth_type="write" static_values="" dyna_interface="dictionaryService.findList" dyna_param="{TYPE_CODE:'EXE_STATUS',ORDER_TYPE:'ASC'}" name="Q_E.STATUS_EQ">
								    </plattag:select>
								</div>
							<div class="table-filter-list-bottom">
								<a onclick="PlatUtil.tableSearchReset('flowmonitor_search');" class="btn btn-default" href="javascript:void(0);"> 重 置</a> 
								<a onclick="PlatUtil.tableDoSearch('flowmonitor_datatable','flowmonitor_search',true);" class="btn btn-primary" href="javascript:void(0);"> 查 询</a>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="toolbar">
	      <button type="button" onclick="editJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-primary btn-sm">
			<i class="fa fa-pencil"></i>&nbsp;流程任务管理
		  </button>
		
	      <button type="button" onclick="forwardJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-mail-forward"></i>&nbsp;环节跳转
		  </button>
		
	      <button type="button" onclick="endRecordInfo();" platreskey="" class="btn btn-outline btn-danger btn-sm">
			<i class="fa fa-minus-circle"></i>&nbsp;终结实例
		  </button>
		
	      <button type="button" onclick="delJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-danger btn-sm">
			<i class="fa fa-trash"></i>&nbsp;删除实例
		  </button>
		
	      <button type="button" onclick="updateToNewest();" platreskey="" class="btn btn-outline btn-primary btn-sm">
			<i class="fa fa-refresh"></i>&nbsp;更新到最新版本
		  </button>
		
	</div>
</div>

<div class="gridPanel">
	<table id="flowmonitor_datatable"></table>
</div>
<div id="flowmonitor_datatable_pager"></div>
<script type="text/javascript">
function editJqGridRecordInfo(){
	var rowData = PlatUtil.getTableOperSingleRecord("flowmonitor_datatable");
	if(rowData){
        var EXECUTION_ID = rowData.EXECUTION_ID;
        PlatUtil.openWindow({
          title:"流程任务管理",
          area: ["80%","400px"],
          content: "workflow/JbpmTaskController.do?goMonitorList&EXECUTION_ID="+EXECUTION_ID,
          end:function(){
              $("#flowmonitor_datatable").trigger("reloadGrid"); 
          }
        });
	}
}
function forwardJqGridRecordInfo(){
	var rowData = PlatUtil.getTableOperSingleRecord("flowmonitor_datatable");
	if(rowData){
        var EXECUTION_ID = rowData.EXECUTION_ID;
        PlatUtil.openWindow({
          title:"环节跳转",
          area: ["90%","400px"],
          content: "workflow/JbpmTaskController.do?goJumpList&EXECUTION_ID="+EXECUTION_ID,
          end:function(){
              $("#flowmonitor_datatable").trigger("reloadGrid"); 
          }
        });
	}
}
function endRecordInfo(){
	PlatUtil.operMulRecordForTable({
		tableId:"flowmonitor_datatable",
        tipMsg:"您确定要终结所选流程实例?",
		selectColName:"EXECUTION_ID",
		url:"workflow/ExecutionController.do?endExe",
		callback:function(resultJson){
			$("#flowmonitor_datatable").trigger("reloadGrid"); 
		}
	});
}
function delJqGridRecordInfo(){
	PlatUtil.operMulRecordForTable({
		tableId:"flowmonitor_datatable",
		selectColName:"EXECUTION_ID",
		url:"workflow/ExecutionController.do?multiDel",
		callback:function(resultJson){
			$("#flowmonitor_datatable").trigger("reloadGrid"); 
		}
	});
}
function updateToNewest(){
   var selectDatas = PlatUtil.getTableOperMulRecord("flowmonitor_datatable");
   var isSelectRunning = true;
   $.each(selectDatas,function(index,obj){
      var STATUS = obj.STATUS;
      if(STATUS!=1){
        isSelectRunning = false;
      }
   });
   if(isSelectRunning){
     PlatUtil.operMulRecordForTable({
          tableId:"flowmonitor_datatable",
          selectColName:"EXECUTION_ID",
          url:"workflow/ExecutionController.do?updateToNewest",
       tipMsg:"您确定要将所选实例版本更新为最新吗?",
          callback:function(resultJson){
              $("#flowmonitor_datatable").trigger("reloadGrid"); 
          }
		});
   }else{
     parent.layer.alert("只能选择正在办理的实例!",{icon: 2,resize:false});
   }
	
}

$(function(){
	PlatUtil.initJqGrid({
		  tableId:"flowmonitor_datatable",
		  searchPanelId:"flowmonitor_search",
		  url: "appmodel/CommonUIController.do?tabledata&FORMCONTROL_ID=402848a55c2ef72c015c2f02cd150057",
		  ondblClickRow:function(rowid,iRow,iCol,e){
				  
		  },
		  colModel: [
		         {name: "EXECUTION_ID",label:"实例号",
		         width: 200,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "EXECUTION_SUBJECT",label:"实例标题",
		         width: 200,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
  var EXECUTION_SUBJECT = rowObject.EXECUTION_SUBJECT;
  var EXECUTION_ID = rowObject.EXECUTION_ID;
  var STATUS = rowObject.STATUS;
  var jbpmIsQuery = "true";
  var href = "<a href=\"javascript:void(0)\"";
  href+=" onclick=\"PlatUtil.showExecutionWindow('"+EXECUTION_ID+"','"+EXECUTION_SUBJECT+"','"+jbpmIsQuery+"','',reloadMonitor);\" ";
  href += " ><span style='text-decoration:underline;color:green;'>"+EXECUTION_SUBJECT+"</span></a>";
  return href;
},
		         
		         sortable:false
		         },
		         {name: "FLOWDEF_NAME",label:"流程定义",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "EXECUTION_VERSION",label:"版本号",
		         width: 80,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "EXECUTION_CREATETIME",label:"发起时间",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "STATUS",label:"状态",
		         width: 100,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
  return PlatUtil.formatExeStatus(cellvalue,options,rowObject);
},
		         
		         sortable:false
		         },
		         {name: "CURRENT_NODENAMES",label:"当前环节",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "CURRENT_HANDLERNAMES",label:"当前办理人",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "EXECUTION_ENDTIME",label:"办结时间",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "STATUS",label:"状态",
		         width: 100,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         }
           ]
	 });
});
</script>
</div>
</div>
  </body>
</html>

<script type="text/javascript">
function reloadMonitor(){
  $("#flowmonitor").trigger("reloadGrid");
}
</script>

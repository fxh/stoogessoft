<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<form method="post" class="form-horizontal" compcode="formcontainer" action="" id="attachform" style="">

<div class="form-group plat-form-title" compcode="formtitle" id="">
    <span class="plat-current">
		文章内容
	</span>
</div><div class="form-group" compcode="formgroup">
<label class="col-sm-2 control-label">
文章内容：
</label>
<div class="col-sm-10">
   <script id="ARTICLE_CONTENT" name="ARTICLE_CONTENT" type="text/plain">${article.ARTICLE_CONTENT}</script>
</div>

<script type="text/javascript">
$(function() {
	UE.Editor.prototype._bkGetActionUrl = UE.Editor.prototype.getActionUrl;
	UE.Editor.prototype.getActionUrl = function(action) {
		if (action == 'uploadimage') {
		    //这里调用后端我们写的图片上传接口
		    return __ctxPath+'/system/FileAttachController/uploadUE.do';
		}else if (action == 'uploadfile') {
		    //这里调用后端我们写的图片上传接口
		    return __ctxPath+'/system/FileAttachController/uploadUE.do';
		}else if (action == 'uploadvideo') {
		    //这里调用后端我们写的图片上传接口
		    return __ctxPath+'/system/FileAttachController/uploadUE.do';
		}else{
		     return this._bkGetActionUrl.call(this, action);
		}
	} 
	var ue = UE.getEditor('ARTICLE_CONTENT', {
	    autoHeightEnabled: false,
	    autoFloatEnabled: true,
	    maximumWords:100000,
	    initialFrameHeight:400,
	    /* 上传图片配置项 */
	    "imageActionName": "uploadimage", /* 执行上传图片的action名称 */
	    "imageFieldName": "file", /* 提交的图片表单名称 */
	    "imageMaxSize": 2097152, /* 上传大小限制，单位B */
	    "imageAllowFiles": [".png", ".jpg", ".jpeg", ".gif", ".bmp"], /* 上传图片格式显示 */
	    "imageCompressEnable": true, /* 是否压缩图片,默认是true */
	    "imageCompressBorder": 1600, /* 图片压缩最长边限制 */
	    "imageInsertAlign": "none", /* 插入的图片浮动方式 */
	    "imageUrlPrefix": "", /* 图片访问路径前缀 */
	    "imagePathFormat": "",
    	/* 上传文件配置 */
        "fileActionName": "uploadfile", /* controller里,执行上传视频的action名称 */
        "fileFieldName": "file", /* 提交的文件表单名称 */
        "filePathFormat": "", /* 上传保存路径,可以自定义保存路径和文件名格式 */
        "fileUrlPrefix": "", /* 文件访问路径前缀 */
        "fileMaxSize": 20971520, /* 上传大小限制，单位B，默认50MB */
        "fileAllowFiles": [
            ".png", ".jpg", ".jpeg", ".gif", ".bmp",
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid",
            ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso",
            ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml"
        ], /* 上传文件格式显示 */
        /* 上传视频配置 */
        "videoActionName": "uploadvideo", /* 执行上传视频的action名称 */
        "videoFieldName": "file", /* 提交的视频表单名称 */
        "videoPathFormat": "", /* 上传保存路径,可以自定义保存路径和文件名格式 */
        "videoUrlPrefix": "", /* 视频访问路径前缀 */
        "videoMaxSize": 102400000, /* 上传大小限制，单位B，默认100MB */
        /* 上传视频格式显示 */
        "videoAllowFiles": [
            ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg",
            ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid"] 
            
	});
});
</script>

</div>
<div class="hr-line-dashed"></div></form>

<script type="text/javascript">

</script>

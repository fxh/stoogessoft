<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>产品类别列表</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,ztree,bootswitch,tipsy,autocomplete,pinyin,nicevalid,slimscroll"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,ztree,bootswitch,tipsy,autocomplete,pinyin,nicevalid,slimscroll"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout" layoutsize="&quot;west__size&quot;:280">
   <div class="ui-layout-west" platundragable="true" compcode="direct_layout">
   <plattag:treepanel id="basetree" panel_title="产品类别树" right_menu="[新增,fa fa-plus,showTreeInfoWin][编辑,fa fa-pencil,editTreeInfoWin][删除,fa fa-trash,delTreeNodeInfo]">
</plattag:treepanel>
<script type="text/javascript">
function showTreeInfoWin(PROTYPE_ID) {
	var url = "common/baseController.do?goTreeTableForm&tableName=PLAT_DEMO_PROTYPE&entityName=proType&busDesc=产品类别&UI_DESIGNCODE=protype_form";
	var title = "新增产品类别";
	if(PROTYPE_ID){
		title = "编辑产品类别";
		url+=("&PROTYPE_ID="+PROTYPE_ID);
	}else{
		PlatUtil.hideZtreeRightMenu();
		var selectTreeNode = $.fn.zTree.getZTreeObj("basetree").getSelectedNodes()[0];
		var PROTYPE_ID = selectTreeNode.id;
		url+=("&PROTYPE_PARENTID="+PROTYPE_ID);
	}
	PlatUtil.openWindow({
		title:title,
		area: ["800px","300px"],
		content: url,
		end:function(){
		  if(PlatUtil.isSubmitSuccess()){
			  $.fn.zTree.getZTreeObj("basetree").reAsyncChildNodes(null, "refresh");
		  }
		}
	});
}
function editTreeInfoWin(){
	PlatUtil.hideZtreeRightMenu();
	var selectTreeNode = $.fn.zTree.getZTreeObj("basetree").getSelectedNodes()[0];
	var PROTYPE_ID = selectTreeNode.id;
	if(PROTYPE_ID=="0"){
		parent.layer.alert("根节点不能进行编辑!",{icon: 2,resize:false});
	}else{
		showTreeInfoWin(PROTYPE_ID);
	}
}
function delTreeNodeInfo(){
	PlatUtil.deleteZtreeNode({
		treeId:"basetree",
		url:"common/baseController.do?delTreeNode&tableName=PLAT_DEMO_PROTYPE&busDesc=产品类别"
	});
}
  //这个只是示例代码
function onTreePanelClick(event, treeId, treeNode, clickFlag) {
	if(event.target.tagName=="SPAN"){
		if(treeNode.id=="0"){
			$("input[name='Q_T.DIC_DICTYPE_CODE_EQ']").val("");
			$("#dictionary_paneltitle").text("字典信息列表");
		}else{
			$("input[name='Q_T.DIC_DICTYPE_CODE_EQ']").val(treeNode.DICTYPE_CODE);
			$("#dictionary_paneltitle").text(treeNode.name+"-->字典信息列表");
		}
		//刷新右侧列表
		PlatUtil.tableDoSearch("dictionary_datatable","dictionary_search");
	}
}  
  $(function(){
	  PlatUtil.initZtree({
		  treeId:"basetree",
		  //自动补全匹配方式1:客户端匹配(客户端匹配则一次性加载所有数据) 2:服务端匹配(如果是服务端匹配则需要传入)
		  //如果需要自定义服务端url,请传入autoServerUrl
		  autoCompleteType:"1",
		  callback: {
			  onRightClick: PlatUtil.onZtreeRightClick,
			  onClick:onTreePanelClick,
			  onDrop: PlatUtil.onZtreeNodeDrop
		  },
		  async : {
			url:"common/baseController.do?tree",
			otherParam : {
				//树形表名称
				"tableName" : "PLAT_DEMO_PROTYPE",
				//键和值的列名称
				"idAndNameCol" : "PROTYPE_ID,PROTYPE_NAME",
				//查询其它部门列名称
				"targetCols" : "PROTYPE_CODE,PROTYPE_PARENTID,PROTYPE_LEVEL",
				//最先读取的根节点的值
				"loadRootId" : "0",
				//需要回填的值
				"needCheckIds":"",
				//是否显示树形标题
				"isShowTreeTitle" : "true",
				//根据节点名称
				"treeTitle":"产品类别树"
			}
		  }
	  });
	  
});
</script>

</div>
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
   </div>
</div>
  </body>
</html>

<script type="text/javascript">

</script>

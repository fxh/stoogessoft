<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>文章列表</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,slimscroll"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,slimscroll"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
   <div class="panel-Title">
	<h5 id="article_datatablepaneltitle">文章列表</h5>
</div>
<div class="titlePanel">
	<div class="title-search form-horizontal" id="article_search" formcontrolid="402848a55d2f2e79015d2f8eaffd000d">
		<table>
			<tr>
				<td>查询条件</td>
				<td style="padding-left: 5px;">
					<div class="table-filter">
                			  <input type="hidden" name="Q_T.ARTICLE_TYPE_EQ" value="">
						<input type="text" style="width: 200px; " onclick="PlatUtil.showOrHideSearchTable(this);" class="table-form-control" name="search" readonly="readonly">
						<div class="table-filter-list" style="width: 420px; display: none;max-height: 280px;">
								<div class="form-group">
								    <plattag:select placeholder="" istree="false" label_col_num="3" label_value="所属站点" style="width:100%;" allowblank="true" comp_col_num="9" auth_type="write" static_values="" dyna_interface="webSiteService.findForSelect" dyna_param="无" name="Q_T.ARTICLE_SITEID_EQ">
								    </plattag:select>
								</div>
								<div class="form-group">
								    <plattag:select placeholder="" istree="false" label_col_num="3" label_value="所属栏目" style="width:100%;" allowblank="true" comp_col_num="9" auth_type="write" static_values="" dyna_interface="columnService.findSelectTree" dyna_param="无" name="Q_T.ARTICLE_COLUMNID_EQ">
								    </plattag:select>
								</div>
								<div class="form-group">
								    <plattag:radio name="Q_T.ARTICLE_ISPUB_EQ" auth_type="write" label_col_num="3" label_value="是否发布" static_values="所有:,发布:1,不发布:-1" dyna_interface="" dyna_param="" select_first="true" allowblank="true" is_horizontal="true" comp_col_num="9">
								    </plattag:radio>
								</div>
								<div class="form-group">
									<plattag:input name="Q_T.ARTICLE_SIGN_EQ" auth_type="write" label_value="标识" maxlength="100" allowblank="true" placeholder="" label_col_num="3" comp_col_num="9"></plattag:input>
								</div>
								<div class="form-group">
									<plattag:input name="Q_T.ARTICLE_TITLE_LIKE" auth_type="write" label_value="标题" maxlength="100" allowblank="true" placeholder="" label_col_num="3" comp_col_num="9"></plattag:input>
								</div>
								<div class="form-group">
								    <jsp:include page="/webpages/common/plattagtpl/rangetime_tag.jsp">
									    <jsp:param name="label_col_num" value="3"></jsp:param>
									    <jsp:param name="format" value="YYYY-MM-DD"></jsp:param>
									    <jsp:param name="label_value" value="发布时间"></jsp:param>
									    <jsp:param name="comp_col_num" value="9"></jsp:param>
									    <jsp:param value="auth_type" name="write"></jsp:param>
									    <jsp:param name="posttimefmt" value=""></jsp:param>
									    <jsp:param name="istime" value="false"></jsp:param>
									    <jsp:param name="allowblank" value="false"></jsp:param>
									    <jsp:param name="start_name" value="Q_T.ARTICLE_PUBTIME_GE"></jsp:param>
									    <jsp:param name="end_name" value="Q_T.ARTICLE_PUBTIME_LE"></jsp:param>
									</jsp:include>
								</div>
							<div class="table-filter-list-bottom">
								<a onclick="PlatUtil.tableSearchReset('article_search');" class="btn btn-default" href="javascript:void(0);"> 重 置</a> 
								<a onclick="PlatUtil.tableDoSearch('article_datatable','article_search',true);" class="btn btn-primary" href="javascript:void(0);"> 查 询</a>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="toolbar">
	      <button type="button" onclick="addJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-plus"></i>&nbsp;新增
		  </button>
		
	      <button type="button" onclick="editJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-primary btn-sm">
			<i class="fa fa-pencil"></i>&nbsp;编辑
		  </button>
		
	      <button type="button" onclick="delJqGridRecordInfo();" platreskey="" class="btn btn-outline btn-danger btn-sm">
			<i class="fa fa-trash"></i>&nbsp;删除
		  </button>
		
	      <button type="button" onclick="topArticle();" platreskey="" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-long-arrow-up"></i>&nbsp;置顶文章
		  </button>
		
	      <button type="button" onclick="unTopArticle();" platreskey="" class="btn btn-outline btn-danger btn-sm">
			<i class="fa fa-long-arrow-down"></i>&nbsp;取消置顶
		  </button>
		
	</div>
</div>

<div class="gridPanel">
	<table id="article_datatable"></table>
</div>
<div id="article_datatable_pager"></div>
<script type="text/javascript">
function addJqGridRecordInfo(ARTICLE_ID){
	var url = "cms/ArticleController.do?goForm&UI_DESIGNCODE=articleinfo";
	var title = "新增文章信息信息";
	if(ARTICLE_ID){
		url+=("&ARTICLE_ID="+ARTICLE_ID);
		title = "编辑文章信息信息";
	}
	PlatUtil.openWindow({
	  title:title,
	  area: ["100%","100%"],
	  content: url,
	  end:function(){
		  $("#article_datatable").trigger("reloadGrid"); 
	  }
	});
}
function editJqGridRecordInfo(){
	var rowData = PlatUtil.getTableOperSingleRecord("article_datatable");
	if(rowData){
		addJqGridRecordInfo(rowData.ARTICLE_ID);
	}
}
function delJqGridRecordInfo(){
	PlatUtil.operMulRecordForTable({
		tableId:"article_datatable",
		selectColName:"ARTICLE_ID",
		url:"cms/ArticleController.do?multiDel",
		callback:function(resultJson){
			$("#article_datatable").trigger("reloadGrid"); 
		}
	});
}
function topArticle(){
	PlatUtil.operMulRecordForTable({
		tableId:"article_datatable",
		selectColName:"ARTICLE_ID",
      tipMsg:"您确定要置顶所选文章吗?",
		url:"cms/ArticleController.do?updatetop&isTop=1",
		callback:function(resultJson){
			$("#article_datatable").trigger("reloadGrid"); 
		}
	});
}
function unTopArticle(){
	PlatUtil.operMulRecordForTable({
		tableId:"article_datatable",
		selectColName:"ARTICLE_ID",
      tipMsg:"您确定要对所选文章取消置顶吗?",
		url:"cms/ArticleController.do?updatetop&isTop=-1",
		callback:function(resultJson){
			$("#article_datatable").trigger("reloadGrid"); 
		}
	});
}

$(function(){
	PlatUtil.initJqGrid({
		  tableId:"article_datatable",
		  searchPanelId:"article_search",
		  url: "appmodel/CommonUIController.do?tabledata&FORMCONTROL_ID=402848a55d2f2e79015d2f8eaffd000d",
		  ondblClickRow:function(rowid,iRow,iCol,e){
				  
		  },
		  colModel: [
		         {name: "ARTICLE_ID",label:"文章ID",
		         width: 100,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         },
		         {name: "ARTICLE_SIGN",label:"标识",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "COLUMN_NAME",label:"所属栏目",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "ARTICLE_TITLE",label:"标题",
		         width: 350,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
  var ARTYPE = rowObject.ARTYPE;
  var ARTICLE_SIGN = rowObject.ARTICLE_SIGN;
  var ARTICLE_TITLE = rowObject.ARTICLE_TITLE;
  var ARTICLE_EXTERNAL = rowObject.ARTICLE_EXTERNAL;
  var ARTICLE_EXTERNALURL = rowObject.ARTICLE_EXTERNALURL;
  var title = "["+ARTYPE+"]"+ARTICLE_TITLE;
  var href = "<a href=\"javascript:void(0)\"";
  href+=" onclick=\"PlatUtil.showArticleInfo('"+ARTICLE_SIGN+"','"+ARTICLE_EXTERNAL+"','"+ARTICLE_EXTERNALURL+"');\" ";
  href += " ><span style='text-decoration:underline;color:green;'>"+title+"</span></a>";
  return href;
},
		         
		         sortable:false
		         },
		         {name: "ARTICLE_PUBTIME",label:"发布时间",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "ARTICLE_PUBNAME",label:"发布者姓名",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "ARTICLE_ISTOP",label:"是否置顶",
		         width: 80,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
    if(cellvalue=="-1"){
       return "<span class=\"label label-danger\">否</span>";
    }else if(cellvalue=="1"){
       return "<span class=\"label label-primary\">是</span>";      
    }
    
},
		         
		         sortable:false
		         },
		         {name: "ARTICLE_ISPUB",label:"是否发布",
		         width: 80,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
    if(cellvalue=="-1"){
       return "<span class=\"label label-danger\">未发布</span>";
    }else if(cellvalue=="1"){
       return "<span class=\"label label-primary\">发布</span>";      
    }
    
},
		         
		         sortable:false
		         },
		         {name: "ARTICLE_CLICKNUM",label:"点击数",
		         width: 70,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "ARTYPE",label:"类型",
		         width: 100,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         },
		         {name: "ARTICLE_EXTERNAL",label:"来源外部",
		         width: 10,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         },
		         {name: "ARTICLE_EXTERNALURL",label:"外部地址",
		         width: 10,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         }
           ]
	 });
});
</script>
</div>
   <div class="ui-layout-west" platundragable="true" compcode="direct_layout">
     <plattag:listgroup onclickfn="reloadArticleDatatable" grouptitle="文章类型组" dyna_interface="articleService.findTypeList" dyna_param="无"></plattag:listgroup>
<script type="text/javascript">
  function reloadArticleDatatable(typeCode){
	if(typeCode!="0"){
		$("input[name='Q_T.ARTICLE_TYPE_EQ']").val(typeCode);
	}else{
		$("input[name='Q_T.ARTICLE_TYPE_EQ']").val("");
	}
	PlatUtil.tableDoSearch("article_datatable","article_search");
}
</script>
</div>
</div>
  </body>
</html>

<script type="text/javascript">
$(function(){
  $("select[name='Q_T.ARTICLE_SITEID_EQ']").change(function() {
		var SITE_ID = PlatUtil.getSelectAttValue("Q_T.ARTICLE_SITEID_EQ","value");
        PlatUtil.reloadSelect("Q_T.ARTICLE_COLUMNID_EQ",{
			dyna_param:SITE_ID
	   });
	});
});
</script>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>单位信息表单</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout" style="overflow-y:auto;">
   <form method="post" class="form-horizontal" compcode="formcontainer" action="system/CompanyController.do?saveOrUpdate" id="CompanyForm" style="">

  <input type="hidden" name="COMPANY_ID" value="${company.COMPANY_ID}">
  <input type="hidden" name="COMPANY_PARENTID" value="${company.COMPANY_PARENTID}">
<div class="form-group plat-form-title" compcode="formtitle" id="">
    <span class="plat-current">
		基本信息
	</span>
</div><div class="form-group" compcode="formgroup">
   <plattag:input name="COMPANY_PARENTNAME" allowblank="false" auth_type="readonly" value="${company.COMPANY_PARENTNAME}" datarule="required;" label_value="上级单位" placeholder="请输入上级单位" comp_col_num="4" label_col_num="2">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:input name="COMPANY_NAME" allowblank="false" auth_type="write" value="${company.COMPANY_NAME}" datarule="required;" maxlength="30" label_value="单位名称" placeholder="请输入单位名称" comp_col_num="4" label_col_num="2">
   </plattag:input>
   <plattag:input name="COMPANY_CODE" allowblank="true" auth_type="write" value="${company.COMPANY_CODE}" datarule="onlyLetterNumberUnderLine;remote[common/baseController.do?checkUnique&amp;VALID_TABLENAME=PLAT_SYSTEM_COMPANY&amp;VALID_FIELDLABLE=单位编码&amp;VALID_FIELDNAME=COMPANY_CODE]" maxlength="30" label_value="单位编码" placeholder="请输入单位编码" comp_col_num="4" label_col_num="2">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div><div class="form-group plat-form-title" compcode="formtitle" id="">
    <span class="plat-current">
		地址信息
	</span>
</div><div class="form-group" compcode="formgroup">
   <plattag:select name="COMPANY_PROVINCECODE" allowblank="true" auth_type="write" value="${company.COMPANY_PROVINCECODE}" istree="false" onlyselectleaf="false" label_value="所属省份" placeholder="请选择所属省份" comp_col_num="3" label_col_num="1" dyna_interface="dicTypeService.findChildren" dyna_param="ADMIN_DIVISION">
   </plattag:select>
   <plattag:select name="COMPANY_CITYCODE" allowblank="true" auth_type="write" value="${company.COMPANY_CITYCODE}" istree="false" onlyselectleaf="false" label_value="所属城市" placeholder="请选择所属城市" comp_col_num="3" label_col_num="1" dyna_interface="dicTypeService.findChildren" dyna_param="${company.COMPANY_PROVINCECODE}">
   </plattag:select>
   <plattag:select name="COMPANY_AREACODE" allowblank="true" auth_type="write" value="${company.COMPANY_AREACODE}" istree="false" onlyselectleaf="false" label_value="所属县区" placeholder="请选择所属县区" comp_col_num="3" label_col_num="1" dyna_interface="dicTypeService.findChildren" dyna_param="${company.COMPANY_CITYCODE}">
   </plattag:select>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:input name="COMPANY_ADRESS" allowblank="true" auth_type="write" value="${company.COMPANY_ADRESS}" maxlength="126" label_value="详细地址" placeholder="请输入详细地址,例如门牌号,楼层等信息" comp_col_num="11" label_col_num="1">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div></form></div>
   <div class="ui-layout-south" platundragable="true" compcode="direct_layout">
   <div class="row" style="height:40px; margin-top: -6px;padding-top: 4px; background: #e5e5e5;" platundragable="true" compcode="buttontoolbar">
     <div class="col-sm-12 text-right">
		<button type="button" onclick="submitBusForm();" id="" class="btn btn-outline btn-primary btn-sm">
			<i class="fa fa-check"></i>&nbsp;提交
		</button>
		<button type="button" onclick="closeWindow();" id="" class="btn btn-outline btn-danger btn-sm">
			<i class="fa fa-times"></i>&nbsp;关闭
		</button>
     </div>
</div>

<script type="text/javascript">
function submitBusForm(){
	if(PlatUtil.isFormValid("#CompanyForm")){
		var url = $("#CompanyForm").attr("action");
		var formData = PlatUtil.getFormEleData("CompanyForm");
		PlatUtil.ajaxProgress({
			url:url,
			params : formData,
			callback : function(resultJson) {
				if (resultJson.success) {
					parent.layer.msg(PlatUtil.SUCCESS_MSG, {icon: 1});
					PlatUtil.setData("submitSuccess",true);
					PlatUtil.closeWindow();
				} else {
					parent.layer.alert(PlatUtil.FAIL_MSG,{icon: 2,resize:false});
				}
			}
		});
	}
}
function closeWindow(){
  PlatUtil.closeWindow();
}

</script>

</div>
</div>
  </body>
</html>

<script type="text/javascript">
$(function(){
   if("${company.COMPANY_CODE}"){
     PlatUtil.changeUICompAuth("readonly","COMPANY_CODE");
   }
	$("select[name='COMPANY_PROVINCECODE']").change(function() {
		var COMPANY_PROVINCECODE = $(this).val();
		PlatUtil.reloadSelect("COMPANY_CITYCODE",{
           dyna_param:COMPANY_PROVINCECODE
        });
	});
  
   $("select[name='COMPANY_CITYCODE']").change(function() {
		var COMPANY_CITYCODE = $(this).val();
		PlatUtil.reloadSelect("COMPANY_AREACODE",{
           dyna_param:COMPANY_CITYCODE
        });
	});
  
});
</script>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>文章表单</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,fancybox,webuploader,ueditor"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,fancybox,webuploader,ueditor"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
   
<div class="plat-wizard">
	<ul class="steps">
		<li class="active">
		<span class="step" nextstep_fnname="savebaseform">1</span>基本信息<span class="chevron"></span>
		</li>	
		<li class="">
		<span class="step" nextstep_fnname="saveotherform">2</span>其它信息<span class="chevron"></span>
		</li>	
		<li class="">
		<span class="step">3</span>附加信息<span class="chevron"></span>
		</li>	
	</ul>
</div>
<div id="wizard-showdiv" style="height: calc(100% - 48px);">
		<div platundragable="true" compcode="platwizard" class="plat-wizard-everystep" id="基本信息" style="height:100%">
   		<div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout" style="overflow-y:auto;">
   <form method="post" class="form-horizontal" compcode="formcontainer" action="" id="baseform" style="">

  <input type="hidden" name="ARTICLE_ID" value="${article.ARTICLE_ID}">
<div class="form-group" compcode="formgroup">
   <plattag:select name="ARTICLE_SITEID" allowblank="false" auth_type="write" value="${article.ARTICLE_SITEID}" istree="false" onlyselectleaf="false" label_value="所属站点" placeholder="请选择站点" comp_col_num="4" label_col_num="2" dyna_interface="webSiteService.findForSelect" dyna_param="无">
   </plattag:select>
   <plattag:select name="ARTICLE_COLUMNID" allowblank="false" auth_type="write" value="${article.ARTICLE_COLUMNID}" istree="false" onlyselectleaf="false" label_value="所属栏目" placeholder="请选择栏目" comp_col_num="4" label_col_num="2" dyna_interface="columnService.findSelectTree" dyna_param="${article.ARTICLE_SITEID}">
   </plattag:select>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:radio name="ARTICLE_TYPE" allowblank="false" auth_type="write" value="${article.ARTICLE_TYPE}" select_first="true" is_horizontal="true" label_value="文章类型" comp_col_num="10" label_col_num="2" dyna_interface="dictionaryService.findList" dyna_param="{TYPE_CODE:'articletype',ORDER_TYPE:'ASC'}">
   </plattag:radio>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:input name="ARTICLE_TITLE" allowblank="false" auth_type="write" value="${article.ARTICLE_TITLE}" datarule="required;" maxlength="254" label_value="标题" placeholder="请输入标题" comp_col_num="10" label_col_num="2">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:radio name="ARTICLE_ISPUB" allowblank="false" auth_type="write" value="${article.ARTICLE_ISPUB}" select_first="true" is_horizontal="true" label_value="是否发布" comp_col_num="4" label_col_num="2" static_values="发布:1,不发布:-1">
   </plattag:radio>
   <plattag:datetime name="ARTICLE_PUBTIME" allowblank="false" auth_type="write" value="${article.ARTICLE_PUBTIME}" format="YYYY-MM-DD hh:mm:ss" istime="false" label_value="发布时间" placeholder="请选择发布时间" label_col_num="2" comp_col_num="4">
   </plattag:datetime>

<script type="text/javascript">
   
</script>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:select name="ARTICLE_TPLID" allowblank="false" auth_type="write" value="${article.ARTICLE_TPLID}" istree="false" onlyselectleaf="false" label_value="绑定模版" placeholder="请选择绑定模版" comp_col_num="4" label_col_num="2" dyna_interface="templateService.findForSelect" dyna_param="无">
   </plattag:select>
   <plattag:radio name="ARTICLE_EXTERNAL" allowblank="false" auth_type="write" value="${article.ARTICLE_EXTERNAL}" select_first="true" is_horizontal="true" label_value="来源外部" comp_col_num="4" label_col_num="2" static_values="否:-1,是:1">
   </plattag:radio>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup" id="externaldiv">
   <plattag:input name="ARTICLE_EXTERNALURL" allowblank="false" auth_type="write" value="${article.ARTICLE_EXTERNALURL}" datarule="required;url;" maxlength="200" label_value="外部地址" placeholder="请输入外部地址" comp_col_num="10" label_col_num="2">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
<label class="col-sm-2 control-label">
标题图片：
</label>
<div class="col-sm-4">
<div class="imageUploadDiv platSingleImgUploadDiv" compcode="singleimgupload" filesinglesizelimit="2097152" style="width:470px;height:320px;" uploadrootfolder="article">
<jsp:include page="/webpages/common/plattagtpl/singleimgupload_tag.jsp">
    <jsp:param name="FILESIGNLE_LIMIT" value="2097152"></jsp:param>
    <jsp:param name="IMG_WIDTH" value="470"></jsp:param>
    <jsp:param name="IMG_HEIGHT" value="320"></jsp:param>
    <jsp:param name="UPLOAD_ROOTFOLDER" value="article"></jsp:param>
    <jsp:param name="DEFAULT_IMGPATH" value="plug-in/platform-1.0/images/articlenophoto.png"></jsp:param>
    <jsp:param name="BIND_ID" value="picker"></jsp:param>
    <jsp:param name="BUS_TABLENAME" value="PLAT_CMS_ARTICLE"></jsp:param>
    <jsp:param name="BUS_RECORDID" value="${article.ARTICLE_ID}"></jsp:param>
    <jsp:param name="FILE_TYPEKEY" value=""></jsp:param>
    <jsp:param name="CONTROL_LABEL" value="标题图片"></jsp:param>
    <jsp:param name="COMP_COL_NUM" value="4"></jsp:param>
    <jsp:param name="LABEL_COL_NUM" value="2"></jsp:param>
</jsp:include>

</div>

</div>
</div>
<div class="hr-line-dashed"></div></form></div>
</div></div>
		<div platundragable="true" compcode="platwizard" class="plat-wizard-everystep" id="其它信息" style="height:100%">
   		<form method="post" class="form-horizontal" compcode="formcontainer" action="" id="otherform" style="">

<div class="form-group" compcode="formgroup">
   <plattag:input name="ARTICLE_TAGS" allowblank="true" auth_type="write" value="${article.ARTICLE_TAGS}" maxlength="510" label_value="TAG标签" placeholder="请输入TAG标签" comp_col_num="10" label_col_num="2">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:textarea name="ARTICLE_SUMMARY" allowblank="true" auth_type="write" value="${article.ARTICLE_SUMMARY}" maxlength="1998" label_value="摘要" placeholder="请输入摘要" comp_col_num="10" label_col_num="2">
   </plattag:textarea>

<script type="text/javascript">

</script>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:input name="ARTICLE_AUTHOR" allowblank="true" auth_type="write" value="${article.ARTICLE_AUTHOR}" maxlength="14" label_value="作者" placeholder="请输入作者" comp_col_num="4" label_col_num="2">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div></form></div>
		<div platundragable="true" compcode="platwizard" class="plat-wizard-everystep" id="附加信息" style="height:100%">
   		</div>
</div>
<script type="text/javascript">
function savebaseform(){
  var result = false;
	if(PlatUtil.isFormValid("#baseform")){
		var formData = PlatUtil.getFormEleData("baseform");
      var PHOTO_JSON = PlatUtil.getSingleImgWebUploaderValue("picker");
      formData.PHOTO_JSON = PHOTO_JSON;
		PlatUtil.ajaxProgress({
			url:"cms/ArticleController.do?saveOrUpdate",
			async:"-1",
			params : formData,
			callback : function(resultJson) {
				if (resultJson.success) {
              var ARTICLE_ID = resultJson.ARTICLE_ID;
              $("input[name='ARTICLE_ID']").val(ARTICLE_ID);
					result = true;
				} else {
					parent.layer.alert(PlatUtil.FAIL_MSG,{icon: 2,resize:false});
					result = false;
				}
			}
		});
	}
	return result;
}
function saveotherform(){
  var result = false;
	if(PlatUtil.isFormValid("#otherform")){
		var formData = PlatUtil.getFormEleData("otherform");
      var ARTICLE_ID = $("input[name='ARTICLE_ID']").val();
      var ARTICLE_TYPE = PlatUtil.getCheckRadioTagValue("ARTICLE_TYPE","VALUE");
      formData.ARTICLE_ID = ARTICLE_ID;
		PlatUtil.ajaxProgress({
			url:"cms/ArticleController.do?saveOther",
			async:"-1",
			params : formData,
			callback : function(resultJson) {
				if (resultJson.success) {
             PlatUtil.ajaxProgress({
						url:"cms/ArticleController.do?goAttach",
						async:"-1",
						params :{
                  ARTICLE_TYPE:ARTICLE_TYPE,
                  ARTICLE_ID:ARTICLE_ID
                },
						callback : function(resultHtml) {
							if(resultHtml&&resultHtml!=""){
								$("#附加信息").html(resultHtml);
							}
							result = true;
						}
					});
				} else {
					parent.layer.alert(PlatUtil.FAIL_MSG,{icon: 2,resize:false});
					result = false;
				}
			}
		});
	}
	return result;
}

</script>
</div>
   <div class="ui-layout-south" platundragable="true" compcode="direct_layout">
   <div class="row" style="height:40px; margin-top: -6px;padding-top: 4px; background: #e5e5e5;" platundragable="true" compcode="buttontoolbar">
     <div class="col-sm-12 text-right">
		<button type="button" onclick="wizardPre();" platreskey="" id="WIZARD_PREBTN" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-arrow-left"></i>&nbsp;上一步
		</button>
		<button type="button" onclick="wizardNext();" platreskey="" id="WIZARD_NEXTBTN" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-arrow-right"></i>&nbsp;下一步
		</button>
		<button type="button" onclick="submitform();" platreskey="" id="WIZARD_OVERBTN" disabled="disabled" class="btn btn-outline btn-info btn-sm">
			<i class="fa fa-check"></i>&nbsp;完成
		</button>
     </div>
</div>

<script type="text/javascript">
function wizardPre(){
  PlatUtil.wizardPre();
}
function wizardNext(){
  PlatUtil.wizardNext();
}
function submitform(){
	if(PlatUtil.isFormValid("#attachform")){
		var formData = PlatUtil.getFormEleData("attachform");
     var ARTICLE_ID = $("input[name='ARTICLE_ID']").val();
     formData.ARTICLE_ID = ARTICLE_ID;
		PlatUtil.ajaxProgress({
          url:"cms/ArticleController.do?saveAttach",
          params : formData,
          callback : function(resultJson) {
              if (resultJson.success) {
                  parent.layer.msg(PlatUtil.SUCCESS_MSG, {icon: 1});
                  PlatUtil.setData("submitSuccess",true);
                  PlatUtil.closeWindow();
              } else {
                  parent.layer.alert(PlatUtil.FAIL_MSG,{icon: 2,resize:false});
              }
          }
      });
	}
}

</script>

</div>
</div>
  </body>
</html>

<script type="text/javascript">
$(function(){
  $("select[name='ARTICLE_SITEID']").change(function() {
		var SITE_ID = PlatUtil.getSelectAttValue("ARTICLE_SITEID","value");
        PlatUtil.reloadSelect("ARTICLE_COLUMNID",{
			dyna_param:SITE_ID
	   });
	});
  var ARTICLE_EXTERNAL = PlatUtil.getCheckRadioTagValue("ARTICLE_EXTERNAL","VALUE");
  if(ARTICLE_EXTERNAL=="1"){
    $("#externaldiv").attr("style","");
  }else{
    $("#externaldiv").attr("style","display:none;");
  }
  $("input[name='ARTICLE_EXTERNAL']").change(function() {
      var ARTICLE_EXTERNAL = PlatUtil.getCheckRadioTagValue("ARTICLE_EXTERNAL","value");
      if(ARTICLE_EXTERNAL=="1"){
        $("#externaldiv").attr("style","");
      }else{
        $("#externaldiv").attr("style","display:none;");
      }
   });
 
  
});
</script>

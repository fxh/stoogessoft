<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>请假申请单</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <plattag:resources restype="css" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,webuploader,flowdesign"></plattag:resources>
    <plattag:resources restype="js" loadres="bootstrap-checkbox,jquery-ui,jqgrid,jedate,select2,nicevalid,webuploader,flowdesign"></plattag:resources>
  </head>
  
  <body>
    <script type="text/javascript">
	$(function(){
		  PlatUtil.initUIComp();
	});
	</script>
    <div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-north" platundragable="true" compcode="direct_layout">
      <plattag:flowbtntoolbar></plattag:flowbtntoolbar>
</div>
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout" style="overflow-y:auto;">
   <div class="tabs-container" platundragable="true" compcode="bootstraptab" id="leavetab" style="width:100%;height:100%;">
	<ul class="nav nav-tabs">
		<li class="active" subtabid="tab1" onclick="PlatUtil.onBootstrapTabClick('leavetab','tab1','','1');"><a data-toggle="tab" href="#tab1" aria-expanded="true">请假申请单</a></li>
		<li class="" subtabid="tab2" onclick="PlatUtil.onBootstrapTabClick('leavetab','tab2','','1');"><a data-toggle="tab" href="#tab2" aria-expanded="false">审批日志</a></li>
		<li class="" subtabid="tab3" onclick="PlatUtil.onBootstrapTabClick('leavetab','tab3','','-1');"><a data-toggle="tab" href="#tab3" aria-expanded="false">流程图</a></li>
		<li class="" subtabid="tab4" onclick="PlatUtil.onBootstrapTabClick('leavetab','tab4','refreshUpload','1');"><a data-toggle="tab" href="#tab4" aria-expanded="false">附件列表</a></li>
	</ul>
	<div class="tab-content" platundragable="true" compcode="bootstraptab" style="height: calc(100% - 42px);">
		<div id="tab1" class="tab-pane active" style="height:100%;" platundragable="true" compcode="bootstraptab">
		<form method="post" class="form-horizontal" compcode="formcontainer" action="" id="LeaveInfoForm" style="">

  <input type="hidden" name="LEAVEINFO_ID" value="${leaveInfo.LEAVEINFO_ID}">
<div class="form-group" compcode="formgroup">
   <plattag:input name="LEAVEINFO_NAME" allowblank="false" auth_type="write" value="${leaveInfo.LEAVEINFO_NAME}" datarule="required;" maxlength="14" label_value="请假人姓名" placeholder="请输入请假人姓名" comp_col_num="4" label_col_num="2">
   </plattag:input>
   <plattag:select name="LEAVEINFO_TYPE" allowblank="false" auth_type="write" value="${leaveInfo.LEAVEINFO_TYPE}" istree="false" onlyselectleaf="false" label_value="请假类型" placeholder="请选择请假类型" comp_col_num="4" label_col_num="2" static_values="年休:1,事假:2">
   </plattag:select>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:datetime name="LEAVEINFO_CREATETIME" allowblank="false" auth_type="write" value="${leaveInfo.LEAVEINFO_CREATETIME}" format="YYYY-MM-DD hh:mm:ss" istime="true" label_value="申请时间" placeholder="请选择请假申请时间" label_col_num="2" comp_col_num="4" defaultnow="1">
   </plattag:datetime>

<script type="text/javascript">
   
</script>
   <plattag:input name="LEAVEINFO_DAYS" allowblank="false" auth_type="write" value="${leaveInfo.LEAVEINFO_DAYS}" datarule="required;positiveInteger;" maxlength="9" label_value="请假天数" placeholder="请输入请假天数" comp_col_num="4" label_col_num="2">
   </plattag:input>
</div>
<div class="hr-line-dashed"></div><div class="form-group" compcode="formgroup">
   <plattag:textarea name="LEAVEINFO_REASON" allowblank="true" auth_type="write" value="${leaveInfo.LEAVEINFO_REASON}" maxlength="1998" label_value="请假事由" placeholder="请输入请假事由" comp_col_num="10" label_col_num="2">
   </plattag:textarea>

<script type="text/javascript">

</script>
</div>
<div class="hr-line-dashed"></div></form></div>
		<div id="tab2" class="tab-pane " style="height:100%;" platundragable="true" compcode="bootstraptab">
		   <jsp:include page="<%=&quot;/appmodel/DesignController/includeUI.do&quot;%>">
    <jsp:param value="flowtasklist" name="DESIGN_CODE"></jsp:param>
</jsp:include>
</div>
		<div id="tab3" class="tab-pane " style="height:100%;" platundragable="true" compcode="bootstraptab">
		<div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
      <jsp:include page="/webpages/background/workflow/flowdef/flownodemonitor.jsp"></jsp:include>
</div>
</div></div>
		<div id="tab4" class="tab-pane " style="height:100%;" platundragable="true" compcode="bootstraptab">
		<div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout">
   <jsp:include page="/webpages/common/plattagtpl/multifileupload_tag.jsp">
    <jsp:param name="FILESIGNLE_LIMIT" value="10485760"></jsp:param>
    <jsp:param name="ALLOW_FILEEXTS" value="xls,xlsx"></jsp:param>
    <jsp:param name="UPLOAD_ROOTFOLDER" value="test"></jsp:param>
    <jsp:param name="BIND_ID" value="test"></jsp:param>
    <jsp:param name="BUS_TABLENAME" value="PLAT_DEMO_LEAVEINFO"></jsp:param>
    <jsp:param name="BUS_RECORDID" value="${leaveInfo.LEAVEINFO_ID}"></jsp:param>
    <jsp:param name="FILE_TYPEKEY" value=""></jsp:param>
    <jsp:param name="FILE_RIGHTS" value="upload,del"></jsp:param>
    <jsp:param name="FILE_UPSERVER" value="1"></jsp:param>
    <jsp:param name="FILE_UPURL" value=""></jsp:param>
    <jsp:param name="IS_INIT" value="-1"></jsp:param>
</jsp:include>
</div>
</div></div>
	</div>
</div>

<script type="text/javascript">


function 函数命名(subTabId){

}
function refreshUpload(subTabId){
	PlatUtil.initMultiFileUploader();
}
</script></div>
</div>
  </body>
</html>

<script type="text/javascript">

</script>

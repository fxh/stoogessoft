<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<div class="plat-directlayout" style="height:100%" platundragable="true" compcode="direct_layout">
   <div class="ui-layout-center" platundragable="true" compcode="direct_layout" style="overflow-y:auto;">
   <div class="panel-Title">
	<h5 id="flowtasklist_datatablepaneltitle">审核日志列表</h5>
</div>
<div class="titlePanel" style="display: none;">
	<div class="title-search form-horizontal" id="flowtasklist_search" formcontrolid="402881e65c28ea67015c290147780032">
		<table style="display: none;">
			<tr>
				<td>查询条件</td>
				<td style="padding-left: 5px;">
					<div class="table-filter">
                			  <input type="hidden" name="Q_T.TASK_EXEID_EQ" value="${jbpmFlowInfo.jbpmExeId!=null?jbpmFlowInfo.jbpmExeId:'-1'}">
						<input type="text" style="width: 200px; " onclick="PlatUtil.showOrHideSearchTable(this);" class="table-form-control" name="search" readonly="readonly">
						<div class="table-filter-list" style="width: 420px; display: none;max-height: 280px;">
							<div class="table-filter-list-bottom">
								<a onclick="PlatUtil.tableSearchReset('flowtasklist_search');" class="btn btn-default" href="javascript:void(0);"> 重 置</a> 
								<a onclick="PlatUtil.tableDoSearch('flowtasklist_datatable','flowtasklist_search',true);" class="btn btn-primary" href="javascript:void(0);"> 查 询</a>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="gridPanel">
	<table id="flowtasklist_datatable"></table>
</div>
<div id="flowtasklist_datatable_pager"></div>
<script type="text/javascript">

$(function(){
	PlatUtil.initJqGrid({
		  tableId:"flowtasklist_datatable",
		  searchPanelId:"flowtasklist_search",
		  url: "appmodel/CommonUIController.do?tabledata&FORMCONTROL_ID=402881e65c28ea67015c290147780032",
		  nopager:true,
		  rowNum : -1,
		  ondblClickRow:function(rowid,iRow,iCol,e){
				  
		  },
		  colModel: [
		         {name: "TASK_ID",label:"任务ID",
		         width: 100,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         },
		         {name: "TASK_NODENAME",label:"环节名称",
		         width: 100,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "TASK_HANDLECOMPANYNAMES",label:"办理单位",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "TASK_HANDLENAMES",label:"办理人",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "TASK_CREATETIME",label:"开始时间",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "TASK_STATUS",label:"任务状态",
		         width: 100,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
  return PlatUtil.formatTaskStatus(cellvalue,options,rowObject);
},
		         
		         sortable:false
		         },
		         {name: "TASK_ENDTIME",label:"结束时间",
		         width: 150,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "TASK_LEFTDAYS",label:"剩余时限天数",
		         width: 150,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
  return PlatUtil.formatTaskLeftDays(cellvalue,options,rowObject);
},
		         
		         sortable:false
		         },
		         {name: "TASK_OPINION",label:"审核意见",
		         width: 200,align:"left",
		         
		         
		         
		         sortable:false
		         },
		         {name: "TASK_HANDLEDESC",label:"执行动作",
		         width: 150,align:"left",
		         
		         formatter:function(cellvalue, options, rowObject){
	//rowObject对象代表这行的数据对象
   var TASK_REALHANDLERNAME = rowObject.TASK_REALHANDLERNAME;
   var TASK_HANDLEDESC = rowObject.TASK_HANDLEDESC;
   if(TASK_REALHANDLERNAME){
      return TASK_REALHANDLERNAME+"【"+TASK_HANDLEDESC+"】";
   }else{
     return "";
   }
},
		         
		         sortable:false
		         },
		         {name: "TASK_REALHANDLERNAME",label:"真正办理人姓名",
		         width: 100,align:"left",
		         hidden:true,
		         
		         
		         sortable:false
		         }
           ]
	 });
});
</script>
</div>
</div>

<script type="text/javascript">

</script>

<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<%@ taglib prefix="plattag" uri="/plattag"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>研发二部基础开发框架基线版</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<plattag:resources restype="css" loadres="bootstrap-submenu,plat-main,slimscroll"></plattag:resources>
	<style type="text/css">
	.plat-gray-bg {
	    background-color: #f9f9f9;
	    overflow: hidden;
	}
	.plat-wrapper-content {
	    margin: 10px 10px 0px; padding: 0px;
	}
	</style>

  </head>
  
<body style="overflow: hidden;">
	<input type="hidden" id="__BACKPLATUSERJSON" value="${sessionScope.__BACKPLATUSERJSON}">
	<div id="ajax-loader"
		style="cursor: progress; position: fixed; top: -50%; left: -50%; width: 200%; height: 200%; background: #fff; z-index: 10000; overflow: hidden;">
		<img src="plug-in/platform-1.0/images/ajax-loader.gif"
			style="position: absolute; top: 0; left: 0; right: 0; bottom: 0; margin: auto;" />
	</div>
	<div class="eui-content" style="height:100%;">
		<div class="eui-side">
		  <div id="navDiv" style="overflow-y: auto; overflow-x: hidden; height: 100%; width: 100px;">
		  <div class="slimscroll">
		<%--开始引入菜单界面 --%>
		<plattag:platbackmenu userId="${sessionScope.__BACKPLATUSER.SYSUSER_ID}"></plattag:platbackmenu>
		</div>
		<%--结束引入菜单界面 --%>
		   </div>
		</div>
		<div class="eui-main" style="margin-left:100px; position: relative; z-index: 999;">
		<div class="plat-Head">
			<div class="logo">
				<img src="plug-in/platform-1.0/images/logo.png" />
			</div>
			<div class="left-bar" id="left-bar">
			</div>
			<div class="right-bar">
				<ul>
					<li class="dropdown user user-menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <img
							src="plug-in/platform-1.0/images/user2-160x160.jpg"
							onerror="javascript: this.src = 'plug-in/platform-1.0/images/user2-160x160.jpg'"
							class="user-image" alt="User Image"> <span class="hidden-xs">${sessionScope.__BACKPLATUSER.SYSUSER_NAME}</span>
					</a>
						<ul class="dropdown-menu pull-right">
							<li><a href="javascript:void(0);" onclick="goUserInfo();"><i class="fa fa-user"></i>个人信息</a></li>
							<li class="divider"></li>
							<li><a onclick="PlatUtil.exitBackgroudSystem();"><i
									class="ace-icon fa fa-power-off"></i>安全退出</a></li>
						</ul></li>
				</ul>
			</div>
		</div>
		<div class="lea-tabs">
			<div class="menuTabs">
				<div class="page-tabs-content">
					<a href="javascript:;" class="menuTab active"
						data-id="/Home/AdminPrettyDesktop"><i class="fa fa-home"></i>欢迎首页</a>
				</div>
			</div>
			<div class="tabs-right-tool">
				<button class="roll-nav tabLeft">
					<i class="fa fa fa-chevron-left"></i>
				</button>
				<button class="roll-nav tabRight">
					<i class="fa fa fa-chevron-right" style="margin-left: 3px;"></i>
				</button>
				<button class="roll-nav fullscreen" onclick="PlatUtil.fullscreen(this);">
					<i class="fa fa-arrows-alt"></i>
				</button>
				<div class="dropdown">
					<button class="roll-nav dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-gear "></i>
					</button>
					<ul class="dropdown-menu dropdown-menu-right"
						style="margin-top:40px">
						<li><a class="tabReload" href="javascript:;">刷新当前</a></li>
						<li><a class="tabCloseCurrent" href="javascript:;">关闭当前</a></li>
						<li><a class="tabCloseAll" href="javascript:;">全部关闭</a></li>
						<li><a class="tabCloseOther" href="javascript:;">除此之外全部关闭</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="content-iframe plat-gray-bg">
			<div id="mainContent" class="lea-content  plat-wrapper-content">
				<iframe class="Plat_iframe" width="100%" height="100%"
					src="appmodel/PortalThemeController.do?goportal" frameborder="0"
					data-id="/Home/AdminPrettyDesktop"></iframe>
			</div>
		</div>
		<div class="footer" id="rightCopyFoot">
			<div style="float: left; width: 30%;">
				&nbsp;技术支持：<a href="http://www.evecom.net/"
					target="_blank" style="color: white;">长威信息科技发展股份有限公司</a>
			</div>
			<div style="float: left; width: 40%; text-align: center;">
				Copyright © 2017 - 2020 By EVECOM</div>
		</div>

		<div id="loading_background" class="loading_background"
			style="display: none;"></div>
		<div id="loading_manage">正在拼了命为您加载…</div>
		</div>
	</div>
</body>
</html>
<plattag:resources restype="js" loadres="bootstrap-submenu,jquery-cookie,plat-util,plat-tab,layer,slimscroll">
</plattag:resources>
<script>
	$(function() {
		PlatUtil.initPlatFrame();
		$("#navDiv").mouseenter(function(){
		 	$(this).css("width","10000px");
		 	//$(".eui-side").css("overflow","auto");
		});
		$("#navDiv").mouseleave(function(){
			$(this).css("width","100px");
			//$(".eui-side").css("overflow","hidden");
		});
		
		$(".slimscroll").slimScroll({
	        width: "auto", //可滚动区域宽度
	        height: "100%" //可滚动区域高度
	    });

	});
	
	function goUserInfo(){
		PlatTab.newTab({
			id : "userInfo",
			title : "个人信息",
			closed : true,
			icon : "fa fa-user",
			url : "system/SysUserController.do?goUserInfo"
		});
	}
	
</script>

/*
 * Copyright (c) 2005, 2018, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.core.util;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONPath;

/**
 * @author 胡裕
 *
 * 
 */
public class PlatJsonUtil {

    /**
     * 在一个JSON字符串中查询单条有效数据
     * @param keyName 字段的名称
     * @param keyValue 字段值
     * @param jsonDataSource JSON数据源
     * @return
     */
    public static Map getUniqueData(String keyName,String keyValue,String jsonDataSource){
        StringBuffer pathExp = new StringBuffer("$[?(@.");
        pathExp.append(keyName).append("='").append(keyValue).append("')]");
        List<Map> list = JSON.parseArray(JSONPath.eval(JSON.parseArray(jsonDataSource),pathExp.toString())
                .toString(), Map.class);
        if(list!=null&&list.size()>0){
            return list.get(0);
        }else{
            return null;
        }
    }
}

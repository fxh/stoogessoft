/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.core.util;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;









import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import com.stooges.core.model.PagingBean;

/**
 * 描述 使用dbutils来操纵数据库工具类
 * 
 * @author 胡裕
 * @version 1.0
 * @created 2014年10月28日 上午9:14:57
 */
public class PlatDbUtil {

    /**
     * 
     * 描述 执行更新数据语句
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午11:01:39
     * @param sql
     * @param params
     * @return
     */
    public static int update(Connection conn, String sql, Object[] params) {
        return update(conn, sql, params, true);
    }

    /**
     * 
     * 描述 执行更新数据语句
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午11:01:39
     * @param sql
     * @param params
     * @return
     */
    public static int update(Connection conn, String sql, Object[] params, boolean isClose) {
        try {
            QueryRunner qRunner = new QueryRunner();
            int n = 0;
            if (params != null && params.length > 0) {
                n = qRunner.update(conn, sql, params);
            } else {
                n = qRunner.update(conn, sql);
            }
            return n;
        } catch (SQLException e) {
            try {
                DbUtils.rollback(conn);
            } catch (SQLException e1) {
                PlatLogUtil.printStackTrace(e1);
            }
            PlatLogUtil.printStackTrace(e);
        } finally {
            if (isClose)
                DbUtils.closeQuietly(conn);
        }
        return 0;
    }

    /**
     * 
     * 描述 根据sql获取唯一对象
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午10:41:38
     * @param sql
     * @param params
     * @return
     */
    public static Object getObjectBySql(Connection conn, String sql, Object[] params) {
        return getObjectBySql(conn, sql, params, true);
    }

    /**
     * 
     * 描述 根据sql获取唯一对象
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午10:41:38
     * @param sql
     * @param params
     * @param closeConn
     *            true查询后关闭连接
     * @return
     */
    public static Object getObjectBySql(Connection conn, String sql, Object[] params, boolean closeConn) {
        ResultSetHandler rsh = new ScalarHandler();
        try {
            QueryRunner qRunner = new QueryRunner();
            Object result = null;
            if (params != null && params.length > 0) {
                result = qRunner.query(conn, sql, params, rsh);
            } else {
                result = qRunner.query(conn, sql, rsh);
            }
            return result;
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        } finally {
//            DbUtils.closeQuietly(conn);
        }
        return null;

    }

    /**
     * 
     * 描述 根据sql语句获取MAP
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午10:40:01
     * @param sql
     * @param params
     * @return
     */
    public static Map<String, Object> getMapBySql(Connection conn, String sql, Object[] params) {
        return getMapBySql(conn, sql, params, false);
    }

    /**
     * 
     * 描述 根据sql语句获取MAP
     * 
     * @author Derek Zhang
     * @created 2015年12月31日 下午2:30:13
     * @param sql
     * @param params
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getMapBySql(Connection conn, String sql, Object[] params, boolean isCloseDb) {
        ResultSetHandler rsh = new MapHandler();
        try {
            QueryRunner qRunner = new QueryRunner();
            Map<String, Object> map = null;
            if (params != null && params.length > 0) {
                map = (Map<String, Object>) qRunner.query(conn, sql, params, rsh);
            } else {
                map = (Map<String, Object>) qRunner.query(conn, sql, rsh);
            }
            if (map == null || map.isEmpty())
                return null;
            Iterator<Map.Entry<String, Object>> entries = map.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Object> entry = entries.next();
                if (entry.getValue() instanceof Clob) {
                    map.put(entry.getKey(), PlatStringUtil.clobToString((Clob) entry.getValue()));
                } else if (entry.getValue() instanceof Blob) {
                    map.put(entry.getKey(), PlatStringUtil.blobToString((Blob) entry.getValue(), "GBK"));
                }
            }
            return map;
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        } finally {
            if (isCloseDb){
                DbUtils.closeQuietly(conn);
            }
        }
        return null;
    }

    /**
     * 
     * 描述 根据sql语句获取MAP
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午10:40:01
     * @param sql
     * @param params
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getMapBySqlBlob(Connection conn, String sql, Object[] params) {
        ResultSetHandler rsh = new MapHandler();
        try {
            QueryRunner qRunner = new QueryRunner();
            Map<String, Object> map = null;
            if (params != null && params.length > 0) {
                map = (Map<String, Object>) qRunner.query(conn, sql, params, rsh);
            } else {
                map = (Map<String, Object>) qRunner.query(conn, sql, rsh);
            }
            Iterator<Map.Entry<String, Object>> entries = map.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Object> entry = entries.next();
                if (entry.getValue() instanceof Clob) {
                    map.put(entry.getKey(), (Clob) entry.getValue());
                } else if (entry.getValue() instanceof Blob) {
                    map.put(entry.getKey(), (Blob) entry.getValue());
                }
            }
            return map;
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        } finally {
        }
        return null;
    }

    /**
     * 
     * 描述 根据SQL语句获取数量值
     * 
     * @author 胡裕
     * @created 2014年10月28日 下午3:46:08
     * @param sql
     * @return
     */
    public static int getCount(Connection conn, String sql, Object[] params) {
        StringBuffer newSql = new StringBuffer("select count(*) from (").append(sql).append(") tmp_count_t ");
        return Integer.parseInt(PlatDbUtil.getObjectBySql(conn, newSql.toString(), params).toString());
    }
    
    /**
     * 获取分页的SQL语句面向ORACLE
     * @param sql
     * @param pb
     * @return
     */
    public static String getPagingBeanSqlForOracle(String sql,PagingBean pb){
        int startIndex = pb.getStart() + 1;
        int endIndex = startIndex + pb.getPageSize() - 1;
        String newSql = "select * from (select rOraclePageSQL.*,ROWNUM as currentRow from ("
                + sql
                + ") rOraclePageSQL where rownum <="
                + endIndex
                + ") where currentRow>=" + startIndex;
        int totalPage = (pb.getTotalItems()+pb.getPageSize()-1)/pb.getPageSize(); 
        pb.setTotalPage(totalPage);
        return newSql;
    }

    /**
     * 
     * 描述
     * 
     * @author 胡裕
     * @created 2014年10月28日 下午4:08:48
     * @param sql
     * @param params
     * @param pb
     * @param orderSql
     * @return
     */
    public static List<Map<String, Object>> findBySql(Connection conn, String sql, Object[] params, PagingBean pb,
            String dbType) {
        int totalCount = PlatDbUtil.getCount(conn, sql, params);
        pb.setTotalItems(totalCount);
        ResultSetHandler rsh = new MapListHandler();
        int pageSize = pb.getPageSize();
        String newSql = null;
        if("ORACLE".equals(dbType)){
            newSql = PlatDbUtil.getPagingBeanSqlForOracle(sql, pb);
        }
        try {
            QueryRunner qRunner = new QueryRunner();
            List<Map<String, Object>> list = null;
            if (params != null && params.length > 0) {
                list = (List<Map<String, Object>>) qRunner.query(conn, newSql.toString(), params, rsh);
            } else {
                list = (List<Map<String, Object>>) qRunner.query(conn, newSql.toString(), rsh);
            }
            return list;
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        }
        return null;
    }

    /**
     * 
     * 描述 转换大字段
     * 
     * @author 胡裕
     * @created 2015年10月16日 上午10:11:34
     * @param list
     * @return
     */
    private static List<Map<String, Object>> conventClobAndBlob(List<Map<String, Object>> list) {
        for (Map<String, Object> map : list) {
            Iterator<Map.Entry<String, Object>> entries = map.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Object> entry = entries.next();
                if (entry.getValue() instanceof Clob) {
                    map.put(entry.getKey(), PlatStringUtil.clobToString((Clob) entry.getValue()));
                } else if (entry.getValue() instanceof Blob) {
                    map.put(entry.getKey(), PlatStringUtil.blobToString((Blob) entry.getValue(), "GBK"));
                }
            }
        }
        return list;
    }
    

    /**
     * 
     * 描述 根据SQL语句获取列表数据
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午9:32:46
     * @param sql
     * @param params
     * @return
     */
    public static List<Map<String, Object>> findBySql(Connection conn, String sql, Object[] params) {
        ResultSetHandler rsh = new MapListHandler();
        try {
            QueryRunner qRunner = new QueryRunner();
            List<Map<String, Object>> list = null;
            if (params != null && params.length > 0) {
                list = (List<Map<String, Object>>) qRunner.query(conn, sql, params, rsh);
            } else {
                list = (List<Map<String, Object>>) qRunner.query(conn, sql, rsh);
            }
            return PlatDbUtil.conventClobAndBlob(list);
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        } 
        return null;
    }


    /**
     * 
     * 描述 根据SQL语句获取列表数据
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午9:32:46
     * @param sql
     * @param params
     * @return
     */
    public static List<Map<String, Object>> findBySqlClob(Connection conn, String sql, Object[] params) {
        ResultSetHandler rsh = new MapListHandler();
        try {
            QueryRunner qRunner = new QueryRunner();
            List<Map<String, Object>> list = null;
            if (params != null && params.length > 0) {
                list = (List<Map<String, Object>>) qRunner.query(conn, sql, params, rsh);
            } else {
                list = (List<Map<String, Object>>) qRunner.query(conn, sql, rsh);
            }
            return PlatDbUtil.conventClobAndBlob(list);
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        }
        return null;
    }

    /**
     * 
     * 描述 获取连接对象
     * 
     * @author 胡裕
     * @created 2014年10月28日 上午9:33:17
     * @return
     * @throws SQLException
     */
    public static Connection getConnect(String dbUrl, String username, String password) throws SQLException {
        Connection conn = DriverManager.getConnection(dbUrl, username, password);
        return conn;
    }
    
}

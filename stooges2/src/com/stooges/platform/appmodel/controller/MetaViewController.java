/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.DataSrcService;
import com.stooges.platform.appmodel.service.DbManagerService;
import com.stooges.platform.appmodel.service.MetaViewService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 元数据视图业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-21 09:03:25
 */
@Controller  
@RequestMapping("/resledger/MetaViewController")  
public class MetaViewController extends BaseController {
    /**
     * 
     */
    @Resource
    private MetaViewService metaViewService;
    /**
     * 
     */
    @Resource
    private DbManagerService dbManagerService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    /**
     * 
     */
    @Resource
    private DataSrcService dataSrcService;
    
    /**
     * 验证表名的有效性
     * @param request
     * @param response
     */
    @RequestMapping(params = "checkValid")
    public void checkValid(HttpServletRequest request,
            HttpServletResponse response) {
        String VALID_FIELDNAME = request.getParameter("VALID_FIELDNAME");
        String VALID_FIELDVALUE = request.getParameter(VALID_FIELDNAME);
        String viewName = VALID_FIELDVALUE.toUpperCase();
        Map<String,String> result = new HashMap<String,String>();
        boolean isExists = metaViewService.isExistsView(viewName);
        if(isExists){
            result.put("error", "该视图已经存在配置,请重新输入!");  
        }else{
            boolean isExistInDb = dbManagerService.isExistsView(viewName, true);
            if(!isExistInDb){
                result.put("error", "在数据库中未查询到该视图,请重新输入!");   
            }
        }
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 删除元数据视图数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        metaViewService.deleteRecords("PLAT_RESLEDGER_VIEW","VIEW_ID",selectColValues.split(","));
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改元数据视图数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> metaView = PlatBeanUtil.getMapFromRequest(request);
        metaView = metaViewService.saveOrUpdate("PLAT_RESLEDGER_VIEW",
                metaView,AllConstants.IDGENERATOR_UUID,null);
        metaView.put("success", true);
        this.printObjectJsonString(metaView, response);
    }
    
    /**
     * 跳转到元数据视图表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String VIEW_ID = request.getParameter("VIEW_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> metaView = null;
        if(StringUtils.isNotEmpty(VIEW_ID)){
            metaView = this.metaViewService.getRecord("PLAT_RESLEDGER_VIEW"
                    ,new String[]{"VIEW_ID"},new Object[]{VIEW_ID});
        }else{
            metaView = new HashMap<String,Object>();
        }
        request.setAttribute("metaView", metaView);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 跳转到详细页界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goDetail")
    public ModelAndView goDetail(HttpServletRequest request) {
        String VIEW_ID = request.getParameter("VIEW_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> metaView = null;
        if(StringUtils.isNotEmpty(VIEW_ID)){
            metaView = this.metaViewService.getRecord("PLAT_RESLEDGER_VIEW"
                    ,new String[]{"VIEW_ID"},new Object[]{VIEW_ID});
            //获取视图名称
            String VIEW_NAME = (String) metaView.get("VIEW_NAME");
            String VIEW_DATASRCID = (String) metaView.get("VIEW_DATASRCID");
            Map<String,Object> viewInfo = dataSrcService.getViewInfo(VIEW_DATASRCID, VIEW_NAME);
            String textCode = (String) viewInfo.get("TEXT");
            metaView.put("VIEW_CODE", textCode);
            Map<String,Object> dataSrc = this.metaViewService.getRecord("PLAT_RESLEDGER_DATASRC"
                    ,new String[]{"DATASRC_ID"},new Object[]{VIEW_DATASRCID});
            String DATASRC_NAME = (String) dataSrc.get("DATASRC_NAME");
            metaView.put("VIEW_DATASRCNAME", DATASRC_NAME);
        }
        request.setAttribute("metaView", metaView);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.BrowserUtils;
import com.stooges.core.util.PlatAppUtil;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatLogUtil;
import com.stooges.core.util.PlatStringUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.AppSerService;
import com.stooges.platform.appmodel.service.InvokelogService;
import com.stooges.platform.appmodel.service.RedisService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 APP服务业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-08-02 17:11:23
 */
@Controller  
@RequestMapping("/dataset/AppSerController")  
public class AppSerController extends BaseController {
    /**
     * 
     */
    @Resource
    private AppSerService appSerService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    
    /**
     * 
     */
    @Resource
    private InvokelogService invokelogService;
    /**
     * 
     */
    @Resource
    private RedisService redisService;
    
    /**
     * 删除APP服务数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        appSerService.deleteRecords("MZT_DATASET_APPSER","APPSER_ID",selectColValues.split(","));
        sysLogService.saveBackLog("APP服务管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+selectColValues+"]的APP服务", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改APP服务数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> appSer = PlatBeanUtil.getMapFromRequest(request);
        String APPSER_ID = (String) appSer.get("APPSER_ID");
        appSer = appSerService.saveOrUpdate("MZT_DATASET_APPSER",
                appSer,AllConstants.IDGENERATOR_UUID,null);
        if(StringUtils.isNotEmpty(APPSER_ID)){
            sysLogService.saveBackLog("APP服务管理",SysLogService.OPER_TYPE_EDIT,
                    "修改了ID为["+APPSER_ID+"]APP服务", request);
        }else{
            APPSER_ID = (String) appSer.get("APPSER_ID");
            sysLogService.saveBackLog("APP服务管理",SysLogService.OPER_TYPE_ADD,
                    "新增了ID为["+APPSER_ID+"]APP服务", request);
        }
        appSer.put("success", true);
        this.printObjectJsonString(appSer, response);
    }
    
    /**
     * 跳转到APP服务表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String APPSER_ID = request.getParameter("APPSER_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> appSer = null;
        if(StringUtils.isNotEmpty(APPSER_ID)){
            appSer = this.appSerService.getRecord("MZT_DATASET_APPSER"
                    ,new String[]{"APPSER_ID"},new Object[]{APPSER_ID});
        }else{
            appSer = new HashMap<String,Object>();
        }
        request.setAttribute("appSer", appSer);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 
     * @param appSer
     * @param dataResult
     * @param errorLog
     * @return
     */
    private Map<String,Object> invokeDyna(Map<String,Object> appSer,
            Map<String,Object> dataResult,String errorLog,
            Map<String,Object> result,HttpServletRequest request,Map<String,Object> postParam){
        String APPSER_JAVAINTER = (String) appSer.get("APPSER_JAVAINTER");
        String beanId = APPSER_JAVAINTER.split("[.]")[0];
        String method = APPSER_JAVAINTER.split("[.]")[1];
        Object serviceBean = PlatAppUtil.getBean(beanId);
        if (serviceBean != null) {
            Method invokeMethod;
            try {
                invokeMethod = serviceBean.getClass().getDeclaredMethod(method,
                        new Class[] { HttpServletRequest.class,Map.class});
                dataResult = (Map<String,Object>) invokeMethod.invoke(serviceBean,
                        new Object[] {request,postParam});
            } catch (Exception e) {
                errorLog = ExceptionUtils.getStackTrace(e);
                PlatLogUtil.printStackTrace(e);
                result.put("success", false);
                result.put("msg", "服务器内部出错!");
            }
        }
        return dataResult;
    }
    
    /**
     * 调用服务
     * @param request
     * @param response
     */
    @RequestMapping("/invokeservice")
    public void invokeservice(HttpServletRequest request,
            HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Headers", "origin,x-requested-with,content-type");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Methods", "POST");
        response.addHeader("Access-Control-Max-Age", "30");
        //boolean isValidIp = userService.isValidIpAddress(request);
        boolean isValidIp = true;
        Map<String,Object> result = new HashMap<String,Object>();
        if(isValidIp){
            String POSTPARAM_JSON = request.getParameter("POSTPARAM_JSON");
            if(StringUtils.isNotEmpty(POSTPARAM_JSON)){
                Map<String,Object> postParam = JSON.parseObject(POSTPARAM_JSON,Map.class);
                //获取服务编码
                String INVOKESERVICE_CODE = (String) postParam.get("INVOKESERVICE_CODE");
                Map<String,Object> appSer = appSerService.getRecord("MZT_DATASET_APPSER",
                        new String[]{"APPSER_CODE"},new Object[]{INVOKESERVICE_CODE});
                if(appSer!=null){
                    String APPSER_STATUS = appSer.get("APPSER_STATUS").toString();
                    if(APPSER_STATUS.equals("1")){
                        Map<String,Object> dataResult = null;
                        String APPSER_ADDCACHE = appSer.get("APPSER_ADDCACHE").toString();
                        if(APPSER_ADDCACHE.equals("1")){
                            String encodeKey = null;
                            String APPSER_CACHEKEY = (String) appSer.get("APPSER_CACHEKEY");
                            if(StringUtils.isNotEmpty(APPSER_CACHEKEY)){
                                String cacheKeyName = (String) postParam.get(APPSER_CACHEKEY);
                                encodeKey = cacheKeyName;
                            }else{
                                encodeKey = PlatStringUtil.getMD5Encode(POSTPARAM_JSON, null, 1);
                                encodeKey = INVOKESERVICE_CODE+"_"+encodeKey;
                            }
                            String cacheResult = redisService.get(encodeKey);
                            if(StringUtils.isNotEmpty(cacheResult)){
                                dataResult = JSON.parseObject(cacheResult,Map.class);
                            }
                        }
                        String errorLog = null;
                        if(dataResult==null){
                            dataResult = this.invokeDyna(appSer, dataResult,
                                    errorLog,result, request, postParam);
                            if(APPSER_STATUS.equals("1")&&APPSER_ADDCACHE.equals("1")){
                                String encodeKey = null;
                                String APPSER_CACHEKEY = (String) appSer.get("APPSER_CACHEKEY");
                                if(StringUtils.isNotEmpty(APPSER_CACHEKEY)){
                                    String cacheKeyName = (String) postParam.get(APPSER_CACHEKEY);
                                    encodeKey = cacheKeyName;
                                }else{
                                    encodeKey = PlatStringUtil.getMD5Encode(POSTPARAM_JSON, null, 1);
                                    encodeKey = INVOKESERVICE_CODE+"_"+encodeKey;
                                }
                                redisService.set(encodeKey,JSON.toJSONString(dataResult));
                            }
                        }
                        if(dataResult!=null){
                            Map<String,Object> data = dataResult;
                            result.put("success",data.get("success"));
                            result.put("msg", data.get("msg"));
                            data.remove("success");
                            data.remove("msg");
                            result.put("data", data);
                        }else{
                            result.put("success", false);
                            result.put("msg", "服务器内部出错!");
                        }
                        //保存调用日志
                        saveInvokeRecordLog(request, result, POSTPARAM_JSON,
                                INVOKESERVICE_CODE, appSer, errorLog);
                       
                    }else{
                        result.put("success", false);
                        result.put("msg", "请求服务已停止运行!");
                    }
                }else{
                    result.put("success", false);
                    result.put("msg", "所请求服务并不存在!");
                }
            }else{
                result.put("success", false);
                result.put("msg", "缺失必要参数!");
            }
        }else{
            result.put("success", false);
            result.put("msg", "请求方IP地址无访问权限!");
        }
        this.printObjectJsonString(result, response);
    }

    /**
     * @param request
     * @param result
     * @param POSTPARAM_JSON
     * @param INVOKESERVICE_CODE
     * @param appSer
     * @param errorLog
     */
    private void saveInvokeRecordLog(HttpServletRequest request,
            Map<String, Object> result, String POSTPARAM_JSON,
            String INVOKESERVICE_CODE, Map<String, Object> appSer,
            String errorLog) {
        String APPSER_RECORDLOG = appSer.get("APPSER_RECORDLOG").toString();
        if(APPSER_RECORDLOG.equals("1")){
            boolean success = (boolean) result.get("success");
            String clientIp = BrowserUtils.getIpAddr(request);
            Map<String,Object> invokeLog = new HashMap<String,Object>();
            invokeLog.put("INVOKELOG_POSTPARAM", POSTPARAM_JSON);
            invokeLog.put("INVOKELOG_CODE",INVOKESERVICE_CODE);
            invokeLog.put("INVOKELOG_IP",clientIp);
            invokeLog.put("INVOKELOG_NAME",appSer.get("APPSER_NAME"));
            if(success){
                invokeLog.put("INVOKELOG_RESULT",1);
            }else{
                invokeLog.put("INVOKELOG_ERRORLOG",errorLog);
                invokeLog.put("INVOKELOG_RESULT",-1);
            }
            invokelogService.saveOrUpdate("MZT_DATASET_INVOKELOG",invokeLog,
                    AllConstants.IDGENERATOR_UUID,null);
        }
    }
}

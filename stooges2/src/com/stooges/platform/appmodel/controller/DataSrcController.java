/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.DataSrcService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 数据源业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-27 17:24:02
 */
@Controller  
@RequestMapping("/resledger/DataSrcController")  
public class DataSrcController extends BaseController {
    /**
     * 
     */
    @Resource
    private DataSrcService dataSrcService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    
    /**
     * 删除数据源数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        dataSrcService.deleteRecords("PLAT_RESLEDGER_DATASRC","DATASRC_ID",selectColValues.split(","));
        sysLogService.saveBackLog("数据源管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+selectColValues+"]的数据源", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改数据源数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> dataSrc = PlatBeanUtil.getMapFromRequest(request);
        dataSrc = dataSrcService.saveOrUpdate("PLAT_RESLEDGER_DATASRC",
                dataSrc,AllConstants.IDGENERATOR_UUID,null);
        dataSrc.put("success", true);
        this.printObjectJsonString(dataSrc, response);
    }
    
    /**
     * 跳转到数据源表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String DATASRC_ID = request.getParameter("DATASRC_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> dataSrc = null;
        if(StringUtils.isNotEmpty(DATASRC_ID)){
            dataSrc = this.dataSrcService.getRecord("PLAT_RESLEDGER_DATASRC"
                    ,new String[]{"DATASRC_ID"},new Object[]{DATASRC_ID});
        }else{
            dataSrc = new HashMap<String,Object>();
        }
        request.setAttribute("dataSrc", dataSrc);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 验证数据源有效性
     * @param request
     * @param response
     */
    @RequestMapping(params = "isValid")
    public void isValid(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> dataSrc = PlatBeanUtil.getMapFromRequest(request);
        boolean isValidDb = dataSrcService.isValidDbSource(dataSrc);
        dataSrc.put("success", isValidDb);
        this.printObjectJsonString(dataSrc, response);
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.ResourcesService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 资源信息表业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-24 09:15:27
 */
@Controller  
@RequestMapping("/resledger/ResourcesController")  
public class ResourcesController extends BaseController {
    /**
     * 
     */
    @Resource
    private ResourcesService resourcesService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    
    /**
     * 删除资源信息表数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        resourcesService.deleteCascadeSub(selectColValues);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 保存资源基本信息
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveBase")
    public void saveBase(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> resources = resourcesService.saveBaseInfo(request);
        resources.put("success", true);
        this.printObjectJsonString(resources, response);
    }
    
    /**
     * 跳转到资源信息表表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String RESOURCES_ID = request.getParameter("RESOURCES_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> resources = null;
        if(StringUtils.isNotEmpty(RESOURCES_ID)){
            resources = this.resourcesService.getRecord("PLAT_RESLEDGER_RESOURCES"
                    ,new String[]{"RESOURCES_ID"},new Object[]{RESOURCES_ID});
            Map<String,Object> POST_PARAMS = new HashMap<String,Object>();
            POST_PARAMS.put("RESOURCES_ID", RESOURCES_ID);
            request.setAttribute("POST_PARAMS", POST_PARAMS);
        }else{
            resources = new HashMap<String,Object>();
        }
        request.setAttribute("resources", resources);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 保存数据源配置信息
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveDataSource")
    public void saveDataSource(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> resources = resourcesService.saveDataSource(request);
        resources.put("success", true);
        this.printObjectJsonString(resources, response);
    }
    
    /**
     * 保存附加信息例如返回结果列和排序
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveAttachInfo")
    public void saveAttachInfo(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> resources = PlatBeanUtil.getMapFromRequest(request);
        resources = resourcesService.saveOrUpdate("PLAT_RESLEDGER_RESOURCES",
                resources,AllConstants.IDGENERATOR_UUID,null);
        resources.put("success", true);
        this.printObjectJsonString(resources, response);
    }
    
    

}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.ReqserService;
import com.stooges.platform.appmodel.service.SerApplyService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 服务申请记录业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-26 17:54:49
 */
@Controller  
@RequestMapping("/resledger/SerApplyController")  
public class SerApplyController extends BaseController {
    /**
     * 
     */
    @Resource
    private SerApplyService serApplyService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    /**
     * 
     */
    @Resource
    private ReqserService reqserService;
    
    /**
     * 删除服务申请记录数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        serApplyService.deleteRecords("PLAT_RESLEDGER_SERAPPLY","SERAPPLY_ID",selectColValues.split(","));
        sysLogService.saveBackLog("服务申请管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+selectColValues+"]的服务申请记录", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改服务申请记录数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> serApply = PlatBeanUtil.getMapFromRequest(request);
        String SERAPPLY_ID = (String) serApply.get("SERAPPLY_ID");
        serApply = serApplyService.saveOrUpdate("PLAT_RESLEDGER_SERAPPLY",
                serApply,AllConstants.IDGENERATOR_UUID,null);
        serApply.put("success", true);
        this.printObjectJsonString(serApply, response);
    }
    
    /**
     * 跳转到服务申请记录表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String SERAPPLY_ID = request.getParameter("SERAPPLY_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> serApply = null;
        if(StringUtils.isNotEmpty(SERAPPLY_ID)){
            serApply = this.serApplyService.getRecord("PLAT_RESLEDGER_SERAPPLY"
                    ,new String[]{"SERAPPLY_ID"},new Object[]{SERAPPLY_ID});
            String SERAPPLY_SERIDS = (String) serApply.get("SERAPPLY_SERIDS");
            List<String> SERAPPLY_SERNAMES = serApplyService.findSerApplySerNames(SERAPPLY_SERIDS);
            StringBuffer serNames = new StringBuffer("");
            for(int i=0;i<SERAPPLY_SERNAMES.size();i++){
                if(i>0){
                    serNames.append(",");
                }
                serNames.append(SERAPPLY_SERNAMES.get(i));
            }
            serApply.put("SERAPPLY_SERNAMES", serNames.toString());
        }else{
            serApply = new HashMap<String,Object>();
        }
        request.setAttribute("serApply", serApply);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 跳转到服务申请记录表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goAudit")
    public ModelAndView goAudit(HttpServletRequest request) {
        String SERAPPLY_ID = request.getParameter("SERAPPLY_ID");
        Map<String,Object> serApply = null;
        if(StringUtils.isNotEmpty(SERAPPLY_ID)){
            serApply = this.serApplyService.getRecord("PLAT_RESLEDGER_SERAPPLY"
                    ,new String[]{"SERAPPLY_ID"},new Object[]{SERAPPLY_ID});
            String SERAPPLY_SERIDS = (String) serApply.get("SERAPPLY_SERIDS");
            List<Map<String,Object>> serviceList =reqserService.findBySerIds(SERAPPLY_SERIDS);
            String SERAPPLY_IPS = (String) serApply.get("SERAPPLY_IPS");
            List<Map> ipList = JSON.parseArray(SERAPPLY_IPS, Map.class);
            request.setAttribute("serviceList", serviceList);
            request.setAttribute("ipList", ipList);
        }
        request.setAttribute("serApply", serApply);
        return new ModelAndView("background/resledger/serapply/applyser_aduitform");
    }
    
    /**
     * 新增或者修改服务申请记录数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "updateAuditResult")
    public void updateAuditResult(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> serApply = PlatBeanUtil.getMapFromRequest(request);
        serApply = serApplyService.saveOrUpdate("PLAT_RESLEDGER_SERAPPLY",
                serApply,AllConstants.IDGENERATOR_UUID,null);
        serApply.put("success", true);
        this.printObjectJsonString(serApply, response);
    }
    
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.DbManagerService;
import com.stooges.platform.appmodel.service.MetaTableService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 元数据表业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-20 11:13:48
 */
@Controller  
@RequestMapping("/resledger/MetaTableController")  
public class MetaTableController extends BaseController {
    /**
     * 
     */
    @Resource
    private MetaTableService metaTableService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    /**
     * 
     */
    @Resource
    private DbManagerService dbManagerService;
    
    /**
     * 验证表名的有效性
     * @param request
     * @param response
     */
    @RequestMapping(params = "checkValid")
    public void checkValid(HttpServletRequest request,
            HttpServletResponse response) {
        String VALID_FIELDNAME = request.getParameter("VALID_FIELDNAME");
        String VALID_FIELDVALUE = request.getParameter(VALID_FIELDNAME);
        String tableName = VALID_FIELDVALUE.toUpperCase();
        Map<String,String> result = new HashMap<String,String>();
        boolean isExists = metaTableService.isExistsTable(tableName);
        if(isExists){
            result.put("error", "该表已经存在配置,请重新输入!");  
        }else{
            boolean isExistInDb = dbManagerService.isExistsTable(tableName,true);
            if(!isExistInDb){
                result.put("error", "在数据库中未查询到该表,请重新输入!");   
            }
        }
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 删除元数据表数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        metaTableService.deleteRecords("PLAT_RESLEDGER_TABLE","TABLE_ID",selectColValues.split(","));
        sysLogService.saveBackLog("技术元数据管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+selectColValues+"]的元数据表", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改元数据表数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> metaTable = PlatBeanUtil.getMapFromRequest(request);
        String TABLE_NAME = request.getParameter("TABLE_NAME");
        metaTable.put("TABLE_NAME", TABLE_NAME.toUpperCase());
        metaTable = metaTableService.saveOrUpdate("PLAT_RESLEDGER_TABLE",
                metaTable,AllConstants.IDGENERATOR_UUID,null);
        metaTable.put("success", true);
        this.printObjectJsonString(metaTable, response);
    }
    
    /**
     * 修改元数据表信息
     * @param request
     * @param response
     */
    @RequestMapping(params = "updateTableInfo")
    public void updateTableInfo(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> metaTable = PlatBeanUtil.getMapFromRequest(request);
        metaTable = metaTableService.updateTableCascadeColumn(metaTable);
        metaTable.put("success", true);
        this.printObjectJsonString(metaTable, response);
    }
    
    /**
     * 跳转到元数据表表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String TABLE_ID = request.getParameter("TABLE_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> metaTable = null;
        if(StringUtils.isNotEmpty(TABLE_ID)){
            metaTable = this.metaTableService.getRecord("PLAT_RESLEDGER_TABLE"
                    ,new String[]{"TABLE_ID"},new Object[]{TABLE_ID});
        }else{
            metaTable = new HashMap<String,Object>();
        }
        request.setAttribute("metaTable", metaTable);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 跳转到元数据表编辑界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goEditForm")
    public ModelAndView goEditForm(HttpServletRequest request) {
        String TABLE_ID = request.getParameter("TABLE_ID");
        Map<String,Object> metaTable = this.metaTableService.getRecord("PLAT_RESLEDGER_TABLE"
                ,new String[]{"TABLE_ID"},new Object[]{TABLE_ID});
        String TABLE_DATASRCID = (String) metaTable.get("TABLE_DATASRCID");
        Map<String,Object> dataSrc = this.metaTableService.getRecord("PLAT_RESLEDGER_DATASRC"
                ,new String[]{"DATASRC_ID"},new Object[]{TABLE_DATASRCID});
        String DATASRC_NAME = (String) dataSrc.get("DATASRC_NAME");
        metaTable.put("TABLE_DATASRCNAME", DATASRC_NAME);
        request.setAttribute("table", metaTable);
        return PlatUICompUtil.goDesignUI("tableeditform", request);
    }
}

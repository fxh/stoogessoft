/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.stooges.core.model.SqlFilter;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.CatalogService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 资源目录业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-21 11:07:35
 */
@Controller  
@RequestMapping("/resledger/CatalogController")  
public class CatalogController extends BaseController {
    /**
     * 
     */
    @Resource
    private CatalogService catalogService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    
    /**
     * 删除资源目录数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String catalogId = request.getParameter("treeNodeId");
        catalogService.deleteCascaseSub(catalogId);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改资源目录数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> catalog = PlatBeanUtil.getMapFromRequest(request);
        String CATALOG_ID = (String) catalog.get("CATALOG_ID");
        //如果是保存树形结构表数据,请调用下面的接口,而注释掉上面的接口代码
        catalog = catalogService.saveOrUpdateTreeData("PLAT_RESLEDGER_CATALOG",
                catalog,AllConstants.IDGENERATOR_UUID,null);
        if(StringUtils.isNotEmpty(CATALOG_ID)){
            sysLogService.saveBackLog("资源目录管理",SysLogService.OPER_TYPE_EDIT,
                    "修改了ID为["+CATALOG_ID+"]资源目录", request);
        }else{
            CATALOG_ID = (String) catalog.get("CATALOG_ID");
            sysLogService.saveBackLog("资源目录管理",SysLogService.OPER_TYPE_ADD,
                    "新增了ID为["+CATALOG_ID+"]资源目录", request);
        }
        catalog.put("success", true);
        this.printObjectJsonString(catalog, response);
    }
    
    /**
     * 跳转到资源目录表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        //如果是跳转到树形表单录入界面,请开放以下代码,,而注释掉上面的代码
        String CATALOG_ID = request.getParameter("CATALOG_ID");
        String CATALOG_PARENTID = request.getParameter("CATALOG_PARENTID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> catalog = null;
        if(StringUtils.isNotEmpty(CATALOG_ID)){
            catalog = this.catalogService.getRecord("PLAT_RESLEDGER_CATALOG"
                    ,new String[]{"CATALOG_ID"},new Object[]{CATALOG_ID});
            CATALOG_PARENTID = (String) catalog.get("CATALOG_PARENTID");
        }
        Map<String,Object> parentCatalog = null;
        if(CATALOG_PARENTID.equals("0")){
            parentCatalog = new HashMap<String,Object>();
            parentCatalog.put("CATALOG_ID",CATALOG_PARENTID);
            parentCatalog.put("CATALOG_NAME","资源目录树");
        }else{
            parentCatalog = this.catalogService.getRecord("PLAT_RESLEDGER_CATALOG",
                    new String[]{"CATALOG_ID"}, new Object[]{CATALOG_PARENTID});
        }
        if(catalog==null){
            catalog = new HashMap<String,Object>();
        }
        catalog.put("CATALOG_PARENTID",parentCatalog.get("CATALOG_ID"));
        catalog.put("CATALOG_PARENTNAME",parentCatalog.get("CATALOG_NAME"));
        request.setAttribute("catalog", catalog);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 自动补全目录和服务
     * @param request
     * @param response
     */
    @RequestMapping(params = "autoCatalogAndSer")
    public void autoCatalogAndSer(HttpServletRequest request,
            HttpServletResponse response) {
        //如果自动补全的类型为2,那么获取到key之后进行判断过滤
        //String keyword = request.getParameter("keyword");
        SqlFilter sqlFilter = new SqlFilter(request);
        List<Map<String,Object>> list = catalogService.findAutoCatalogSer(sqlFilter);
        String json = JSON.toJSONString(list);
        this.printJsonString(json.toLowerCase(), response);
    }
    
    /**
     * 获取树形分类和服务
     * @param request
     * @param response
     */
    @RequestMapping(params = "catalogAndSers")
    public void groupAndRoles(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> params= PlatBeanUtil.getMapFromRequest(request);
        String departJson = catalogService.getCatalogAndSerJson(params);
        this.printJsonString(departJson, response);
    }
}

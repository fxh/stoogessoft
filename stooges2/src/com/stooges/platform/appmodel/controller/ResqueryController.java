/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.ResqueryService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 查询参数业务相关Controllero
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 09:20:52
 */
@Controller  
@RequestMapping("/resledger/ResqueryController")  
public class ResqueryController extends BaseController {
    /**
     * 
     */
    @Resource
    private ResqueryService resqueryService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    
    /**
     * 删除查询参数数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        resqueryService.deleteRecords("PLAT_RESLEDGER_RESQUERY","RESQUERY_ID",selectColValues.split(","));
        sysLogService.saveBackLog("资源目录管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+selectColValues+"]的查询参数", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改查询参数数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> resquery = PlatBeanUtil.getMapFromRequest(request);
        String RESQUERY_ID = (String) resquery.get("RESQUERY_ID");
        resquery = resqueryService.saveOrUpdate("PLAT_RESLEDGER_RESQUERY",
                resquery,AllConstants.IDGENERATOR_UUID,null);
        //如果是保存树形结构表数据,请调用下面的接口,而注释掉上面的接口代码
        //resquery = resqueryService.saveOrUpdateTreeData("PLAT_RESLEDGER_RESQUERY",
        //        resquery,AllConstants.IDGENERATOR_UUID,null);
        if(StringUtils.isNotEmpty(RESQUERY_ID)){
            sysLogService.saveBackLog("资源目录管理",SysLogService.OPER_TYPE_EDIT,
                    "修改了ID为["+RESQUERY_ID+"]查询参数", request);
        }else{
            RESQUERY_ID = (String) resquery.get("RESQUERY_ID");
            sysLogService.saveBackLog("资源目录管理",SysLogService.OPER_TYPE_ADD,
                    "新增了ID为["+RESQUERY_ID+"]查询参数", request);
        }
        resquery.put("success", true);
        this.printObjectJsonString(resquery, response);
    }
    
    /**
     * 跳转到查询参数表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String RESQUERY_ID = request.getParameter("RESQUERY_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> resquery = null;
        if(StringUtils.isNotEmpty(RESQUERY_ID)){
            resquery = this.resqueryService.getRecord("PLAT_RESLEDGER_RESQUERY"
                    ,new String[]{"RESQUERY_ID"},new Object[]{RESQUERY_ID});
        }else{
            String RESOURCES_ID = request.getParameter("RESOURCES_ID");
            resquery = new HashMap<String,Object>();
            resquery.put("RESQUERY_RESID", RESOURCES_ID);
        }
        request.setAttribute("resquery", resquery);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
}

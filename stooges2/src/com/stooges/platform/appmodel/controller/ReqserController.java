/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.stooges.core.util.BrowserUtils;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatLogUtil;
import com.stooges.core.util.PlatPropUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.service.ReqserService;
import com.stooges.platform.appmodel.service.ResourcesService;
import com.stooges.platform.appmodel.service.ResqueryService;
import com.stooges.platform.appmodel.service.SerApplyService;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 请求服务业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 15:17:01
 */
@Controller  
@RequestMapping("/resledger/ReqserController")  
public class ReqserController extends BaseController {
    /**
     * 
     */
    @Resource
    private ReqserService reqserService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    /**
     * 
     */
    @Resource
    private ResqueryService resqueryService;
    /**
     * 
     */
    @Resource
    private ResourcesService resourcesService;
    /**
     * 
     */
    @Resource
    private SerApplyService serApplyService;
    
    /**
     * 删除请求服务数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        reqserService.deleteRecords("PLAT_RESLEDGER_REQSER","REQSER_ID",selectColValues.split(","));
        sysLogService.saveBackLog("请求服务管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+selectColValues+"]的请求服务", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改请求服务数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> reqser = PlatBeanUtil.getMapFromRequest(request);
        reqser = reqserService.saveCascadeSub(request);
        reqser.put("success", true);
        this.printObjectJsonString(reqser, response);
    }
    
    /**
     * 跳转到请求服务表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String REQSER_ID = request.getParameter("REQSER_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> reqser = null;
        if(StringUtils.isNotEmpty(REQSER_ID)){
            reqser = this.reqserService.getRecord("PLAT_RESLEDGER_REQSER"
                    ,new String[]{"REQSER_ID"},new Object[]{REQSER_ID});
        }else{
            reqser = new HashMap<String,Object>();
        }
        request.setAttribute("reqser", reqser);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 更新状态
     * @param request
     * @param response
     */
    @RequestMapping(params = "updateStatus")
    public void updateStatus(HttpServletRequest request,
            HttpServletResponse response) {
        String reqserIds = request.getParameter("selectColValues");
        String status = request.getParameter("status");
        reqserService.updateStatus(reqserIds, status);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 跳转到请求服务文档明细
     * @param request
     * @return
     */
    @RequestMapping(params = "goDetail")
    public ModelAndView goDetail(HttpServletRequest request) {
        String REQSER_ID = request.getParameter("REQSER_ID");
        Map<String,Object> reqser = this.reqserService.getRecord("PLAT_RESLEDGER_REQSER"
                ,new String[]{"REQSER_ID"},new Object[]{REQSER_ID});
        //获取请求服务的根据路径
        String reqserUrl = PlatPropUtil.getPropertyValue("conf/config.properties",
                "reqserUrl");
        //定义服务访问路径
        StringBuffer serverUrl = new StringBuffer(reqserUrl);
        serverUrl.append("resledger/ReqserController/getdata.do?servicecode=");
        serverUrl.append(reqser.get("REQSER_CODE"));
        String REQSER_RESID = (String) reqser.get("REQSER_RESID");
        List<Map<String,Object>> resqueryList = resqueryService.findByResId(REQSER_RESID);
        reqser.put("REQSER_URL", serverUrl.toString());
        //获取资源列表
        List<Map> resourcesList = resourcesService.findReturnFields(REQSER_RESID);
        if(resourcesList!=null){
            request.setAttribute("resourcesList", resourcesList);
        }else{
            request.setAttribute("resourcesList",new ArrayList<Map>());
        }
        request.setAttribute("reqser", reqser);
        request.setAttribute("resqueryList", resqueryList);
        return PlatUICompUtil.goDesignUI("reqserapidetail", request);
    }
    
    /**
     * 测试数据服务的数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "testData")
    public void testData(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> result = this.getDataResult(request,false);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 获取数据服务
     * @param request
     * @return
     */
    private Map<String,Object> getDataResult(HttpServletRequest request,boolean validIp){
        Map<String,Object> result = new HashMap<String,Object>();
        String servicecode = request.getParameter("servicecode");
        Map<String,Object> reqser = resourcesService.getRecord("PLAT_RESLEDGER_REQSER",
                new String[]{"REQSER_CODE"}, new Object[]{servicecode});
        if(reqser!=null){
            if(validIp){
                String REQSER_ID = (String) reqser.get("REQSER_ID");
                Set<String> authIpSet = serApplyService.getAuthIpSet(REQSER_ID);
                String clientIpAddress = BrowserUtils.getIpAddr(request);
                if(!authIpSet.contains(clientIpAddress)){
                    result.put("resultmsg", "请求方IP地址无访问权限!");
                    result.put("resultcode", "004");
                    return result;
                }
            }
            String REQSER_STATUS = (String) reqser.get("REQSER_STATUS");
            if(ReqserService.STATUS_START.equals(REQSER_STATUS)){
                String REQSER_RESID = (String) reqser.get("REQSER_RESID");
                //获取传递过来的参数
                Map<String,Object> postParamMap = PlatBeanUtil.getMapFromRequest(request);
                //获取必要参数列表
                List<String> mustParamList = resqueryService.findNotNullList(REQSER_RESID);
                boolean containAllMustParam = true;
                for(String mustParam:mustParamList){
                    if(postParamMap.keySet().contains(mustParam)){
                        if(postParamMap.get(mustParam)==null||
                                StringUtils.isEmpty(postParamMap.get(mustParam).toString())){
                            containAllMustParam = false;
                            break;
                        }
                    }else{
                        containAllMustParam = false;
                        break;
                    }
                }
                if(containAllMustParam){
                    try{
                        Integer REQSER_MAXLINE = -1;
                        if(reqser.get("REQSER_MAXLINE")!=null){
                            REQSER_MAXLINE = Integer.parseInt(reqser.get("REQSER_MAXLINE").toString());
                        }
                        List<Map<String,Object>> data = resourcesService.findQueryData(REQSER_RESID, 
                                request,REQSER_MAXLINE);
                        result.put("resultmsg", "访问成功!");
                        result.put("resultcode", "000");
                        result.put("data", data);
                    }catch(Exception e){
                        PlatLogUtil.printStackTrace(e);
                        result.put("resultmsg", "其它未知异常!");
                        result.put("resultcode", "999");
                    }
                }else{
                    result.put("resultmsg", "缺失必要参数!");
                    result.put("resultcode", "001");
                }
            }else if(ReqserService.STATUS_STOP.equals(REQSER_STATUS)){
                result.put("resultmsg", "服务停止,无法获取数据!");
                result.put("resultcode", "003");
            } 
        }else{
            result.put("resultmsg", "未查询到对应服务!");
            result.put("resultcode", "002");
        }
        return result;
    }
    
    /**
     * 获取请求服务的数据
     * @param request
     * @param response
     */
    @RequestMapping("/getdata")
    public void getdata(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> result = this.getDataResult(request,true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 跳转到服务选择器
     * @param request
     * @return
     */
    @RequestMapping(params = "goSerSelector")
    public ModelAndView goSerSelector(HttpServletRequest request) {
        String SERAPPLY_ID = request.getParameter("SERAPPLY_ID");
        StringBuffer selectedRecordIds = new StringBuffer("");
        if(StringUtils.isNotEmpty(SERAPPLY_ID)){
            Map<String,Object> serApply = serApplyService.getRecord("PLAT_RESLEDGER_SERAPPLY",
                    new String[]{"SERAPPLY_ID"},new Object[]{SERAPPLY_ID});
            String SERAPPLY_SERIDS = (String) serApply.get("SERAPPLY_SERIDS");
            selectedRecordIds.append(SERAPPLY_SERIDS);
        }
        request.setAttribute("selectedRecordIds", selectedRecordIds.toString());
        return PlatUICompUtil.goDesignUI("reqserselector", request);
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao;

import com.stooges.core.dao.BaseDao;

/**
 * 
 * 描述 元数据视图业务相关dao
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-21 09:03:25
 */
public interface MetaViewDao extends BaseDao {
    /**
     * 判断是否存在该视图
     * @param viewName
     * @return
     */
    public boolean isExistsView(String viewName);
}

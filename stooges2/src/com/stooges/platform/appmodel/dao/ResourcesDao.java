/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao;

import com.stooges.core.dao.BaseDao;

/**
 * 
 * 描述 资源信息表业务相关dao
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-24 09:15:27
 */
public interface ResourcesDao extends BaseDao {
    /**
     * 获取最大资源值
     * @return
     */
    public int getMaxResNumber();
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao;

import com.stooges.core.dao.BaseDao;

/**
 * 
 * 描述 APP服务业务相关dao
 * @author 胡裕
 * @version 1.0
 * @created 2017-08-02 17:11:23
 */
public interface AppSerDao extends BaseDao {

}

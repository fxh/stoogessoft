/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.appmodel.dao.AppSerDao;

/**
 * 描述APP服务业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-08-02 17:11:23
 */
@Repository
public class AppSerDaoImpl extends BaseDaoImpl implements AppSerDao {

}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.appmodel.dao.DataSrcDao;

/**
 * 描述数据源业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-27 17:24:02
 */
@Repository
public class DataSrcDaoImpl extends BaseDaoImpl implements DataSrcDao {

}

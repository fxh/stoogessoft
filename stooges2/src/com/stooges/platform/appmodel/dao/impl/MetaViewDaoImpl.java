/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.appmodel.dao.MetaViewDao;

/**
 * 描述元数据视图业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-21 09:03:25
 */
@Repository
public class MetaViewDaoImpl extends BaseDaoImpl implements MetaViewDao {

    /**
     * 判断是否存在该视图
     * @param viewName
     * @return
     */
    public boolean isExistsView(String viewName){
        StringBuffer sql = new StringBuffer("SELECT COUNT(*) FROM ");
        sql.append("PLAT_RESLEDGER_VIEW T WHERE T.VIEW_NAME=? ");
        int count = this.getIntBySql(sql.toString(),new Object[]{viewName});
        if(count==0){
            return false;
        }else{
            return true;
        }
    }
}

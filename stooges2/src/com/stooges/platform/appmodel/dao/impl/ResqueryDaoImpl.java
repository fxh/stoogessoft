/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.appmodel.dao.ResqueryDao;

/**
 * 描述查询参数业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 09:20:52
 */
@Repository
public class ResqueryDaoImpl extends BaseDaoImpl implements ResqueryDao {

    /**
     * 根据资源ID获取非空参数列表
     * @param resId
     * @return
     */
    public List<String> findNotNullList(String resId){
        StringBuffer sql = new StringBuffer("SELECT T.RESQUERY_NAME");
        sql.append(" FROM PLAT_RESLEDGER_RESQUERY T");
        sql.append(" WHERE T.RESQUERY_RESID=? AND T.RESQUERY_NOTNULL=? ");
        sql.append(" ORDER BY T.RESQUERY_CREATETIME ASC");
        List<String> paramList = this.getJdbcTemplate().queryForList(sql.toString(), 
                new Object[]{resId,"1"},String.class);
        return paramList;
    }
}

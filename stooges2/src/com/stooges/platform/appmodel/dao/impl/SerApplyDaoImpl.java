/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.core.util.PlatStringUtil;
import com.stooges.platform.appmodel.dao.SerApplyDao;

/**
 * 描述服务申请记录业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-26 17:54:49
 */
@Repository
public class SerApplyDaoImpl extends BaseDaoImpl implements SerApplyDao {

    /**
     * 
     * @param serIds
     * @return
     */
    public List<String> findSerApplySerNames(String serIds){
        StringBuffer sql = new StringBuffer("SELECT T.REQSER_NAME");
        sql.append(" FROM PLAT_RESLEDGER_REQSER T WHERE ");
        sql.append(" T.REQSER_ID IN ").append(PlatStringUtil.getSqlInCondition(serIds));
        List<String> list = this.getJdbcTemplate().queryForList(sql.toString(),String.class);
        return list;
    }
}

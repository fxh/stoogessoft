/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.appmodel.dao.CatalogDao;

/**
 * 描述资源分类业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-21 11:07:35
 */
@Repository
public class CatalogDaoImpl extends BaseDaoImpl implements CatalogDao {

}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.appmodel.dao.ResourcesDao;

/**
 * 描述资源信息表业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-24 09:15:27
 */
@Repository
public class ResourcesDaoImpl extends BaseDaoImpl implements ResourcesDao {
    
    /**
     * 获取最大资源值
     * @return
     */
    public int getMaxResNumber(){
        StringBuffer sql = new StringBuffer("SELECT max(to_number(R.RESOURCES_NUM))");
        sql.append(" FROM PLAT_RESLEDGER_RESOURCES R");
        int number = this.getIntBySql(sql.toString(),null);
        return number;
    }
}

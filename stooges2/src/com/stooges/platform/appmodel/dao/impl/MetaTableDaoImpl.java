/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao.impl;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.appmodel.dao.MetaTableDao;

/**
 * 描述元数据表业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-20 11:13:48
 */
@Repository
public class MetaTableDaoImpl extends BaseDaoImpl implements MetaTableDao {

    /**
     * 判断是否有这张表记录
     * @param tableName
     * @return
     */
    public boolean isExistsTable(String tableName){
        StringBuffer sql = new StringBuffer("SELECT COUNT(*) FROM ");
        sql.append("PLAT_RESLEDGER_TABLE T WHERE T.TABLE_NAME=? ");
        int count = this.getIntBySql(sql.toString(),new Object[]{tableName});
        if(count==0){
            return false;
        }else{
            return true;
        }
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao;

import java.util.List;

import com.stooges.core.dao.BaseDao;

/**
 * 
 * 描述 服务申请记录业务相关dao
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-26 17:54:49
 */
public interface SerApplyDao extends BaseDao {
    /**
     * 
     * @param serIds
     * @return
     */
    public List<String> findSerApplySerNames(String serIds);
}

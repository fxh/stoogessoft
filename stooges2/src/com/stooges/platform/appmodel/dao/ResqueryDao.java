/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao;

import java.util.List;

import com.stooges.core.dao.BaseDao;

/**
 * 
 * 描述 查询参数业务相关dao
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 09:20:52
 */
public interface ResqueryDao extends BaseDao {
    /**
     * 根据资源ID获取非空参数列表
     * @param resId
     * @return
     */
    public List<String> findNotNullList(String resId);
}

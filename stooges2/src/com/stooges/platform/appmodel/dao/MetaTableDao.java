/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.dao;

import com.stooges.core.dao.BaseDao;

/**
 * 
 * 描述 元数据表业务相关dao
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-20 11:13:48
 */
public interface MetaTableDao extends BaseDao {
    /**
     * 判断是否有这张表记录
     * @param tableName
     * @return
     */
    public boolean isExistsTable(String tableName);
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import com.stooges.core.model.SqlFilter;
import com.stooges.core.model.TableColumn;
import com.stooges.core.service.BaseService;

/**
 * 描述 数据源业务相关service
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-27 17:24:02
 */
public interface DataSrcService extends BaseService {
    /**
     * 判断数据源有效性
     * @param dataSrc
     * @return
     */
    public boolean isValidDbSource(Map<String,Object> dataSrc);
    /**
     * 获取可选数据源列表
     * @param param
     * @return
     */
    public List<Map<String,Object>> findForSelect(String param);
    /**
     * 根据数据源ID获取数据库表列表
     * @param dataSrcId
     * @return
     */
    public List<Map<String,Object>> findTableList(String dataSrcId);
    /**
     * 根据数据源ID获取数据库视图列表
     * @param dataSrcId
     * @return
     */
    public List<Map<String,Object>> findViewList(String dataSrcId);
    /**
     * 获取数据库表的列信息
     * @param sqlFilter
     * @return
     */
    public List<Map<String,Object>> findTableColunm(SqlFilter sqlFilter);
    /**
     * 获取视图信息
     * @param dataSrcId
     * @param viewName
     * @return
     */
    public Map<String,Object> getViewInfo(String dataSrcId,String viewName);
    
    /**
     * 根据sql语句和资源ID获取字段列表
     * @param sqlContent
     * @return
     */
    public List<Map<String,Object>> findFieldColumnsBySql(String sql,String resourcesId);
    /**
     * 获取所查询的列名称
     * @param sql
     * @param dataSrc
     * @return
     */
    public List<String> findQueryColNamesBySql(String sql,Map<String,Object> dataSrc);
    /**
     * 获取表的字段集合
     * @param dataSrcId
     * @param tableName
     * @return
     */
    public List<TableColumn> findTableColumnByTableName(String dataSrcId,String tableName);
    
    /**
     * 获取Oracle数据连接
     * @param dataSrc
     * @return
     */
    public Connection getOracleConn(Map<String,Object> dataSrc);
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.stooges.core.service.BaseService;

/**
 * 描述 APP服务业务相关service
 * @author 胡裕
 * @version 1.0
 * @created 2017-08-02 17:11:23
 */
public interface AppSerService extends BaseService {
    /**
     * 获取字典数据列表
     * @param request
     * @param postParams
     * @return
     */
    public Map<String,Object> findDicList(HttpServletRequest request,
            Map<String,Object> postParams);
    
    /**
     * 获取字典类别数据列表
     * @param request
     * @param postParams
     * @return
     */
    public Map<String,Object> findDicTypeList(HttpServletRequest request,
            Map<String,Object> postParams);
    
}

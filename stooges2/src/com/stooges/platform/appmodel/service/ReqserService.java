/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.stooges.core.model.SqlFilter;
import com.stooges.core.service.BaseService;

/**
 * 描述 请求服务业务相关service
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 15:17:01
 */
public interface ReqserService extends BaseService {
    /**
     * 状态:启用
     */
    public static final String STATUS_START = "1";
    /**
     * 状态:停用
     */
    public static final String STATUS_STOP = "-1";
    /**
     * 级联保存相关数据
     * @param request
     * @return
     */
    public Map<String,Object> saveCascadeSub(HttpServletRequest request);
    /**
     * 更新状态
     * @param reqserIds
     * @param status
     */
    public void updateStatus(String reqserIds,String status);
    
    /**
     * 获取查询参数列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findQueryParams(SqlFilter sqlFilter);
    /**
     * 获取服务列表根据目录路径
     * @param catalogId
     * @return
     */
    public List<Map<String,Object>> findByCatalogPath(String catalogId);
    
    /**
     * 根据filter获取网格项目
     * @param sqlFilter
     * @return
     */
    public List<Map<String,Object>> findGridItemList(SqlFilter sqlFilter);
    /**
     * 根据服务IDS获取服务列表数据
     * @param serIds
     * @return
     */
    public List<Map<String,Object>> findBySerIds(String serIds);
}

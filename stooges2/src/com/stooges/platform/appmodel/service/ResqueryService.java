/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service;

import java.util.List;
import java.util.Map;

import com.stooges.core.service.BaseService;

/**
 * 描述 查询参数业务相关service
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 09:20:52
 */
public interface ResqueryService extends BaseService {
    /**
     * 根据资源ID获取查询参数列表
     * @param resId
     * @return
     */
    public List<Map<String,Object>> findByResId(String resId);
    
    /**
     * 根据资源ID获取非空参数列表
     * @param resId
     * @return
     */
    public List<String> findNotNullList(String resId);
    
}

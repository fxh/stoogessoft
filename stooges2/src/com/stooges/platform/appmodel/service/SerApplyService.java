/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.stooges.core.model.SqlFilter;
import com.stooges.core.service.BaseService;

/**
 * 描述 服务申请记录业务相关service
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-26 17:54:49
 */
public interface SerApplyService extends BaseService {
    /**
     * 获取Ip地址列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findIpAddress(SqlFilter sqlFilter);
    
    /**
     * 
     * @param serIds
     * @return
     */
    public List<String> findSerApplySerNames(String serIds);
    /**
     * 根据服务ID获取被授权的IP集合
     * @param serviceId
     * @return
     */
    public Set<String> getAuthIpSet(String serviceId);
    
}

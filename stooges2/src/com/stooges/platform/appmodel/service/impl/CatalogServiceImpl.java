/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.stooges.core.dao.BaseDao;
import com.stooges.core.model.SqlFilter;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.platform.appmodel.dao.CatalogDao;
import com.stooges.platform.appmodel.service.CatalogService;
import com.stooges.platform.appmodel.service.ReqserService;

/**
 * 描述 资源分类业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-21 11:07:35
 */
@Service("catalogService")
public class CatalogServiceImpl extends BaseServiceImpl implements CatalogService {

    /**
     * 所引入的dao
     */
    @Resource
    private CatalogDao dao;
    /**
     * 
     */
    @Resource
    private ReqserService reqserService;

    @Override
    protected BaseDao getDao() {
        return dao;
    }

    /**
     * 删除数据并且级联删除子表
     * @param catalogId
     */
    public void deleteCascaseSub(String catalogId){
        //删除资源列表
        StringBuffer sql = new StringBuffer("DELETE FROM ");
        sql.append("PLAT_RESLEDGER_RESOURCES  WHERE RESOURCES_CATALOGID IN ");
        sql.append("(SELECT C.CATALOG_ID FROM PLAT_RESLEDGER_CATALOG C ");
        sql.append(" WHERE C.CATALOG_PATH LIKE ? )");
        dao.executeSql(sql.toString(), new Object[]{"%"+catalogId+"%"});
        //删除目录信息
        sql = new StringBuffer("DELETE FROM PLAT_RESLEDGER_CATALOG ");
        sql.append(" WHERE CATALOG_PATH LIKE ?");
        dao.executeSql(sql.toString(), new Object[]{"%"+catalogId+"%"});
    }
    
    /**
     * 获取自动补全的目录和服务数据列表
     * @param filter
     * @return
     */
    public List<Map<String,Object>> findAutoCatalogSer(SqlFilter filter){
        StringBuffer sql = new StringBuffer("SELECT R.CATALOG_NAME AS value");
        sql.append(",R.CATALOG_NAME AS label");
        sql.append(" FROM PLAT_RESLEDGER_CATALOG R ");
        sql.append(" ORDER BY R.CATALOG_CREATETIME ASC ");
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        List<Map<String,Object>> catalogList = dao.findBySql(sql.toString(),null,null);
        list.addAll(catalogList);
        //获取所有服务列表
        sql = new StringBuffer("SELECT R.REQSER_NAME AS value,R.REQSER_NAME AS label");
        sql.append(" FROM PLAT_RESLEDGER_REQSER R ORDER BY R.REQSER_CREATETIME ASC ");
        List<Map<String,Object>> reqSerList = dao.findBySql(sql.toString(),null,null);
        list.addAll(reqSerList);
        return list;
    }
    
    /**
     * 获取顶级分类列表
     * @return
     */
    public List<Map<String,Object>> findTopCatalogList(){
        StringBuffer sql = new StringBuffer("select T.CATALOG_ID AS VALUE,T.CATALOG_NAME AS LABEL ");
        sql.append("from PLAT_RESLEDGER_CATALOG T ");
        sql.append(" WHERE T.CATALOG_PARENTID=? ");
        sql.append(" ORDER BY T.CATALOG_CREATETIME ASC ");
        List<Map<String,Object>> list = dao.findBySql(sql.toString(),new Object[]{"0"}, null);
        return list;
    }
    
    /**
     * 获取目录和服务树形JSON
     * @param params
     * @return
     */
    public String getCatalogAndSerJson(Map<String,Object> params){
        String needCheckIds = (String) params.get("needCheckIds");
        Set<String> needCheckIdSet = new HashSet<String>();
        if(StringUtils.isNotEmpty(needCheckIds)){
            needCheckIdSet = new HashSet<String>(Arrays.asList(needCheckIds.split(",")));
        }
        Map<String,Object> rootNode = new HashMap<String,Object>();
        rootNode.put("id", "0");
        rootNode.put("name", "服务分类树");
        rootNode.put("nocheck", true);
        List<Map<String,Object>> catalogList = this.findTopCatalogList();
        for(Map<String,Object> catalog:catalogList){
            String catalogId = (String) catalog.get("VALUE");
            String catalogName = (String) catalog.get("LABEL");
            catalog.put("id", catalogId);
            catalog.put("name", catalogName);
            catalog.put("nocheck", true);
            List<Map<String,Object>> reqserList = reqserService.findByCatalogPath(catalogId);
            if(reqserList!=null&&reqserList.size()>0){
                for(Map<String,Object> reqser:reqserList){
                    String reqserId = (String) reqser.get("REQSER_ID");
                    if(needCheckIdSet.contains(reqserId)){
                        reqser.put("checked", true);
                    }
                    reqser.put("id", reqser.get("REQSER_ID"));
                    reqser.put("name", reqser.get("REQSER_NAME"));
                }
                catalog.put("children", reqserList);
            }
        }
        rootNode.put("children", catalogList);
        return JSON.toJSONString(rootNode);
    }
}

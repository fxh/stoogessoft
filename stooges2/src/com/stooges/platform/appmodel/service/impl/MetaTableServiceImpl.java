/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.core.util.AllConstants;
import com.stooges.platform.appmodel.dao.MetaTableDao;
import com.stooges.platform.appmodel.service.DbManagerService;
import com.stooges.platform.appmodel.service.MetaTableService;

/**
 * 描述 元数据表业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-20 11:13:48
 */
@Service("metaTableService")
public class MetaTableServiceImpl extends BaseServiceImpl implements MetaTableService {

    /**
     * 所引入的dao
     */
    @Resource
    private MetaTableDao dao;
    /**
     * 
     */
    @Resource
    private DbManagerService dbManagerService;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 判断是否有这张表记录
     * @param tableName
     * @return
     */
    public boolean isExistsTable(String tableName){
        return dao.isExistsTable(tableName);
    }
    
    /**
     * 更新元数据表信息并且级联更新数据库字段
     * @param tableInfo
     * @return
     */
    public Map<String,Object> updateTableCascadeColumn(Map<String,Object> metaTable){
       /* String tableColumnJson = (String) metaTable.get("tableColumnJson");
        String tableName = (String) metaTable.get("TABLE_NAME");*/
        metaTable = dao.saveOrUpdate("PLAT_RESLEDGER_TABLE",
                metaTable,AllConstants.IDGENERATOR_UUID,null);
        /*List<TableColumn> tableColumns= JSON.parseArray(tableColumnJson,TableColumn.class);
        dbManagerService.saveOrUpdateTable("true", tableName, tableComments, tableColumns,oldTableName);*/
        return metaTable;
    }
  
}

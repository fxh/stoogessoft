/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.model.SqlFilter;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatStringUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.dao.ReqserDao;
import com.stooges.platform.appmodel.service.ReqserService;

/**
 * 描述 请求服务业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 15:17:01
 */
@Service("reqserService")
public class ReqserServiceImpl extends BaseServiceImpl implements ReqserService {

    /**
     * 所引入的dao
     */
    @Resource
    private ReqserDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 级联保存相关数据
     * @param request
     * @return
     */
    public Map<String,Object> saveCascadeSub(HttpServletRequest request){
        Map<String,Object> reqser = PlatBeanUtil.getMapFromRequest(request);
        String REQSER_RESID = (String) reqser.get("REQSER_RESID");
        Map<String,Object> resources = dao.getRecord("PLAT_RESLEDGER_RESOURCES",
                new String[]{"RESOURCES_ID"},new Object[]{REQSER_RESID});
        String RESOURCES_NUM = (String) resources.get("RESOURCES_NUM");
        String REQSER_CODE = "FJ_"+RESOURCES_NUM;
        reqser.put("REQSER_CODE", REQSER_CODE);
        reqser = dao.saveOrUpdate("PLAT_RESLEDGER_REQSER",
                reqser,AllConstants.IDGENERATOR_UUID,null);
        return reqser;
    }
    
    /**
     * 更新状态
     * @param reqserIds
     * @param status
     */
    public void updateStatus(String reqserIds,String status){
        StringBuffer sql = new StringBuffer("UPDATE ");
        sql.append("PLAT_RESLEDGER_REQSER T SET T.REQSER_STATUS=? ");
        sql.append("WHERE T.REQSER_ID IN ").append(PlatStringUtil.getSqlInCondition(reqserIds));
        dao.executeSql(sql.toString(), new Object[]{status});
    }
    
    /**
     * 获取查询参数列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findQueryParams(SqlFilter sqlFilter){
        return null;
    }
    
    /**
     * 获取服务列表根据目录路径
     * @param catalogId
     * @return
     */
    public List<Map<String,Object>> findByCatalogPath(String catalogId){
        StringBuffer sql = new StringBuffer("SELECT R.REQSER_ID,R.REQSER_NAME ");
        sql.append("FROM PLAT_RESLEDGER_REQSER R WHERE R.REQSER_RESID IN (");
        sql.append("SELECT R.RESOURCES_ID FROM PLAT_RESLEDGER_RESOURCES R WHERE R.REQSER_CATALOGID");
        sql.append(" IN (SELECT C.CATALOG_ID FROM PLAT_RESLEDGER_CATALOG C");
        sql.append(" WHERE C.CATALOG_PATH LIKE ? )) ORDER BY R.REQSER_CREATETIME ASC");
        List<Map<String,Object>> list = dao.findBySql(sql.toString(), 
                new Object[]{"%"+catalogId+"%"}, null);
        return list;
    }
    
    /**
     * 根据filter获取网格项目
     * @param sqlFilter
     * @return
     */
    public List<Map<String,Object>> findGridItemList(SqlFilter sqlFilter){
        StringBuffer sql = new StringBuffer("SELECT T.REQSER_ID,T.REQSER_NAME");
        sql.append(" FROM PLAT_RESLEDGER_REQSER T ");
        String selectedRoleIds = sqlFilter.getRequest().getParameter("selectedRecordIds");
        String iconfont = sqlFilter.getRequest().getParameter("iconfont");
        String itemconf = sqlFilter.getRequest().getParameter("itemconf");
        Map<String,String> getGridItemConf = PlatUICompUtil.getGridItemConfMap(itemconf);
        if(StringUtils.isNotEmpty(selectedRoleIds)){
            sql.append(" WHERE T.REQSER_ID IN ");
            sql.append(PlatStringUtil.getSqlInCondition(selectedRoleIds));
            sql.append(" ORDER BY T.REQSER_CREATETIME DESC");
            List<Map<String,Object>> list = dao.findBySql(sql.toString(),null, null);
            list = PlatUICompUtil.getGridItemList("REQSER_ID", iconfont, getGridItemConf, list);
            return list;
        }else{
            return null;
        }
    }
    
    /**
     * 根据服务IDS获取服务列表数据
     * @param serIds
     * @return
     */
    public List<Map<String,Object>> findBySerIds(String serIds){
        StringBuffer sql = new StringBuffer("SELECT R.REQSER_ID");
        sql.append(",R.REQSER_CODE,R.REQSER_NAME FROM PLAT_RESLEDGER_REQSER R");
        sql.append(" WHERE R.REQSER_ID IN ").append(PlatStringUtil.getSqlInCondition(serIds));
        return dao.findBySql(sql.toString(), null,null);
    }
  
}

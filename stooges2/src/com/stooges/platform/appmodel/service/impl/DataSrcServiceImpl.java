/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.model.SqlFilter;
import com.stooges.core.model.TableColumn;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.core.util.PlatDbUtil;
import com.stooges.core.util.PlatLogUtil;
import com.stooges.core.util.PlatStringUtil;
import com.stooges.platform.appmodel.dao.DataSrcDao;
import com.stooges.platform.appmodel.service.DataSrcService;
import com.stooges.platform.appmodel.service.DbManagerService;

/**
 * 描述 数据源业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-27 17:24:02
 */
@Service("dataSrcService")
public class DataSrcServiceImpl extends BaseServiceImpl implements DataSrcService {
    /**
     * 
     */
    @Resource
    private DbManagerService dbManagerService;

    /**
     * 所引入的dao
     */
    @Resource
    private DataSrcDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 验证oracle连接的有效性
     * @param dataSrc
     * @return
     */
    private boolean validateOracle(Map<String,Object> dataSrc){
        String DATASRC_CNTYPE = dataSrc.get("DATASRC_CNTYPE").toString();
        StringBuffer dbUrl = null;
        if(DATASRC_CNTYPE.equals("1")){
            dbUrl = new StringBuffer("jdbc:oracle:thin:@");
            dbUrl.append(dataSrc.get("DATASRC_IP")).append(":");
            dbUrl.append(dataSrc.get("DATASRC_PORT")).append(":");
            dbUrl.append(dataSrc.get("DATASRC_SERNAME"));
        }else{
            //获取配置的URL字段
            String DATASRC_URL= (String) dataSrc.get("DATASRC_URL");
            if(StringUtils.isNotEmpty(DATASRC_URL)){
                dbUrl = new StringBuffer(DATASRC_URL);
            }
        }
        String username = (String) dataSrc.get("DATASRC_USERNAME");
        String password = (String) dataSrc.get("DATASRC_PASS");
        Connection conn = null;
        boolean isValid = false;
        try {
            conn = DriverManager.getConnection(dbUrl.toString(), username, password);
            isValid = conn.isValid(10);
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        }finally{
            try {
                if(conn!=null){
                    DbUtils.close(conn);
                }
            } catch (SQLException e) {
                PlatLogUtil.printStackTrace(e);
            }
        }
        return isValid;
    }
    
    /**
     * 判断数据源有效性
     * @param dataSrc
     * @return
     */
    public boolean isValidDbSource(Map<String,Object> dataSrc){
        //获取数据库类型
        String DATASRC_DBTYPE = (String) dataSrc.get("DATASRC_DBTYPE");
        if(DATASRC_DBTYPE.equals("ORACLE")){
            return this.validateOracle(dataSrc);
        }
        return false;
    }
    
    /**
     * 获取可选数据源列表
     * @param param
     * @return
     */
    public List<Map<String,Object>> findForSelect(String param){
        StringBuffer sql = new StringBuffer("SELECT T.DATASRC_ID");
        sql.append(" AS VALUE,T.DATASRC_NAME AS LABEL FROM ");
        sql.append("PLAT_RESLEDGER_DATASRC T ORDER BY T.DATASRC_CREATETIME ASC");
        return dao.findBySql(sql.toString(), null, null);
    }
    
    /**
     * 获取Oracle数据连接
     * @param dataSrc
     * @return
     */
    public Connection getOracleConn(Map<String,Object> dataSrc){
        String DATASRC_CNTYPE = dataSrc.get("DATASRC_CNTYPE").toString();
        StringBuffer dbUrl = null;
        if(DATASRC_CNTYPE.equals("1")){
            dbUrl = new StringBuffer("jdbc:oracle:thin:@");
            dbUrl.append(dataSrc.get("DATASRC_IP")).append(":");
            dbUrl.append(dataSrc.get("DATASRC_PORT")).append(":");
            dbUrl.append(dataSrc.get("DATASRC_SERNAME"));
        }else{
            dbUrl = new StringBuffer(dataSrc.get("DATASRC_URL").toString());
        }
        String username = (String) dataSrc.get("DATASRC_USERNAME");
        String password = (String) dataSrc.get("DATASRC_PASS");
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(dbUrl.toString(), username, password);
        } catch (SQLException e) {
            PlatLogUtil.printStackTrace(e);
        }
        return conn;
    }
    
    /**
     * 根据数据源ID获取数据库表列表
     * @param dataSrcId
     * @return
     */
    public List<Map<String,Object>> findTableList(String dataSrcId){
        if(StringUtils.isNotEmpty(dataSrcId)){
            Map<String,Object> dataSrc = this.getRecord("PLAT_RESLEDGER_DATASRC",
                    new String[]{"DATASRC_ID"},new Object[]{dataSrcId});
            //获取数据库类型
            String DATASRC_DBTYPE = (String) dataSrc.get("DATASRC_DBTYPE");
            Connection conn = null;
            StringBuffer sql = null;
            List<Map<String,Object>> dataList = null;
            if(DATASRC_DBTYPE.equals("ORACLE")){
                conn = this.getOracleConn(dataSrc);
                sql = new StringBuffer("SELECT U.TABLE_NAME,U.COMMENTS ");
                sql.append("FROM USER_TAB_COMMENTS U WHERE U.table_type=? ");
                sql.append(" ORDER BY U.table_name ASC ");
                dataList = PlatDbUtil.findBySql(conn, sql.toString(), new Object[]{"TABLE"});
            }
            for(Map<String,Object> data:dataList){
                StringBuffer LABEL = new StringBuffer(data.get("TABLE_NAME").toString());
                LABEL.append("(").append(data.get("COMMENTS")).append(")");
                data.put("LABEL", LABEL.toString());
                data.put("VALUE", data.get("TABLE_NAME").toString());
            }
            if(conn!=null){
                DbUtils.closeQuietly(conn);
            }
            return dataList;
        }else{
            return null;
        }
    }
    
    /**
     * 根据数据源ID获取数据库视图列表
     * @param dataSrcId
     * @return
     */
    public List<Map<String,Object>> findViewList(String dataSrcId){
        if(StringUtils.isNotEmpty(dataSrcId)){
            Map<String,Object> dataSrc = this.getRecord("PLAT_RESLEDGER_DATASRC",
                    new String[]{"DATASRC_ID"},new Object[]{dataSrcId});
            //获取数据库类型
            String DATASRC_DBTYPE = (String) dataSrc.get("DATASRC_DBTYPE");
            Connection conn = null;
            StringBuffer sql = null;
            List<Map<String,Object>> dataList = null;
            if(DATASRC_DBTYPE.equals("ORACLE")){
                conn = this.getOracleConn(dataSrc);
                sql = new StringBuffer("SELECT U.TABLE_NAME,U.COMMENTS ");
                sql.append("FROM USER_TAB_COMMENTS U WHERE U.table_type=? ");
                sql.append(" ORDER BY U.table_name ASC ");
                dataList = PlatDbUtil.findBySql(conn, sql.toString(), new Object[]{"VIEW"});
            }
            for(Map<String,Object> data:dataList){
                data.put("LABEL", data.get("TABLE_NAME").toString());
                data.put("VALUE", data.get("TABLE_NAME").toString());
            }
            if(conn!=null){
                DbUtils.closeQuietly(conn);
            }
            return dataList;
        }else{
            return null;
        }
    }
    
    /**
     * 获取oracle表的数据列表
     * @param tableName
     * @param dataSrc
     * @return
     */
    public List<TableColumn> findOracleTableColumn(String tableName,Map<String,Object> dataSrc){
        List<Object> params = new ArrayList<Object>();
        StringBuffer sql = new StringBuffer("");
        sql.append("SELECT COL.COLUMN_ID,COL.COLUMN_NAME,COL.DATA_TYPE");
        sql.append(",COL.DATA_LENGTH,COL.NULLABLE,COL.DATA_DEFAULT,CM.comments");
        sql.append(" FROM user_tab_columns col left join user_col_comments cm ");
        sql.append("on col.COLUMN_NAME=cm.column_name AND COL.TABLE_NAME=CM.table_name ");
        sql.append("where col.TABLE_NAME=? ");
        params.add(tableName);
        sql.append("ORDER BY col.COLUMN_ID ASC");
        Connection conn = this.getOracleConn(dataSrc);
        List<TableColumn> columns = new ArrayList<TableColumn>();
        List<Map<String,Object>> list = PlatDbUtil.findBySql(conn, sql.toString(), params.toArray());
        for(Map<String,Object> map:list){
            TableColumn column = dao.getOracleTableColumn(map);
            columns.add(column);
        }
        if(conn!=null){
            DbUtils.closeQuietly(conn);
        }
        return columns;
    }
    
    /**
     * 获取表的字段集合
     * @param dataSrcId
     * @param tableName
     * @return
     */
    public List<TableColumn> findTableColumnByTableName(String dataSrcId,String tableName){
        Map<String,Object> dataSrc = this.getRecord("PLAT_RESLEDGER_DATASRC",
                new String[]{"DATASRC_ID"},new Object[]{dataSrcId});
        //获取数据库类型
        String DATASRC_DBTYPE = (String) dataSrc.get("DATASRC_DBTYPE");
        List<TableColumn> columnList = null;
        if(DATASRC_DBTYPE.equals("ORACLE")){
            columnList = this.findOracleTableColumn(tableName, dataSrc);
        }
        return columnList;
    }
    
    /**
     * 根据数据源ID获取表列表
     * @param dataSrcId
     * @return
     */
    public List<Map<String,Object>> findTableColumns(String dataSrcId,String tableName){
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>(); 
        List<TableColumn> columnList = this.findTableColumnByTableName(dataSrcId, tableName);
        for(TableColumn column:columnList){
            Map<String,Object> map = new HashMap<String,Object>();
            Field[] declaredFields = column.getClass().getDeclaredFields();
            for(Field field:declaredFields){
                field.setAccessible(true); 
                try {
                    map.put(field.getName(),field.get(column));
                } catch (IllegalArgumentException e) {
                    PlatLogUtil.printStackTrace(e);
                } catch (IllegalAccessException e) {
                    PlatLogUtil.printStackTrace(e);
                }
            }
            list.add(map);
        }
        return list;
    }
    
    /**
     * 获取数据库表的列信息
     * @param sqlFilter
     * @return
     */
    public List<Map<String,Object>> findTableColunm(SqlFilter sqlFilter){
        String RECORD_ID = sqlFilter.getRequest().getParameter("RECORD_ID");
        String RECORD_TYPE = sqlFilter.getRequest().getParameter("RECORD_TYPE");
        if(StringUtils.isNotEmpty(RECORD_ID)){
            String TABLE_DATASRCID = null;
            String TABLE_NAME = null;
            if(RECORD_TYPE.equals("table")){
                Map<String,Object> tableInfo = dao.getRecord("PLAT_RESLEDGER_TABLE",
                        new String[]{"TABLE_ID"},new Object[]{RECORD_ID});
                TABLE_DATASRCID = (String) tableInfo.get("TABLE_DATASRCID");
                TABLE_NAME = (String) tableInfo.get("TABLE_NAME");
            }else if(RECORD_TYPE.equals("view")){
                Map<String,Object> viewInfo = dao.getRecord("PLAT_RESLEDGER_VIEW",
                        new String[]{"VIEW_ID"},new Object[]{RECORD_ID});
                TABLE_DATASRCID = (String) viewInfo.get("VIEW_DATASRCID");
                TABLE_NAME = (String) viewInfo.get("VIEW_NAME");
            }
            return this.findTableColumns(TABLE_DATASRCID, TABLE_NAME);
        }else{
            return null;
        }
    }
    
    /**
     * 获取视图信息
     * @param dataSrcId
     * @param viewName
     * @return
     */
    public Map<String,Object> getViewInfo(String dataSrcId,String viewName){
        Map<String,Object> dataSrc = this.getRecord("PLAT_RESLEDGER_DATASRC",
                new String[]{"DATASRC_ID"},new Object[]{dataSrcId});
        //获取数据库类型
        String DATASRC_DBTYPE = (String) dataSrc.get("DATASRC_DBTYPE");
        if(DATASRC_DBTYPE.equals("ORACLE")){
            Connection conn = this.getOracleConn(dataSrc);
            StringBuffer sql = new StringBuffer("SELECT * FROM USER_VIEWS T");
            sql.append(" WHERE T.VIEW_NAME=? ");
            Map<String,Object> userView = PlatDbUtil.getMapBySql(conn,sql.toString(),
                    new Object[]{viewName});
            if(conn!=null){
                DbUtils.closeQuietly(conn);
            }
            return userView;
        }else{
            return null;
        }
    }
    
    /**
     * 根据sql语句和资源ID获取字段列表
     * @param sqlContent
     * @return
     */
    public List<Map<String,Object>> findFieldColumnsBySql(String sql,String resourcesId){
        Map<String,Object> resources = dao.getRecord("PLAT_RESLEDGER_RESOURCES",
                new String[]{"RESOURCES_ID"}, new Object[]{resourcesId});
        String dataSrcId = (String) resources.get("RESOURCES_DATASRCID");
        Map<String,Object> dataSrc = this.getRecord("PLAT_RESLEDGER_DATASRC",
                new String[]{"DATASRC_ID"},new Object[]{dataSrcId});
        List<String> tableNames = dbManagerService.getSelectTableNames(sql);
        List<String> colNames = this.findQueryColNamesBySql(sql,dataSrc);
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        String dbType = (String) dataSrc.get("DATASRC_DBTYPE");
        Connection conn = null;
        if("ORACLE".equals(dbType)){
            conn = this.getOracleConn(dataSrc);
        }
        for(String colName:colNames){
            Map<String,Object> colInfo = new HashMap<String,Object>();
            colInfo.put("FIELD_NAME",colName);
            String FIELD_COMMENT = this.getColumnComments(conn,tableNames,colName,dbType);
            if(StringUtils.isEmpty(FIELD_COMMENT)){
                FIELD_COMMENT = colName;
            }
            colInfo.put("FIELD_COMMENT",FIELD_COMMENT);
            list.add(colInfo);
        }
        if(conn!=null){
            DbUtils.closeQuietly(conn);
        }
        return list;
    }
    
    /**
     * 获取字段注释
     * @param conn
     * @param tableNames
     * @param colName
     * @return
     */
    public String getColumnComments(Connection conn,List<String> tableNames,
            String colName,String dbType){
        if("ORACLE".equals(dbType)){
            StringBuffer sql = new StringBuffer("SELECT U.COMMENTS FROM user_col_comments U");
            sql.append(" where u.table_name in");
            String tableArray = PlatStringUtil.getSqlInCondition(tableNames.
                    toArray(new String[tableNames.size()]));
            sql.append(tableArray).append(" AND U.column_name=? ");
            List<Map<String,Object>> list = PlatDbUtil.findBySql(conn, sql.toString(),
                    new Object[]{colName});
            if(list!=null&&list.size()>0){
                return (String) list.get(0).get("COMMENTS");
            }else{
                return "";
            }
        }else{
            return null;
        }
    }
    
    /**
     * 获取所查询的列名称
     * @param sql
     * @param dataSrc
     * @return
     */
    public List<String> findQueryColNamesBySql(String sql,Map<String,Object> dataSrc){
        //获取数据库类型
        String dbType = (String) dataSrc.get("DATASRC_DBTYPE");
        if("ORACLE".equals(dbType)){
            final List<String> keys = new ArrayList<String>();
            StringBuffer targetSql = new StringBuffer("select * FROM (").append(sql.toUpperCase()).append(
                    ")  WHERE ROWNUM<=1 ");
            Connection conn = this.getOracleConn(dataSrc);
            QueryRunner qRunner = new QueryRunner();
            try {
                qRunner.query(conn, targetSql.toString(), new ResultSetHandler(){
                    @Override
                    public Object handle(ResultSet rs) throws SQLException {
                        ResultSetMetaData rsmd = rs.getMetaData();
                        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                            keys.add(rsmd.getColumnName(i));
                        }
                        return null;
                    }
                });
            } catch (SQLException e) {
                PlatLogUtil.printStackTrace(e);
            }
            DbUtils.closeQuietly(conn);
            return keys;
        }else if("MYSQL".equals(dbType)){
            return null;
        }else{
            return null;
        }
    }
  
}

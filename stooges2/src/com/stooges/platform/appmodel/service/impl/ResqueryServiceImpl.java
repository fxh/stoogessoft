/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.platform.appmodel.dao.ResqueryDao;
import com.stooges.platform.appmodel.service.ResqueryService;

/**
 * 描述 查询参数业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-25 09:20:52
 */
@Service("resqueryService")
public class ResqueryServiceImpl extends BaseServiceImpl implements ResqueryService {

    /**
     * 所引入的dao
     */
    @Resource
    private ResqueryDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 根据资源ID获取查询参数列表
     * @param resId
     * @return
     */
    public List<Map<String,Object>> findByResId(String resId){
        StringBuffer sql = new StringBuffer("SELECT T.RESQUERY_NAME");
        sql.append(",T.RESQUERY_NOTNULL,T.RESQUERY_DESC FROM PLAT_RESLEDGER_RESQUERY T ");
        sql.append("WHERE T.RESQUERY_RESID=? ORDER BY T.RESQUERY_CREATETIME ASC");
        return dao.findBySql(sql.toString(), new Object[]{resId}, null);
    }
    
    /**
     * 根据资源ID获取非空参数列表
     * @param resId
     * @return
     */
    public List<String> findNotNullList(String resId){
        return dao.findNotNullList(resId);
    }
  
}

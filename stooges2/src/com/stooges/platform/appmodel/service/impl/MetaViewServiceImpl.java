/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.platform.appmodel.dao.MetaViewDao;
import com.stooges.platform.appmodel.service.MetaViewService;

/**
 * 描述 元数据视图业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-21 09:03:25
 */
@Service("metaViewService")
public class MetaViewServiceImpl extends BaseServiceImpl implements MetaViewService {

    /**
     * 所引入的dao
     */
    @Resource
    private MetaViewDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 判断是否存在该视图
     * @param viewName
     * @return
     */
    public boolean isExistsView(String viewName){
        return dao.isExistsView(viewName);
    }
  
}

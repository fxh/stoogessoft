/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.stooges.core.dao.BaseDao;
import com.stooges.core.model.SqlFilter;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.platform.appmodel.dao.SerApplyDao;
import com.stooges.platform.appmodel.service.SerApplyService;

/**
 * 描述 服务申请记录业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-26 17:54:49
 */
@Service("serApplyService")
public class SerApplyServiceImpl extends BaseServiceImpl implements SerApplyService {

    /**
     * 所引入的dao
     */
    @Resource
    private SerApplyDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 获取Ip地址列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findIpAddress(SqlFilter sqlFilter){
        String SERAPPLY_ID = sqlFilter.getRequest().getParameter("SERAPPLY_ID");
        if(StringUtils.isNotEmpty(SERAPPLY_ID)){
            Map<String,Object> serApply = dao.getRecord("PLAT_RESLEDGER_SERAPPLY",
                    new String[]{"SERAPPLY_ID"}, new Object[]{SERAPPLY_ID});
            String SERAPPLY_IPS = (String) serApply.get("SERAPPLY_IPS");
            List<Map> ipsList = JSON.parseArray(SERAPPLY_IPS,Map.class);
            return ipsList;
        }else{
            return null;
        }
    }
    
    
    /**
     * 
     * @param serIds
     * @return
     */
    public List<String> findSerApplySerNames(String serIds){
        return dao.findSerApplySerNames(serIds);
    }
    
    /**
     * 根据服务ID获取被授权的IP集合
     * @param serviceId
     * @return
     */
    public Set<String> getAuthIpSet(String serviceId){
        StringBuffer sql = new StringBuffer("SELECT T.SERAPPLY_IPS FROM ");
        sql.append("PLAT_RESLEDGER_SERAPPLY T WHERE T.SERAPPLY_SERIDS LIKE ?");
        sql.append(" AND T.SERAPPLY_RESULT=? ORDER BY T.SERAPPLY_TIME ASC ");
        Set<String> ipSet = new HashSet<String>();
        List<Map<String,Object>> list = dao.findBySql(sql.toString(), 
                new Object[]{"%"+serviceId+"%","1"}, null);
        for(Map<String,Object> map:list){
            String SERAPPLY_IPS = (String) map.get("SERAPPLY_IPS");
            List<Map> ipList = JSON.parseArray(SERAPPLY_IPS, Map.class);
            for(Map ipInfo:ipList){
                String IP_ADDRESS = (String) ipInfo.get("IP_ADDRESS");
                ipSet.add(IP_ADDRESS);
            }
        }
        return ipSet;
    }
  
}

/*
 * Copyright (c) 2005, 2018, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.stooges.core.dao.BaseDao;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.platform.appmodel.dao.ServerStatusDao;
import com.stooges.platform.appmodel.service.ServerStatusService;

/**
 * 描述 服务器硬件监控信息业务相关service实现类
 * @author 李俊
 * @version 1.0
 * @created 2018-03-20 16:00:36
 */
@Service("serverStatusService")
public class ServerStatusServiceImpl extends BaseServiceImpl implements ServerStatusService {

    /**
     * 所引入的dao
     */
    @Resource
    private ServerStatusDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
  
}

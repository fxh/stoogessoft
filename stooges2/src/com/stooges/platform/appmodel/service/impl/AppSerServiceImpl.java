/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.platform.appmodel.dao.AppSerDao;
import com.stooges.platform.appmodel.service.AppSerService;
import com.stooges.platform.system.service.DicTypeService;
import com.stooges.platform.system.service.DictionaryService;

/**
 * 描述 APP服务业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-08-02 17:11:23
 */
@Service("appSerService")
public class AppSerServiceImpl extends BaseServiceImpl implements AppSerService {

    /**
     * 所引入的dao
     */
    @Resource
    private AppSerDao dao;
    
    /**
     * 
     */
    @Resource
    private DictionaryService dictionaryService;
    /**
     * 
     */
    @Resource
    private DicTypeService dicTypeService;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 获取字典数据列表
     * @param request
     * @param postParams
     * @return
     */
    public Map<String,Object> findDicList(HttpServletRequest request,
            Map<String,Object> postParams){
        String TYPE_CODE = (String) postParams.get("TYPE_CODE");
        List<Map<String,Object>> dataList = dictionaryService.findList(TYPE_CODE, "ASC");
        List<Map<String,Object>> datas = new ArrayList<Map<String,Object>>();
        for(Map<String,Object> data:dataList){
            Map<String,Object> dic = new HashMap<String,Object>();
            dic.put("DIC_NAME", data.get("DIC_NAME"));
            dic.put("DIC_VALUE", data.get("DIC_VALUE"));
            datas.add(dic);
        }
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        result.put("datas", datas);
        return result;
    }
    
    /**
     * 获取字典类别数据列表
     * @param request
     * @param postParams
     * @return
     */
    public Map<String,Object> findDicTypeList(HttpServletRequest request,
            Map<String,Object> postParams){
        String PARENT_TYPECODE = (String) postParams.get("PARENT_TYPECODE");
        List<Map<String,Object>> dicTypeList = dicTypeService.findChildren(PARENT_TYPECODE);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        result.put("datas", dicTypeList);
        return result;
    }
  
}

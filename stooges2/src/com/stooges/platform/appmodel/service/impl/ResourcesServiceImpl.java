/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service.impl;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.stooges.core.dao.BaseDao;
import com.stooges.core.model.PagingBean;
import com.stooges.core.model.SqlFilter;
import com.stooges.core.model.TableColumn;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatAppUtil;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatDbUtil;
import com.stooges.core.util.PlatLogUtil;
import com.stooges.core.util.PlatStringUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.appmodel.dao.ResourcesDao;
import com.stooges.platform.appmodel.service.DataSrcService;
import com.stooges.platform.appmodel.service.DbManagerService;
import com.stooges.platform.appmodel.service.ResourcesService;

/**
 * 描述 资源信息表业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-24 09:15:27
 */
@Service("resourcesService")
public class ResourcesServiceImpl extends BaseServiceImpl implements ResourcesService {

    /**
     * 所引入的dao
     */
    @Resource
    private ResourcesDao dao;
    /**
     * 
     */
    @Resource
    private DbManagerService dbManagerService;
    /**
     * 
     */
    @Resource
    private DataSrcService dataSrcService;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 获取下一个资源编号
     * @return
     */
    public String getNextResNumber(){
        int number = dao.getMaxResNumber();
        int nextNumber = number+1;
        return PlatStringUtil.getFormatNumber(6, String.valueOf(nextNumber));
    }
    
    /**
     * 获取配置的返回结果列表
     * 返回字段结果JSON(FIELD_NAME:字段名称 FIELD_DESC:字段描述)
     * @param sqlFilter
     * @return
     */
    public List<Map> findReturnFields(SqlFilter sqlFilter){
        String RESOURCES_ID = sqlFilter.getRequest().getParameter("RESOURCES_ID");
        if(StringUtils.isNotEmpty(RESOURCES_ID)){
            Map<String,Object> resources = dao.getRecord("PLAT_RESLEDGER_RESOURCES",
                    new String[]{"RESOURCES_ID"},new Object[]{RESOURCES_ID});
            String RESOURCES_RETURNFIELDJSON = (String) resources.get("RESOURCES_RETURNFIELDJSON");
            if(StringUtils.isNotEmpty(RESOURCES_RETURNFIELDJSON)){
                return JSON.parseArray(RESOURCES_RETURNFIELDJSON, Map.class);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    /**
     * 获取配置的元数据列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findMetaTables(SqlFilter sqlFilter){
        String RESOURCES_ID = sqlFilter.getRequest().getParameter("RESOURCES_ID");
        if(StringUtils.isNotEmpty(RESOURCES_ID)){
            Map<String,Object> resources = dao.getRecord("PLAT_RESLEDGER_RESOURCES",
                    new String[]{"RESOURCES_ID"},new Object[]{RESOURCES_ID});
            String RESOURCES_METAJSON = (String) resources.get("RESOURCES_METAJSON");
            if(StringUtils.isNotEmpty(RESOURCES_METAJSON)){
                return JSON.parseArray(RESOURCES_METAJSON, Map.class);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    /**
     * 获取配置的排序条件列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findOrderFields(SqlFilter sqlFilter){
        String RESOURCES_ID = sqlFilter.getRequest().getParameter("RESOURCES_ID_EQ");
        if(StringUtils.isNotEmpty(RESOURCES_ID)){
            Map<String,Object> resources = dao.getRecord("PLAT_RESLEDGER_RESOURCES",
                    new String[]{"RESOURCES_ID"},new Object[]{RESOURCES_ID});
            String RESOURCES_ORDER = (String) resources.get("RESOURCES_ORDER");
            if(StringUtils.isNotEmpty(RESOURCES_ORDER)){
                return JSON.parseArray(RESOURCES_ORDER, Map.class);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    
    /**
     * 保存基本信息并且级联保存列JSON
     * @param request
     * @return
     */
    public Map<String,Object> saveBaseInfo(HttpServletRequest request){
        Map<String,Object> resources = PlatBeanUtil.getMapFromRequest(request);
        String RESOURCES_ID = request.getParameter("RESOURCES_ID");
        if(StringUtils.isEmpty(RESOURCES_ID)){
            //获取下一个编号
            String nextResNumber = this.getNextResNumber();
            resources.put("RESOURCES_NUM",nextResNumber);
        }
        resources = dao.saveOrUpdate("PLAT_RESLEDGER_RESOURCES",
                resources,AllConstants.IDGENERATOR_UUID,null);
        return resources;
    }
    
    /**
     * 保存数据源
     * @param request
     * @return
     */
    public Map<String,Object> saveDataSource(HttpServletRequest request){
        Map<String,Object> resources = PlatBeanUtil.getMapFromRequest(request);
        String RESOURCES_DATATYPE = request.getParameter("RESOURCES_DATATYPE");
        String resultFieldJson = this.getReturnFieldJson(RESOURCES_DATATYPE, resources);
        resources.put("RESOURCES_RETURNFIELDJSON",resultFieldJson);
        resources = dao.saveOrUpdate("PLAT_RESLEDGER_RESOURCES",
                resources,AllConstants.IDGENERATOR_UUID,null);
        return resources;
    }
    
    /**
     * 获取返回字段JSON
     * @param dataType
     * @param resources
     * @return
     */
    private String getReturnFieldJson(String dataType,Map<String,Object> resources){
        List<Map<String,Object>> columnList = null;
        String resourcesId = (String) resources.get("RESOURCES_ID");
        if(dataType.equals(ResourcesService.DATATYPE_SQL)){
            String SQL_CONTENT = (String) resources.get("RESOURCES_SQL");
            SQL_CONTENT = SQL_CONTENT.toUpperCase();
            //获取后台登录用户
            Map<String,Object> backLoginUser = PlatAppUtil.getBackPlatLoginUser();
            Map<String,Object> root = new HashMap<String,Object>();
            root.put(AllConstants.BACKPLAT_USERSESSIONKEY, backLoginUser);
            SQL_CONTENT = PlatStringUtil.getFreeMarkResult(SQL_CONTENT, root);
            columnList = dataSrcService.findFieldColumnsBySql(SQL_CONTENT, resourcesId);
            if(columnList!=null&&columnList.size()>0){
                String RESOURCES_ID  = (String) resources.get("RESOURCES_ID");
                Map<String,Object> oldRes = dao.getRecord("PLAT_RESLEDGER_RESOURCES",
                        new String[]{"RESOURCES_ID"},new Object[]{RESOURCES_ID});
                String oldReturnFieldJson = (String) oldRes.get("RESOURCES_RETURNFIELDJSON");
                List<Map> oldReturnFieldList = new ArrayList<Map>();
                if(StringUtils.isNotEmpty(oldReturnFieldJson)){
                    oldReturnFieldList = JSON.parseArray(oldReturnFieldJson, Map.class);
                }
                for(Map<String,Object> column:columnList){
                    String FIELD_NAME = (String) column.get("FIELD_NAME");
                    for(Map oldColumn:oldReturnFieldList){
                        if(oldColumn.get("FIELD_NAME").toString().equals(FIELD_NAME)){
                            String oldComment = (String) oldColumn.get("FIELD_COMMENT");
                            column.put("FIELD_COMMENT", oldComment);
                        }
                    }
                }
                return JSON.toJSONString(columnList);
            }else{
                return null;
            }
        }else{
            String RESOURCES_ID  = (String) resources.get("RESOURCES_ID");
            Map<String,Object> oldRes = dao.getRecord("PLAT_RESLEDGER_RESOURCES",
                    new String[]{"RESOURCES_ID"},new Object[]{RESOURCES_ID});
            String oldReturnFieldJson = (String) oldRes.get("RESOURCES_RETURNFIELDJSON");
            return oldReturnFieldJson;
        }
        
    }
    
    /**
     * 获取可选元数据列表
     * @return
     */
    public List<Map<String,Object>> getAllMetaList(String paramJson){
        StringBuffer sql = new StringBuffer("SELECT T.TABLE_NAME,");
        sql.append("T.TABLE_DESC FROM PLAT_RESLEDGER_TABLE T ");
        sql.append("ORDER BY T.TABLE_CREATETIME ASC ");
        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
        List<Map<String,Object>> tableList = dao.findBySql(sql.toString(), null, null);
        for(Map<String,Object> table:tableList){
            Map<String,Object> meta = new HashMap<String,Object>();
            String TABLE_NAME = (String) table.get("TABLE_NAME");
            String TABLE_DESC = (String) table.get("TABLE_DESC");
            StringBuffer LABEL = new StringBuffer(TABLE_DESC);
            LABEL.append("(").append(TABLE_NAME).append(")");
            meta.put("VALUE", TABLE_NAME);
            meta.put("LABEL", LABEL.toString());
            meta.put("META_TYPE", "1");
            list.add(meta);
        }
        sql = new StringBuffer("SELECT T.VIEW_NAME,");
        sql.append("T.VIEW_DESC FROM PLAT_RESLEDGER_VIEW T ");
        sql.append("ORDER BY T.VIEW_CREATETIME ASC ");
        List<Map<String,Object>> viewList = dao.findBySql(sql.toString(), null, null);
        for(Map<String,Object> view:viewList){
            Map<String,Object> meta = new HashMap<String,Object>();
            String VIEW_NAME = (String) view.get("VIEW_NAME");
            String VIEW_DESC = (String) view.get("VIEW_DESC");
            StringBuffer LABEL = new StringBuffer(VIEW_DESC);
            LABEL.append("(").append(VIEW_NAME).append(")");
            meta.put("VALUE", VIEW_NAME);
            meta.put("LABEL", LABEL.toString());
            meta.put("META_TYPE", "2");
            list.add(meta);
        }
        return list;
    }
    
    /**
     * 获取可选的表字段信息
     * @param params
     * @return
     */
    public List<Map<String,Object>> findSelectFields(String resourcesId){
        if(StringUtils.isNotEmpty(resourcesId)){
            Map<String,Object> resources = this.getRecord("PLAT_RESLEDGER_RESOURCES",
                    new String[]{"RESOURCES_ID"},new Object[]{resourcesId});
            String ASSOICAL_JSON = (String) resources.get("RESOURCES_METAJSON");
            String dataSrcId = (String) resources.get("RESOURCES_DATASRCID");
            if(StringUtils.isNotEmpty(ASSOICAL_JSON)){
                List<Map> assoicalTables = JSON.parseArray(ASSOICAL_JSON, Map.class);
                List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
                for(Map table:assoicalTables){
                    String TABLE_NAME = (String) table.get("META_NAME");
                    String TABLE_ALIAS = (String) table.get("ALAS_NAME");
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("VALUE", TABLE_NAME);
                    map.put("LABEL", TABLE_NAME);
                    map.put("TREE_LEVEL", 1);
                    list.add(map);
                    List<TableColumn> columnList = dataSrcService.findTableColumnByTableName(dataSrcId, TABLE_NAME);
                    for(TableColumn column:columnList){
                        Map<String,Object> field = new HashMap<String,Object>();
                        field.put("TABLE_NAME", TABLE_NAME);
                        field.put("FIELD_COMMENTS", column.getColumnComments());
                        field.put("LABEL", column.getColumnName()+"("+column.getColumnComments()+")");
                        field.put("VALUE", TABLE_ALIAS+"."+column.getColumnName());
                        field.put("ISLEAF", "true");
                        field.put("TREE_LEVEL", 2);
                        list.add(field);
                    }
                }
                list = PlatUICompUtil.getTreeSelectDatas(list);
                return list;
            }
        }
        return null;
    }
    
    /**
     * 删除资源信息,并且级联删除子表
     * @param resourcesIds
     */
    public void deleteCascadeSub(String resourcesIds){
        StringBuffer sql = new StringBuffer("DELETE FROM PLAT_RESLEDGER_RESQUERY");
        sql.append(" WHERE RESQUERY_RESID IN ").append(PlatStringUtil.getSqlInCondition(resourcesIds));
        dao.executeSql(sql.toString(), null);
        this.deleteRecords("PLAT_RESLEDGER_RESOURCES","RESOURCES_ID",resourcesIds.split(","));
    }
    
    /**
     * 根据目录ID获取资源数据列表
     * @param catalogId
     * @return
     */
    public List<Map<String,Object>> findForSelect(String catalogId){
        if(StringUtils.isNotEmpty(catalogId)){
            StringBuffer sql = new StringBuffer("SELECT ");
            sql.append("T.RESOURCES_ID AS VALUE,T.RESOURCES_NAME AS LABEL");
            sql.append(" FROM PLAT_RESLEDGER_RESOURCES T ");
            sql.append(" WHERE T.RESOURCES_CATALOGID=? ORDER BY");
            sql.append(" T.RESOURCES_CREATETIME DESC ");
            return dao.findBySql(sql.toString(), new Object[]{catalogId},null);
        }else{
            return null;
        }
        
    }
    
    /**
     * 获取返回字段列表
     * @param resourcesId
     * @return
     */
    public List<Map> findReturnFields(String resourcesId){
        Map<String,Object> resources = dao.getRecord("PLAT_RESLEDGER_RESOURCES", 
                new String[]{"RESOURCES_ID"},new Object[]{resourcesId});
        String RESOURCES_RETURNFIELDJSON = (String) resources.get("RESOURCES_RETURNFIELDJSON");
        if(StringUtils.isNotEmpty(RESOURCES_RETURNFIELDJSON)){
            return JSON.parseArray(RESOURCES_RETURNFIELDJSON, Map.class);
        }else{
            return null;
        }
    }
    
    /**
     * 获取数据列表
     * @param resourcesId
     * @return
     */
    public List<Map<String,Object>> findQueryData(String resourcesId,HttpServletRequest request,Integer maxLine){
        Map<String,Object> resources = dao.getRecord("PLAT_RESLEDGER_RESOURCES", 
                new String[]{"RESOURCES_ID"},new Object[]{resourcesId});
        String RESOURCES_DATATYPE = (String) resources.get("RESOURCES_DATATYPE");
        if(ResourcesService.DATATYPE_SQL.equals(RESOURCES_DATATYPE)){
            SqlFilter filter = new SqlFilter(request);
            return this.findSqlDatas(filter, resources,maxLine);
        }else if(ResourcesService.DATATYPE_JAVA.equals(RESOURCES_DATATYPE)){
            SqlFilter filter = new SqlFilter(request);
            return this.findJavaDatas(filter, resources);
        }else{
            return null;
        }
    }
    
    /**
     * 获取JAVA接口的数据列表
     * @param filter
     * @param resources
     * @return
     */
    private List<Map<String,Object>> findJavaDatas(SqlFilter filter,Map<String,Object> resources){
        String JAVA_INTERFACE = (String) resources.get("RESOURCES_INTER");
        String beanId = JAVA_INTERFACE.split("[.]")[0];
        String method = JAVA_INTERFACE.split("[.]")[1];
        Object serviceBean = PlatAppUtil.getBean(beanId);
        List<Map<String,Object>> list = null;
        if (serviceBean != null) {
            Method invokeMethod;
            try {
                invokeMethod = serviceBean.getClass().getDeclaredMethod(method,
                        new Class[] { SqlFilter.class});
                list = (List<Map<String,Object>>) invokeMethod.invoke(serviceBean,
                        new Object[] { filter});
            }catch (Exception e) {
                PlatLogUtil.printStackTrace(e);
            } 
        }
        return list;
    }
    
    /**
     * 获取SQL数据列表
     * @param filter
     * @param resources
     * @return
     */
    private List<Map<String,Object>> findSqlDatas(SqlFilter filter,
            Map<String,Object> resources,Integer maxLine){
        String RESOURCES_ORDER = (String) resources.get("RESOURCES_ORDER");
        String RESOURCES_SQL = (String) resources.get("RESOURCES_SQL");
        String dataSrcId = (String) resources.get("RESOURCES_DATASRCID");
        if(StringUtils.isNotEmpty(RESOURCES_ORDER)){
            List<Map> orderFields = JSON.parseArray(RESOURCES_ORDER,Map.class);
            for(Map<String,Object> map:orderFields){
                String ORDER_FIELD = (String) map.get("ORDER_FIELD");
                String ORDER_TYPE = (String) map.get("ORDER_TYPE");
                filter.addFilter("O_"+ORDER_FIELD,ORDER_TYPE,SqlFilter.FILTER_TYPE_ORDER);
            }
        }
        StringBuffer sql = new StringBuffer(RESOURCES_SQL);
        List params = new ArrayList();
        String exeSql = dao.getQuerySql(filter, sql.toString(), params);
        Map<String,Object> dataSrc = this.getRecord("PLAT_RESLEDGER_DATASRC",
                new String[]{"DATASRC_ID"},new Object[]{dataSrcId});
        //获取数据库类型
        String DATASRC_DBTYPE = (String) dataSrc.get("DATASRC_DBTYPE");
        if(DATASRC_DBTYPE.equals("ORACLE")){
            Connection conn = dataSrcService.getOracleConn(dataSrc);
            List<Map<String,Object>> dataList = null;
            if(maxLine!=-1){
                PagingBean pb = new PagingBean(0,maxLine);
                dataList = PlatDbUtil.findBySql(conn, exeSql, params.toArray(), 
                        pb,DATASRC_DBTYPE);
            }else{
                dataList = PlatDbUtil.findBySql(conn, exeSql, params.toArray());
            }
            DbUtils.closeQuietly(conn);
            return dataList;
        }else{
            return null;
        }
    }
  
}

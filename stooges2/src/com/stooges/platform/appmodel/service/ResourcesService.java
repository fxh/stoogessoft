/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.stooges.core.model.SqlFilter;
import com.stooges.core.service.BaseService;

/**
 * 描述 资源信息表业务相关service
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-24 09:15:27
 */
public interface ResourcesService extends BaseService {
    /**
     * 数据源类型:SQL语句
     */
    public static final String DATATYPE_SQL = "1";
    /**
     * 数据源类型:JAVA接口
     */
    public static final String DATATYPE_JAVA = "2";
    
    /**
     * 获取下一个资源编号
     * @return
     */
    public String getNextResNumber();
    
    /**
     * 获取配置的返回结果列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findReturnFields(SqlFilter sqlFilter);
    /**
     * 获取配置的元数据列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findMetaTables(SqlFilter sqlFilter);
    
    /**
     * 获取配置的排序条件列表
     * @param sqlFilter
     * @return
     */
    public List<Map> findOrderFields(SqlFilter sqlFilter);
    /**
     * 保存基本信息并且级联保存列JSON
     * @param request
     * @return
     */
    public Map<String,Object> saveBaseInfo(HttpServletRequest request);
    
    
    /**
     * 获取可选元数据列表
     * @return
     */
    public List<Map<String,Object>> getAllMetaList(String paramJson);
    /**
     * 保存数据源
     * @param request
     * @return
     */
    public Map<String,Object> saveDataSource(HttpServletRequest request);
    
    /**
     * 获取可选的表字段信息
     * @param params
     * @return
     */
    public List<Map<String,Object>> findSelectFields(String resourcesId);
    /**
     * 删除资源信息,并且级联删除子表
     * @param resourcesIds
     */
    public void deleteCascadeSub(String resourcesIds);
    /**
     * 根据目录ID获取资源数据列表
     * @param catalogId
     * @return
     */
    public List<Map<String,Object>> findForSelect(String catalogId);
    /**
     * 获取返回字段列表
     * @param resourcesId
     * @return
     */
    public List<Map> findReturnFields(String resourcesId);
    /**
     * 获取数据列表
     * @param resourcesId
     * @return
     */
    public List<Map<String,Object>> findQueryData(String resourcesId,
            HttpServletRequest request,Integer maxLine);
}

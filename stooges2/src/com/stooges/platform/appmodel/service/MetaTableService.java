/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.appmodel.service;

import java.util.Map;

import com.stooges.core.service.BaseService;

/**
 * 描述 元数据表业务相关service
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-20 11:13:48
 */
public interface MetaTableService extends BaseService {
    /**
     * 判断是否有这张表记录
     * @param tableName
     * @return
     */
    public boolean isExistsTable(String tableName);
    /**
     * 更新元数据表信息并且级联更新数据库字段
     * @param tableInfo
     * @return
     */
    public Map<String,Object> updateTableCascadeColumn(Map<String,Object> tableInfo);
}

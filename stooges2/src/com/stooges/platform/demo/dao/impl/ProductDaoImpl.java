/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.demo.dao.impl;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.core.util.PlatStringUtil;
import com.stooges.platform.demo.dao.ProductDao;

/**
 * 描述产品信息业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-07-10 13:48:25
 */
@Repository
public class ProductDaoImpl extends BaseDaoImpl implements ProductDao {

    /**
     * 
     * @param productIds
     */
    public void updateIsShow(String productIds,String isShow){
        StringBuffer sql = new StringBuffer("UPDATE PLAT_DEMO_PRODUCT");
        sql.append(" T SET T.PRODUCT_ISSHOW=? ");
        sql.append("WHERE T.PRODUCT_ID IN ").append(PlatStringUtil.getSqlInCondition(productIds));
        this.getJdbcTemplate().update(sql.toString(), new Object[]{isShow});
    }
}

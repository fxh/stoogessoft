/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.system.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.stooges.core.model.SqlFilter;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.DictionaryService;
import com.stooges.platform.system.service.RoleRightService;
import com.stooges.platform.system.service.RoleService;
import com.stooges.platform.system.service.SysLogService;
import com.stooges.platform.system.service.SysUserService;

/**
 * 
 * 描述 角色业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-04-15 15:20:40
 */
@Controller  
@RequestMapping("/system/RoleController")  
public class RoleController extends BaseController {
    /**
     * 
     */
    @Resource
    private RoleService roleService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    /**
     * 
     */
    @Resource
    private SysUserService sysUserService;
    /**
     * 
     */
    @Resource
    private RoleRightService roleRightService;
    /**
     * 
     */
    @Resource
    private DictionaryService dictionaryService;
    
    /**
     * 自动补全
     * @param request
     * @param response
     */
    @RequestMapping(params = "autoRoleAndUser")
    public void autoRoleAndUser(HttpServletRequest request,
            HttpServletResponse response) {
        //如果自动补全的类型为2,那么获取到key之后进行判断过滤
        //String keyword = request.getParameter("keyword");
        SqlFilter sqlFilter = new SqlFilter(request);
        List<Map<String,Object>> list = roleService.findAutoRoleUser(sqlFilter);
        String json = JSON.toJSONString(list);
        this.printJsonString(json.toLowerCase(), response);
    }
    
    /**
     * 删除角色数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String selectColValues = request.getParameter("selectColValues");
        roleService.deleteCascadeUserAndRes(selectColValues);
        sysLogService.saveBackLog("角色管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+selectColValues+"]的角色信息", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改角色数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> role = PlatBeanUtil.getMapFromRequest(request);
        String ROLE_ID = (String) role.get("ROLE_ID");
        role = roleService.saveOrUpdate("PLAT_SYSTEM_ROLE",
                role,AllConstants.IDGENERATOR_UUID,null);
        if(StringUtils.isNotEmpty(ROLE_ID)){
            sysLogService.saveBackLog("角色管理",SysLogService.OPER_TYPE_EDIT,
                    "修改了ID为["+ROLE_ID+"]的角色信息", request);
        }else{
            ROLE_ID = (String) role.get("ROLE_ID");
            sysLogService.saveBackLog("角色管理",SysLogService.OPER_TYPE_ADD,
                    "新增了ID为["+ROLE_ID+"]的角色信息", request);
        }
        role.put("success", true);
        this.printObjectJsonString(role, response);
    }
    
    /**
     * 跳转到角色表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        String ROLE_ID = request.getParameter("ROLE_ID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        String roleGroupId = request.getParameter("roleGroupId");
        Map<String,Object> role = null;
        if(StringUtils.isNotEmpty(ROLE_ID)){
            role = this.roleService.getRecord("PLAT_SYSTEM_ROLE"
                    ,new String[]{"ROLE_ID"},new Object[]{ROLE_ID});
        }else{
            role = new HashMap<String,Object>();
            if(StringUtils.isNotEmpty(roleGroupId)){
                role.put("ROLE_GROUPID", roleGroupId);
            }
        }
        request.setAttribute("role", role);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
    
    /**
     * 获取树形的角色和用户数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "roleAndUsers")
    public void roleAndUsers(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> params= PlatBeanUtil.getMapFromRequest(request);
        String departJson = roleService.getRoleAndUserJson(params);
        this.printJsonString(departJson, response);
    }
    
    /**
     * 跳转到系统用户选择界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goUserGrant")
    public ModelAndView goUserGrant(HttpServletRequest request) {
        String ROLE_ID = request.getParameter("ROLE_ID");
        List<String> userIds = sysUserService.findUserIds(ROLE_ID);
        StringBuffer selectedRecordIds = new StringBuffer("");
        for(int i=0;i<userIds.size();i++){
            if(i>0){
                selectedRecordIds.append(",");
            }
            selectedRecordIds.append(userIds.get(i));
        }
        request.setAttribute("selectedRecordIds", selectedRecordIds.toString());
        return PlatUICompUtil.goDesignUI("userselector", request);
    }
    
    /**
     * 分配用户
     * @param request
     * @param response
     */
    @RequestMapping(params = "grantUsers")
    public void grantUsers(HttpServletRequest request,
            HttpServletResponse response) {
        String ROLE_ID = request.getParameter("ROLE_ID");
        String checkUserIds = request.getParameter("checkUserIds");
        roleService.saveUsers(ROLE_ID, Arrays.asList(checkUserIds.split(",")));
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 跳转到权限授权界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goRightGrant")
    public ModelAndView goRightGrant(HttpServletRequest request) {
        String ROLE_ID = request.getParameter("ROLE_ID");
        List<String> rightRecordIds = roleRightService.getRightRecordIds(ROLE_ID);
        StringBuffer selectedRecordIds = new StringBuffer("");
        for(int i=0;i<rightRecordIds.size();i++){
            if(i>0){
                selectedRecordIds.append(",");
            }
            selectedRecordIds.append(rightRecordIds.get(i));
        }
        //==================开始三员管理员特殊业务代码注入===================
        List<Map<String,Object>> threeMan = dictionaryService.findList("threememspecial", "ASC");
        if(threeMan!=null&&threeMan.size()>0){
            StringBuffer threeManagerCode = new StringBuffer("");
            for(int i=0;i<threeMan.size();i++){
                if(i>0){
                    threeManagerCode.append(",");
                }
                threeManagerCode.append(threeMan.get(i).get("DIC_VALUE"));
            }
            request.setAttribute("threeManagerCode", threeManagerCode.toString());
        }
        //==================结束三员管理员特殊业务代码注入===================
        request.setAttribute("selectedRecordIds", selectedRecordIds.toString());
        request.setAttribute("ROLE_ID", ROLE_ID);
        return PlatUICompUtil.goDesignUI("rolegrantform", request);
    }
    
    /**
     * 分配权限
     * @param request
     * @param response
     */
    @RequestMapping(params = "grantRights")
    public void grantRights(HttpServletRequest request,
            HttpServletResponse response) {
        String ROLE_ID = request.getParameter("ROLE_ID");
        String resIds = request.getParameter("resIds");
        String groupIds = request.getParameter("groupIds");
        String typedefIds = request.getParameter("typedefIds");
        List<String> resIdArray = new ArrayList<String>();
        if(StringUtils.isNotEmpty(resIds)){
            resIdArray = Arrays.asList(resIds.split(","));
        }
        List<String> groupIdArray = new ArrayList<String>();
        if(StringUtils.isNotEmpty(groupIds)){
            groupIdArray = Arrays.asList(groupIds.split(","));
        }
        List<String> typeAndDefIdArray = new ArrayList<String>();
        if(StringUtils.isNotEmpty(typedefIds)){
            typeAndDefIdArray = Arrays.asList(typedefIds.split(","));
        }
        roleRightService.saveRights(ROLE_ID,resIdArray,groupIdArray,typeAndDefIdArray);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.PlatBeanUtil;
import com.stooges.core.util.PlatUICompUtil;
import com.stooges.platform.common.controller.BaseController;
import com.stooges.platform.system.service.CompanyService;
import com.stooges.platform.system.service.SysLogService;

/**
 * 
 * 描述 单位业务相关Controller
 * @author 胡裕
 * @version 1.0
 * @created 2017-04-09 09:43:59
 */
@Controller  
@RequestMapping("/system/CompanyController")  
public class CompanyController extends BaseController {
    /**
     * 
     */
    @Resource
    private CompanyService companyService;
    /**
     * 
     */
    @Resource
    private SysLogService sysLogService;
    
    /**
     * 删除单位数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "multiDel")
    public void multiDel(HttpServletRequest request,
            HttpServletResponse response) {
        String companyId = request.getParameter("treeNodeId");
        companyService.deleteCompanyCacasdeAssocial(companyId);
        sysLogService.saveBackLog("单位部门管理",SysLogService.OPER_TYPE_DEL,
                "删除了ID为["+companyId+"]的单位信息", request);
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("success", true);
        this.printObjectJsonString(result, response);
    }
    
    /**
     * 新增或者修改单位数据
     * @param request
     * @param response
     */
    @RequestMapping(params = "saveOrUpdate")
    public void saveOrUpdate(HttpServletRequest request,
            HttpServletResponse response) {
        Map<String,Object> company = PlatBeanUtil.getMapFromRequest(request);
        String COMPANY_ID = (String) company.get("COMPANY_ID");
        //如果是保存树形结构表数据,请调用下面的接口,而注释掉上面的接口代码
        company = companyService.saveOrUpdateTreeData("PLAT_SYSTEM_COMPANY",
                company,AllConstants.IDGENERATOR_UUID,null);
        if(StringUtils.isNotEmpty(COMPANY_ID)){
            sysLogService.saveBackLog("单位部门管理",SysLogService.OPER_TYPE_EDIT,
                    "修改了ID为["+COMPANY_ID+"]单位信息", request);
        }else{
            COMPANY_ID = (String) company.get("COMPANY_ID");
            sysLogService.saveBackLog("单位部门管理",SysLogService.OPER_TYPE_ADD,
                    "新增了ID为["+COMPANY_ID+"]单位信息", request);
        }
        company.put("success", true);
        this.printObjectJsonString(company, response);
    }
    
    /**
     * 跳转到单位表单界面
     * @param request
     * @return
     */
    @RequestMapping(params = "goForm")
    public ModelAndView goForm(HttpServletRequest request) {
        //如果是跳转到树形表单录入界面,请开放以下代码,,而注释掉上面的代码
        String COMPANY_ID = request.getParameter("COMPANY_ID");
        String COMPANY_PARENTID = request.getParameter("COMPANY_PARENTID");
        //获取设计的界面编码
        String UI_DESIGNCODE = request.getParameter("UI_DESIGNCODE");
        Map<String,Object> company = null;
        if(StringUtils.isNotEmpty(COMPANY_ID)){
            company = this.companyService.getRecord("PLAT_SYSTEM_COMPANY"
                    ,new String[]{"COMPANY_ID"},new Object[]{COMPANY_ID});
            COMPANY_PARENTID = (String) company.get("Company_PARENTID");
        }
        Map<String,Object> parentCompany = null;
        if(COMPANY_PARENTID.equals("0")){
            parentCompany = new HashMap<String,Object>();
            parentCompany.put("COMPANY_ID",COMPANY_PARENTID);
            parentCompany.put("COMPANY_NAME","单位树");
        }else{
            parentCompany = this.companyService.getRecord("PLAT_SYSTEM_COMPANY",
                    new String[]{"COMPANY_ID"}, new Object[]{COMPANY_PARENTID});
        }
        if(company==null){
            company = new HashMap<String,Object>();
            company.put("COMPANY_PROVINCECODE","350000");
        }
        company.put("COMPANY_PARENTID",parentCompany.get("COMPANY_ID"));
        company.put("COMPANY_PARENTNAME",parentCompany.get("COMPANY_NAME"));
        request.setAttribute("company", company);
        return PlatUICompUtil.goDesignUI(UI_DESIGNCODE, request);
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.system.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.system.dao.SysUserDao;

/**
 * 描述系统用户业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-04-10 17:03:31
 */
@Repository
public class SysUserDaoImpl extends BaseDaoImpl implements SysUserDao {

    /**
     * 根据角色ID获取用户IDS
     * @param roleId
     * @return
     */
    public List<String> findUserIds(String roleId){
        StringBuffer sql = new StringBuffer("SELECT T.SYSUSER_ID ");
        sql.append("FROM PLAT_SYSTEM_SYSUSERROLE T");
        sql.append(" WHERE T.ROLE_ID=? ORDER BY T.SYSUSER_ID ASC");
        List<String> list = this.getJdbcTemplate().queryForList(sql.toString(),
                new Object[]{roleId},String.class);
        return list;
    }
    
    /**
     * 获取被授权的资源ID集合
     * @param userId
     * @return
     */
    public List<String> findGrantRightIds(String userId){
        StringBuffer sql = new StringBuffer("SELECT R.RE_RECORDID ");
        sql.append("FROM PLAT_SYSTEM_ROLERIGHT R WHERE R.ROLE_ID");
        sql.append(" IN (SELECT UR.ROLE_ID FROM PLAT_SYSTEM_SYSUSERROLE UR");
        sql.append(" WHERE UR.SYSUSER_ID=? )");
        return this.getJdbcTemplate().queryForList(sql.toString(), new Object[]{userId},
                String.class);
    }
    
    /**
     * 判断是否存在用户
     * @param userId
     * @return
     */
    public boolean isExistsUser(String userId){
        StringBuffer sql = new StringBuffer("SELECT COUNT(*) FROM ");
        sql.append("PLAT_SYSTEM_SYSUSER U WHERE U.SYSUSER_ID=? ");
        int count = this.getIntBySql(sql.toString(), new Object[]{userId});
        if(count==0){
            return false;
        }else{
            return true;
        }
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.system.dao.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.system.dao.RoleGroupDao;

/**
 * 描述角色组业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-04-15 15:20:40
 */
@Repository
public class RoleGroupDaoImpl extends BaseDaoImpl implements RoleGroupDao {

    /**
     * 根据用户ID
     * @param userId
     * @return
     */
    public Set<String> getUserGrantGroupIds(String userId){
        StringBuffer sql = new StringBuffer("SELECT T.RE_RECORDID FROM  PLAT_SYSTEM_ROLERIGHT T");
        sql.append(" WHERE T.RE_TABLENAME='PLAT_SYSTEM_ROLEGROUP' AND T.ROLE_ID IN ");
        sql.append("(SELECT SR.ROLE_ID FROM PLAT_SYSTEM_SYSUSERROLE SR");
        sql.append(" WHERE SR.SYSUSER_ID=? )");
        List<String> resCodeList = this.getJdbcTemplate()
                .queryForList(sql.toString(),new Object[]{userId},String.class);
        if(resCodeList!=null&&resCodeList.size()>0){
            return new HashSet<String>(resCodeList);
        }else{
            return null;
        }
    }
}

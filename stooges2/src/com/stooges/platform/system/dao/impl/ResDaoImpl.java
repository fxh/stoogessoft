/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.system.dao.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.stooges.core.dao.impl.BaseDaoImpl;
import com.stooges.platform.system.dao.ResDao;

/**
 * 描述系统资源业务相关dao实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-04-05 17:12:34
 */
@Repository
public class ResDaoImpl extends BaseDaoImpl implements ResDao {

    /**
     * 根据用户ID获取用户被授权的资源KEY集合
     * @param userId
     * @return
     */
    public Set<String> getUserGrantResCodes(String userId){
        StringBuffer sql = new StringBuffer("select R.RES_CODE from PLAT_SYSTEM_RES R ");
        sql.append("WHERE R.RES_ID IN (SELECT T.RE_RECORDID FROM  PLAT_SYSTEM_ROLERIGHT T");
        sql.append(" WHERE T.RE_TABLENAME='PLAT_SYSTEM_RES' AND T.ROLE_ID IN ");
        sql.append("(SELECT SR.ROLE_ID FROM PLAT_SYSTEM_SYSUSERROLE SR");
        sql.append(" WHERE SR.SYSUSER_ID=? ))");
        List<String> resCodeList = this.getJdbcTemplate()
                .queryForList(sql.toString(),new Object[]{userId},String.class);
        if(resCodeList!=null&&resCodeList.size()>0){
            return new HashSet<String>(resCodeList);
        }else{
            return null;
        }
    }
}

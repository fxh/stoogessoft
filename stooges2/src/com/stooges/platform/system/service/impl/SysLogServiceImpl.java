/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.system.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.core.util.AllConstants;
import com.stooges.core.util.BrowserUtils;
import com.stooges.core.util.PlatAppUtil;
import com.stooges.core.util.PlatDateTimeUtil;
import com.stooges.platform.system.dao.SysLogDao;
import com.stooges.platform.system.service.SysLogService;

/**
 * 描述 系统日志业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-04-17 16:24:38
 */
@Service("sysLogService")
public class SysLogServiceImpl extends BaseServiceImpl implements SysLogService {

    /**
     * 所引入的dao
     */
    @Resource
    private SysLogDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 保存后台系统日志信息
     * @param moduleName
     * @param operType
     * @param logContent
     */
    public void saveBackLog(String moduleName,int operType,String logContent,HttpServletRequest request){
        Map<String,Object> sysLog = new HashMap<String,Object>();
        String browser = BrowserUtils.checkBrowse(request);
        Map<String,Object> curUser = PlatAppUtil.getBackPlatLoginUser();
        sysLog.put("BROWSER", browser);
        sysLog.put("OPER_TYPE", operType);
        if(curUser!=null){
            sysLog.put("OPER_USERNAME",curUser.get("SYSUSER_NAME"));
            sysLog.put("OPER_USERACCOUNT",curUser.get("SYSUSER_ACCOUNT"));
        }
        sysLog.put("IP_ADDRESS", BrowserUtils.getIpAddr(request));
        sysLog.put("LOG_CONTENT",logContent);
        sysLog.put("OPER_MODULENAME",moduleName);
        sysLog.put("OPER_TIME",PlatDateTimeUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        dao.saveOrUpdate("PLAT_SYSTEM_SYSLOG",sysLog,AllConstants.IDGENERATOR_UUID,null);
    }
    
    /**
     * 操作类型转换接口
     * @param dataValue
     * @param rowDatas
     * @return
     */
    public Object conventOperType(Object dataValue,List<Object> rowDatas){
        String value = dataValue.toString();
        if(value.equals("登录操作")){
            return "1";
        }else if(value.equals("登出操作")){
            return "2";
        }else{
            return "3";
        }
    }
    
    /**
     * 验证接口
     * @param dataValue
     * @param rowDatas
     * @return
     */
    public String validDataValue(Object dataValue,List<Object> rowDatas){
        return "必须是有效身份证!";
    }
    
    /**
     * 保存闽政通APP登录日志
     * @param request
     * @param userInfo
     */
    public void saveAppLoginLog(HttpServletRequest request,Map<String,Object> userInfo){
        Map<String,Object> sysLog = new HashMap<String,Object>();
        String browser = BrowserUtils.checkBrowse(request);
        sysLog.put("BROWSER", browser);
        sysLog.put("OPER_TYPE", SysLogService.OPER_TYPE_LOGIN);
        sysLog.put("OPER_USERNAME",userInfo.get("USER_MOBILE"));
        sysLog.put("OPER_USERACCOUNT",userInfo.get("USER_MOBILE"));
        sysLog.put("IP_ADDRESS", BrowserUtils.getIpAddr(request));
        sysLog.put("LOG_CONTENT","登录了闽政通APP");
        sysLog.put("OPER_MODULENAME","闽政通APP登录");
        dao.saveOrUpdate("PLAT_SYSTEM_SYSLOG",sysLog,AllConstants.IDGENERATOR_UUID,null);
    }
  
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.platform.system.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.stooges.core.dao.BaseDao;
import com.stooges.core.service.impl.BaseServiceImpl;
import com.stooges.platform.system.dao.CompanyDao;
import com.stooges.platform.system.service.CompanyService;

/**
 * 描述 单位业务相关service实现类
 * @author 胡裕
 * @version 1.0
 * @created 2017-04-09 09:43:59
 */
@Service("companyService")
public class CompanyServiceImpl extends BaseServiceImpl implements CompanyService {

    /**
     * 所引入的dao
     */
    @Resource
    private CompanyDao dao;

    @Override
    protected BaseDao getDao() {
        return dao;
    }
    
    /**
     * 根据单位ID级联删除相关的数据库表信息
     * @param companyId
     */
    public void deleteCompanyCacasdeAssocial(String companyId){
        //级联删除部门信息
        StringBuffer sql = new StringBuffer("DELETE FROM PLAT_SYSTEM_DEPART ");
        sql.append(" WHERE DEPART_COMPANYID=? ");
        dao.executeSql(sql.toString(), new Object[]{companyId});
        //更新用户的部门字段和单位为空
        sql = new StringBuffer("UPDATE PLAT_SYSTEM_SYSUSER U SET U.SYSUSER_COMPANYID=null");
        sql.append(",U.SYSUSER_DEPARTID=null WHERE U.SYSUSER_COMPANYID=? ");
        dao.executeSql(sql.toString(), new Object[]{companyId});
        //级联删除单位信息
        sql = new StringBuffer("DELETE FROM PLAT_SYSTEM_COMPANY  ");
        sql.append("WHERE COMPANY_PATH LIKE ? ");
        dao.executeSql(sql.toString(), new Object[]{"%."+companyId+".%"});
    }
  
}

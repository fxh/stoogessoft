/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.test.demo;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.stooges.core.util.PlatDateTimeUtil;
import com.stooges.core.util.PlatHttpUtil;

/**
 * @author 胡裕
 *
 * 
 */
public class ProductServiceTestCase {

    /**
     * 
     */
    @Test
    public void testInvokeServer(){
        String url = "http://127.0.0.1/szfoa/dataset/AppSerController/invokeservice.do";
        Map<String,Object> paramsMap = new HashMap<String,Object>();
        paramsMap.put("INVOKESERVICE_CODE","001");
        String POSTPARAM_JSON = JSON.toJSONString(paramsMap);
        Map<String,Object> clientParam = new HashMap<String,Object>();
        clientParam.put("POSTPARAM_JSON", POSTPARAM_JSON);
        clientParam.put("tableName","PLAT_SYSTEM_DICTYPE");
        clientParam.put("idAndNameCol","DICTYPE_ID,DICTYPE_NAME");
        clientParam.put("targetCols","DICTYPE_CODE,DICTYPE_PARENTID,DICTYPE_PATH");
        clientParam.put("loadRootId","0");
        clientParam.put("isShowTreeTitle","true");
        clientParam.put("needCheckIds","");
        clientParam.put("treeTitle","字典类别树");
        System.out.println("时间1:"+PlatDateTimeUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
        String result = PlatHttpUtil.sendPostParams(url,clientParam);
        System.out.println("时间2:"+PlatDateTimeUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss"));
    }
}

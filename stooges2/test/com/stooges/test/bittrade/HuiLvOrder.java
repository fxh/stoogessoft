/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.test.bittrade;

import java.io.Serializable;

/**
 * @author 胡裕
 *
 * 
 */
public class HuiLvOrder implements Serializable {
    /**
     * 购买手数
     */
    private Double handleNum; 
    /**
     * 购买价格
     */
    private int price;
    
    /**
     * @param handleNum
     * @param price
     */
    public HuiLvOrder(Double handleNum, int price) {
        this.handleNum = handleNum;
        this.price = price;
    }
    
    /**
     * @return the handleNum
     */
    public Double getHandleNum() {
        return handleNum;
    }
    /**
     * @param handleNum the handleNum to set
     */
    public void setHandleNum(Double handleNum) {
        this.handleNum = handleNum;
    }
    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }
    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }
}

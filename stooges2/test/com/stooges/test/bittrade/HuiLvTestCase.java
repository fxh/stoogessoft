/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.test.bittrade;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.stooges.core.util.PlatNumberUtil;

/**
 * @author 胡裕
 *
 * 
 */
public class HuiLvTestCase {
    
    /**
     * 测试计算金额
     */
    @Test
    public void testCalCostMoney(){
        //定义单手价格
        Double singleMoney = 245.1;
        //定义初始手数
        Double beginHandleNum = 0.03;
        //定义仓位次数
        int coverNumber = 7;
        //定义总钱数
        Double totalMoney = 0.0;
        for(int i=1;i<=coverNumber;i++){
            Double handleNum = null;
            if(i!=1){
                handleNum = Math.pow(2,i-1)*beginHandleNum;
            }else{
                handleNum = beginHandleNum;
            }
            Double money = singleMoney*handleNum;
            totalMoney+=money;
            System.out.println("第:"+i+"次购买手数:"+handleNum+",所花钱:"+money);
        }
        System.out.println("总花钱:"+totalMoney);
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        //定义单手价格
        Double singleMoney = 245.1;
        //定义当前的价格
        int nowPrice = 122440;
        //定义订单
        List<HuiLvOrder> orderList = new ArrayList<HuiLvOrder>();
        HuiLvOrder order1 = new HuiLvOrder(0.02,124060);
        HuiLvOrder order2 = new HuiLvOrder(0.04,123860);
        HuiLvOrder order3 = new HuiLvOrder(0.08,123460);
        HuiLvOrder order4 = new HuiLvOrder(0.16,123060);
        HuiLvOrder order5 = new HuiLvOrder(0.32,122660);
        HuiLvOrder order6 = new HuiLvOrder(0.64,122260);
        orderList.add(order1);
        orderList.add(order2);
        orderList.add(order3);
        orderList.add(order4);
        orderList.add(order5);
        orderList.add(order6);
        calProfitAndLoss(orderList,nowPrice,singleMoney);
    }
    
    /**
     * 计算盈亏率
     * @param orderList
     * @param nowPrice
     * @param singleMoney
     */
    public static void calProfitAndLoss(List<HuiLvOrder> orderList,int nowPrice,Double singleMoney){
        //定义总投入的钱
        Double money = 0.0;
        //定义总盈亏
        Double totalProfitAndLost = 0.0;
        //定义当前的成本线
        int costPrice = 0;
        int totalPrice = 0;
        Double totalHandleNum = 0.0;
        for(HuiLvOrder order:orderList){
            //定义盈亏的价格
            Double profitAndLoss = (nowPrice-order.getPrice())*order.getHandleNum();
            //System.out.println("目前的盈亏价格:"+profitAndLoss);
            totalProfitAndLost+=profitAndLoss;
            money+=(order.getHandleNum()*singleMoney);
            totalPrice+=(order.getPrice()*order.getHandleNum());
            totalHandleNum+=order.getHandleNum();
        }
        costPrice = PlatNumberUtil.getRoundingValue(totalPrice/totalHandleNum);
        Double closeopen= (money+totalProfitAndLost)-money;
        System.out.println("目前的成本线:"+costPrice);
        System.out.println("目前的总盈亏:"+totalProfitAndLost);
        NumberFormat nFromat = NumberFormat.getPercentInstance();
        nFromat.setMinimumFractionDigits(2);
        nFromat.setGroupingUsed(false);
        String RANGEINFO_UP= nFromat.format(closeopen/money);
        System.out.println("目前的盈亏率:"+RANGEINFO_UP);
        System.out.println("目前花费总金额:"+money);
    }

}

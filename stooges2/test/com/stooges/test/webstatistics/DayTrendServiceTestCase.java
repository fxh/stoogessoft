/*
 * Copyright (c) 2005, 2018, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.test.webstatistics;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;

import com.stooges.core.test.BaseTestCase;
import com.stooges.platform.webstatistics.service.DayTrendService;

/**
 * @author 胡裕
 *
 * 
 */
public class DayTrendServiceTestCase extends BaseTestCase {
    /**
     * 
     */
    @Resource
    private DayTrendService dayTrendService;
    
    /**
     * 
     */
    @Test
    public void findList(){
        List<Map<String,Object>> list = dayTrendService.findList(null, null);
        System.out.println(list.toString());
    }
}

/*
 * Copyright (c) 2005, 2014, STOOGES Technology Co.,Ltd. All rights reserved.
 * STOOGES PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 */
package com.stooges.test.system;

import javax.annotation.Resource;

import org.junit.Test;

import com.stooges.core.test.BaseTestCase;
import com.stooges.platform.system.service.SysUserService;

/**
 * @author 胡裕
 *
 * 
 */
public class SysUserServiceTestCase extends BaseTestCase {
    /**
     * 
     */
    @Resource
    private SysUserService sysUserService;
    
    /**
     * 
     */
    @Test
    public void updateUserRightJson(){
        sysUserService.updateUserRightJson("402848a55b6547ec015b6547ec760000");
    }
}
